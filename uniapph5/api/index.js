import {apiResquest} from '@/utils/request'

// 获取首页列表
export function getList(data) {
	return apiResquest({
		url: `/api/getresource/list2`,
		method: 'post',
		data
	})
}

// 通知
export function getNotice(data) {
	return apiResquest({
		url: `/api/getresource/index2`,
		method: 'post',
		data
	})
}

// 验证
export function verifyPassword(data) {
	return apiResquest({
		// url: `/api/getresource/verifyPassword2?token=${uni.getStorageSync('token')}`,
		url: `/api/getresource/verifyPassword2`,
		method: 'post',
		data
	})
}

// 获取详情页
export function getDetail(id, pagefrom='') {
	
	return apiResquest({
		url: `/api/getresource/details2`,
		method: 'GET',
		data:{id,pagefrom}
	})
}

export function getConfig() {
	return apiResquest({
		url: `/api/getresource/getConfig`,
		method: 'GET',
	})
}

