import Router from 'uni-simple-router';
import env from './env.js';
import Cache from '../utils/cache.js'
// const url = ''
const url = ''

export const apiResquest = (prams) => {
	console.log("prams==>", prams)
	// 判断请求类型
	let headerData = {
		'content-type': 'application/json',
	}
	let dataObj = prams.data || {}
	
	// let pwd = Cache.get('password')
	// if (pwd) dataObj.auth = pwd;
	
	let token = Cache.get('token')
	if (token) dataObj.token = token;
	
	return new Promise((resolve, reject) => {
		uni.showLoading({
			title: '加载中',
			mask: true
		})
		return uni.request({
			url: url + prams.url,
			data: dataObj,
			method: prams.method,
			header: headerData,
			success: (res) => {
				console.log('请求返回---', res)
				uni.hideLoading()
				if (res.statusCode == 404) {
					uni.reLaunch({
						url:'/pages/error/404'
					})
					return;
				}
				
				if (res.data.statusCode == 401 || res.statusCode == 401) {
					uni.navigateTo({
						url: '/pages/pwd/index'
					})
					return;
				}
				if (res.statusCode == 2004 || res.data.statusCode == 2004) {
					uni.showToast({
						icon: 'none',
						title: res.data.msg,
						success() {
							setTimeout(() => {
								uni.navigateTo({
									url: '/pages/pwd/index'
								})
							}, 2000)
						}
					})
					
					return;
				}
				//这里是成功的返回码，大家根据自己的实际情况调整
				if (res.statusCode != 200) {
					uni.showToast({
						icon: 'none',
						title: `获取数据失败${res.data.msg ? res.data.msg : ''}`
					})
					return;
				}
				resolve(res.data);
			},
			fail: (err) => {
				reject(err);
				uni.hideLoading()
			},
			complete: () => {
				
			}
		});
	})
}
