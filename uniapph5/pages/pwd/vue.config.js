module.exports = {
	devServer:{
		port:'8083',
		disableHostCheck:true,
		proxy:{
			'/api':{
				target:'',
				changeOrigin:true,
				pathRewrite:{
					'^/api': ''
				}
			}
		}
	}
}