import App from './App'

// #ifndef VUE3
import Vue from 'vue'
Vue.config.productionTip = false
App.mpType = 'app'

import Cache from './utils/cache'
Vue.prototype.$Cache = Cache

import uView from "uview-ui";
Vue.use(uView);

import "./utils/noScale.js"


// var token = "";
// var tkdir = "";

// if (location.href.split("?")[1] && "token" == location.href.split("?")[1].split("=")[0]) {
// 	token = location.href.split("=")[1];
// } else {
// 	var arr0 = location.href.split("#");
// 	var arr = arr0[0].split("/");
// 	if (arr[arr.length - 1] == "") {
// 		token = arr[arr.length - 2];
// 		tkdir = "/" + token + "/";
// 	} else {
// 		token = arr[arr.length - 1];
// 		tkdir = "/" + token;
// }
// };
// if (token != "" && token.length > 15) {
// 	uni.setStorageSync("token", token);
// };
// uni.setStorageSync("tkdir", tkdir);


try {
	function isPromise(obj) {
		return (
			!!obj &&
			(typeof obj === "object" || typeof obj === "function") &&
			typeof obj.then === "function"
		);
	}

	// 统一 vue2 API Promise 化返回格式与 vue3 保持一致
	uni.addInterceptor({
		returnValue(res) {
			if (!isPromise(res)) {
				return res;
			}
			return new Promise((resolve, reject) => {
				res.then((res) => {
					if (res[0]) {
						reject(res[0]);
					} else {
						resolve(res[1]);
					}
				});
			});
		},
	});
} catch (error) {}

const app = new Vue({
	...App
})
app.$mount()
// #endif

// #ifdef VUE3
import {
	createSSRApp
} from 'vue'
export function createApp() {
	const app = createSSRApp(App)
	return {
		app
	}
}
// #endif