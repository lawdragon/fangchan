define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {

    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'resourcesgx/index',
                    add_url: 'resourcesgx/add',
                    edit_url: 'resourcesgx/edit',
                    del_url: 'resourcesgx/del',
                    multi_url: 'resourcesgx/saveStatus',
                    dragsort_url: '',//ajax/weigh
                    table: 'resourcesgx',
                    set_generate_url: 'resourcesgx/set_generate_url',//生成链接
                }
            });

            var table = $("#table");
            var tableOptions = {
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'rid',
                sortName: 'rid',
                searchFormVisible: true,
                showExport: false,
                visible: false,
                showColumns: false,
                search: false,
                showToggle:false,
                showSearch: false,
                onLoadSuccess: function (data){
                    //判断显示隐藏
                    table.bootstrapTable('hideColumn', data.gid != 1 ? 'is_top' : '');
                    table.bootstrapTable('hideColumn', data.gid != 1 ? 'is_recommend' : '');
                    //table.bootstrapTable('hideColumn', data.gid != 1 ? 'status' : '');

                },
                columns: [
                    [
                        {checkbox: true},
                        //{field: 'rid', title: __('Id'), operate: false},
                        {field: 'number', title:'Id'},
                        {field: 'title', title:'名称'},
                        {field: 'info', title:'房东信息', operate: false},
                        /*{field: 'category_name', title:'分类'},
                        {field: 'area_name', title:'区域'},
                        {field: 'address', title:'地址'},*/
                        {field: 'category_name', title:'分类',searchList:$.getJSON("access/categoryList")},//visible: false, 隐藏列表字段
                        {field: 'area_name', title:'区域',searchList:$.getJSON("access/getAreaList")},
                        //{field: 'address', title:'地址', operate: false},
                        {field: 'dl_name_list', title:'关联代理',searchList:$.getJSON("access/getdlList")},
                        {field: 'pz_id', title:'资源配置',searchList:$.getJSON("access/getPzList"), visible: false},
                        /*{field: 'number', title:'置顶'},
                        {field: 'number', title:'推荐',formatter: Table.api.formatter.buttons},
                        {field: 'number', title:'状态'},*/
                        {field: 'gx_name', title:'更新人', operate: false},
                        {field: 'gx_time', title:'更新时间', operate: false},
                        {field: 'is_top', title: '置顶',align: 'center',
                            table: table,
                            searchList: {"1":__('Yes'),"0":__('No')},
                            formatter: Table.api.formatter.toggle},
                        {field: 'is_recommend', title: '推荐',align: 'center',
                            table: table,
                            searchList: {"1":__('Yes'),"0":__('No')},
                            formatter: Table.api.formatter.toggle},
                        {field: 'status', title: __('Status'),align: 'center',
                            table: table,
                            searchList: {"1":__('Yes'),"0":__('No')},
                            formatter: Table.api.formatter.toggle},
                        {field: 'cjname', title: '创建人',visible:function(gid){
                                if(gid==1){
                                    return true;
                                } else {
                                    return false;
                                }
                            },searchList:$.getJSON("access/getdlList")
                        },    
                        //{field: 'createtime', title:'创建时间', formatter: Table.api.formatter.datetime},
                        {field: 'createtime', title: '关联时间', formatter: Table.api.formatter.datetime,datetimeFormat:'YYYY-MM-DD HH:mm:ss', operate: 'RANGE', addclass: 'datetimerange', sortable: true},
                        //{field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate, formatter: Table.api.formatter.operate}
                        {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate,
                            buttons:[
                               /* {
                                    name:'adopt',
                                    text:'关联',
                                    title:'关联',
                                    classname: 'btn btn-xs btn-info btn-view btn-ajax',
                                    icon: 'fa fa-check',
                                    url: 'resourcesgx/adopt',
                                    visible:function(row){
                                        if(row['gid']!=1){
                                            return true;
                                        }else{
                                            return false;
                                        }
                                        return true;
                                    },
                                    refresh:true
                                },*/
                                /*{
                                    name:'cancel',
                                    text:'取消关联',
                                    title:'取消关联',
                                    classname: 'btn btn-xs btn-warning btn-view btn-ajax',
                                    icon: 'fa fa-times',
                                    url: 'resourcesgx/cancel',
                                    visible:function(row){
                                        if(row['gid']!=1){
                                            return true;
                                        }else{
                                            return false;
                                        }
                                        return true;
                                    },
                                    refresh:true
                                }*/
                                //formatter: Table.api.formatter.operate
                            ],
                            formatter: function(value, row, index) {
                                var that = $.extend({}, this);
                                var table = $(that.table).clone(true);
                                // operate-edit编辑  perate-del删除
                                //判断什么时候显示什么时候不显示
                                if (row.gid === 2) {}
                                    //$(table).data("operate-edit", null);
                                    $(table).data("operate-del", null); // 列表页面隐藏 .编辑operate-edit  - 删除按钮operate-del
                                
                                that.table = table;
                                return Table.api.formatter.operate.call(that, value, row, index);
                            }
                        }
                    ]
                ]
            };
            //console.log(data);
            // 初始化表格
            table.bootstrapTable(tableOptions);

            // 为表格绑定事件
            Table.api.bindevent(table);

            //绑定TAB事件
            $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
                // var options = table.bootstrapTable(tableOptions);
                var typeStr = $(this).attr("href").replace('#', '');
                var options = table.bootstrapTable('getOptions');
                options.pageNumber = 1;
                options.queryParams = function (params) {
                    // params.filter = JSON.stringify({type: typeStr});
                    params.type = typeStr;

                    return params;
                };
                table.bootstrapTable('refresh', {});
                return false;

            });

            //必须默认触发shown.bs.tab事件
            // $('ul.nav-tabs li.active a[data-toggle="tab"]').trigger("shown.bs.tab");
            //搜索提交
            /*$(document).on("click", ".btn-success", function () {
                var options = table.bootstrapTable('getOptions');
                options.pageNumber = 1;
                options.queryParams = function (params) {
                    var number=$('#number').val();
                    var title=$('#title').val();
                    var status=$('select[name="status"] option:selected').val();
                    var category=$('select[name="category"] option:selected').val();
                    var area=$('select[name="area"] option:selected').val();
                    params.number = number;
                    params.title = title;
                    params.status = status;
                    params.category = category;
                    params.area = area;
                    return params;
                };
                table.bootstrapTable('refresh', {});
                return false;
            });*/
            //批量-下架
            $(".batchSet1").click(function() {
                layer.confirm("确定所有下架吗？", function() {
                    $.ajax({
                        url: "resourcesgx/saveStatus",
                        type:"POST",
                        dataType:'json',
                        data: {
                            status: 0
                        },
                        success(res) {
                            if (res == 1) {
                                layer.alert("下架成功！")
                            } else {
                                layer.alert("下架失败！")
                            }
                            table.bootstrapTable('refresh', {});
                        }
                    })
                })
            });
            //批量-上架
            $(".batchSet2").click(function() {
                layer.confirm("确定所有上架吗？", function() {
                    $.ajax({
                        url: "resourcesgx/saveStatus",
                        type:"POST",
                        dataType:'json',
                        data: {
                            status: 1
                        },
                        success(res) {
                            if (res == 1) {
                                //layer.alert("上架成功！")
                                layer.msg('上架成功！');
                            } else {
                                //layer.alert("上架失败！")
                                layer.msg('上架失败！');
                            }
                            table.bootstrapTable('refresh', {});
                        }
                    })
                })
            });
            //生成链接
            $('#setUrlBtn').on('click', function (){
                var number=$('#number').val();
                var title=$('#title').val();
                var status=$('select[name="status"] option:selected').val();
                var category=$('select[name="category"] option:selected').val();
                var area=$('select[name="area"] option:selected').val();
                Fast.api.open($.fn.bootstrapTable.defaults.extend.set_generate_url+'?number='+number+'&title='+title+'&status='+status+'&category='+category+'&area='+area,'生成链接');
            });
            table.on('post-body.bs.table', function (e, settings, json, xhr) {
                $('.datetimerange').each(function () {
                    $(this).attr('autocomplete', 'off');
                })
            });
        },
        add: function () {
            /*var ue = UE.getEditor('container');

            ue.ready(function(){
                ue.setHeight(300);//设置编辑器高度
            });*/
            Controller.api.bindevent();
            setTimeout(function () {
                $("#c-type").trigger("change");
            }, 100);
            /*$(document).on('click', '.btn-primary ', function(){
                Form.api.submit($("form[role=form]"), function () {
                    window.parent.location.reload();
                });
            });*/
        },
        edit: function () {
            /*var ue = UE.getEditor('container');

            ue.ready(function(){
                ue.setHeight(300);//设置编辑器高度
            });*/
            Controller.api.bindevent();
        },
        set_generate_url: function () {
            Controller.api.bindevent();
        },
        api: {
            bindevent: function () {
                $(document).on("change", "#c-type", function () {
                    $("#c-pid option[data-type='all']").prop("selected", true);
                    $("#c-pid option").removeClass("hide");
                    $("#c-pid option[data-type!='" + $(this).val() + "'][data-type!='all']").addClass("hide");
                    $("#c-pid").data("selectpicker") && $("#c-pid").selectpicker("refresh");
                });
                Form.api.bindevent($("form[role=form]"));
            }
        }
    };
    return Controller;
});
