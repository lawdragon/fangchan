define(['jquery', 'bootstrap', 'backend', 'table', 'form', 'jstree'], function ($, undefined, Backend, Table, Form, undefined) {
    //读取选中的条目
    $.jstree.core.prototype.get_all_checked = function (full) {
        var obj = this.get_selected(), i, j;
        for (i = 0, j = obj.length; i < j; i++) {
            obj = obj.concat(this.get_node(obj[i]).parents);
        }
        obj = $.grep(obj, function (v, i, a) {
            return v != '#';
        });
        obj = obj.filter(function (itm, i, a) {
            return i == a.indexOf(itm);
        });
        return full ? $.map(obj, $.proxy(function (i) {
            return this.get_node(i);
        }, this)) : obj;
    };
    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'access/index',
                    add_url: 'access/add',
                    edit_url: 'access/edit',
                    del_url: 'access/del',
                    multi_url: 'access/multi',
                    table: 'access',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'log_id',
                sortName: 'log_id',
                searchFormVisible: true,
                showExport: false,
                visible: false,
                showColumns: false,
                search: false,
                showToggle:false,
                showSearch: false,
                columns: [
                    [
                        {field: 'log_id', title: __('Id'), operate: false},
                        {field: 'number', title:'编号', operate: false},
                        {field: 'title', title:'名称', operate: false},
                        //{field: 'category_name', title:'分类'},
                        {field: 'category_name', title:'分类',searchList:$.getJSON("access/categoryList")},//visible: false, 隐藏列表字段
                        {field: 'area_name', title:'区域',searchList:$.getJSON("access/getAreaList")},
                        {field: 'address', title:'地址', operate: false},
                        {field: 'nickname', title: '所属代理',visible:function(gid){
                            if(gid==1){
                                return false;
                            } else {
                                return false;
                            }
                        },searchList:$.getJSON("access/getdlList")
                        },
                        {field: 'ip', title: 'IP', operate: false},
                        {field: 'createtime', title: '创建时间', formatter: Table.api.formatter.datetime,datetimeFormat:'YYYY-MM-DD HH:mm:ss', operate: 'RANGE', addclass: 'datetimerange', sortable: true}
                        //{field: 'createtime', title:'创建时间', formatter: Table.api.formatter.datetime},
                        /*{field: 'createtime', title:'创建时间',addclass:'datetimerange', formatter: Table.api.formatter.datetime,datetimeFormat:'YYYY-MM-DD', sortable: true,searchList: function (column) {
                                return Template('categorytpl2', {});
                            }
                        }*/
                       /* {checkbox: true},
                        {field: 'id', title: __('Id')},
                        {field: 'name', title: __('Name')},
                        {field: 'createtime', title: __('Createtime'), formatter: Table.api.formatter.datetime, operate: 'RANGE', addclass: 'datetimerange', sortable: true},
                        {field: 'updatetime', title: __('Updatetime'), formatter: Table.api.formatter.datetime, operate: 'RANGE', addclass: 'datetimerange', sortable: true},
                        {field: 'status', title: __('Status'), formatter: Table.api.formatter.status},
                        {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate, formatter: Table.api.formatter.operate}*/
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
            table.on('post-body.bs.table', function (e, settings, json, xhr) {
                $('.datetimerange').each(function () {
                    $(this).attr('autocomplete', 'off');
                })
            });
        },
        add: function () {
            Controller.api.bindevent();
        },
        edit: function () {
            Controller.api.bindevent();
        },
        api: {
            bindevent: function () {
                Form.api.bindevent($("form[role=form]"), null, null, function () {
                    if ($("#treeview").length > 0) {
                        var r = $("#treeview").jstree("get_all_checked");
                        $("input[name='row[rules]']").val(r.join(','));
                    }
                    return true;
                });
                //渲染权限节点树
                //销毁已有的节点树
                $("#treeview").jstree("destroy");
                Controller.api.rendertree(nodeData);
                //全选和展开
                $(document).on("click", "#checkall", function () {
                    $("#treeview").jstree($(this).prop("checked") ? "check_all" : "uncheck_all");
                });
                $(document).on("click", "#expandall", function () {
                    $("#treeview").jstree($(this).prop("checked") ? "open_all" : "close_all");
                });
                $("select[name='row[pid]']").trigger("change");
            },
            rendertree: function (content) {
                $("#treeview")
                    .on('redraw.jstree', function (e) {
                        $(".layer-footer").attr("domrefresh", Math.random());
                    })
                    .jstree({
                        "themes": {"stripes": true},
                        "checkbox": {
                            "keep_selected_style": false,
                        },
                        "types": {
                            "root": {
                                "icon": "fa fa-folder-open",
                            },
                            "menu": {
                                "icon": "fa fa-folder-open",
                            },
                            "file": {
                                "icon": "fa fa-file-o",
                            }
                        },
                        "plugins": ["checkbox", "types"],
                        "core": {
                            'check_callback': true,
                            "data": content
                        }
                    });
            }
        }
    };
    return Controller;
});
