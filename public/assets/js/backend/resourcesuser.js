define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {
  function setSearch() {
          $("select[name='category_name']").parents('.form-group').hide()
          $("select[name='area_name']").parents('.form-group').hide()
          $("select[name='pz_id']").parents('.form-group').hide()
          $("select[name='dl_name_list']").parents('.form-group').hide()
          $("select[name='is_top']").parents('.form-group').hide()
          $("select[name='is_recommend']").parents('.form-group').hide()
          $("select[name='status']").parents('.form-group').hide()
          $("select[name='is_idle']").parents('.form-group').hide()
          $("select[name='cjname']").parents('.form-group').hide()
          $("input[name='createtime']").parents('.form-group').hide()
  }
  var Controller = {
    index: function () {
      // 初始化表格参数配置
      Table.api.init({
        extend: {
          index_url: 'resourcesuser/index',
          add_url: 'resourcesuser/add',
          edit_url: 'resourcesuser/edit',
          del_url: '',//resourcesuser/del_status
          multi_url: 'resourcesuser/multi_save',
          dragsort_url: '',//ajax/weigh
          table: 'resourcesuser',
          set_generate_url: 'resourcesuser/set_generate_url',//生成链接
        }
      });
      var table = $("#table");
      var tableOptions = {
        url: $.fn.bootstrapTable.defaults.extend.index_url,
        pk: 'id',
        sortName: 'id',
        searchFormVisible: true,
        showExport: false,
        visible: false,
        showColumns: false,
        search: true,
        showToggle: false,
        showSearch: true,
        // commonSearch:false,
        onLoadSuccess: function (data) {
          //判断显示隐藏
          // table.bootstrapTable('hideColumn', data.gid != 1 ? 'is_top' : '');
          // table.bootstrapTable('hideColumn', data.gid != 1 ? 'is_recommend' : '');
          // table.bootstrapTable('hideColumn', data.gid != 1 ? 'status' : '');$("select[name='category_name']").parents('.form-group').toggle()
          setSearch();
        },
        columns: [
          [
            { checkbox: true },
            // {field: 'id', title: 'ID', operate: false},
            { field: 'rid', title: '编号' },
            { 
              field: 'title', 
              title: '名称', 
              extend:' target="_blank"',
              formatter: function(item, row){
                console.log('===',item, row)
                return '<a href="/#/pages/detail/index?id='+row.rid+'&pagefrom=backend" class="btn-view btn-dialog" target="_blank">'+item+'</a>'
              }  
            },
            { field: 'info', title: '房东信息',operate: 'LIKE',},
            {
              field: 'status', title: '状态', align: 'center',
              table: table,
              searchList: { "1": '上架', "0": '下架' },
              formatter: Table.api.formatter.toggle
            },
            {
              field: 'is_idle', title: '忙碌', align: 'center',
              table: table,
              searchList: {1: __('Yes'), 0: __('No') },
              formatter: Table.api.formatter.toggle
            },
            { field: 'category_name', title: '分类', searchList: $.getJSON("/api/basedata/categoryList") },//visible: false, 隐藏列表字段
            { field: 'area_name', title: '区域', searchList: $.getJSON("/api/basedata/getAreaList?status=1") },
            //{field: 'address', title:'地址', operate: false},
            { field: 'pz_id', title: '资源配置', searchList: $.getJSON("access/getPzList"), visible: false },
            {
              field: 'is_top', title: '置顶', align: 'center',
              table: table,
              searchList: { 1: __('Yes'), 0: __('No') },
              formatter: Table.api.formatter.toggle
            },
            {
              field: 'is_recommend', title: '推荐', align: 'center',
              table: table,
              searchList: { 1: __('Yes'), 0: __('No') },
              formatter: Table.api.formatter.toggle
            },
            { field: 'dl_name_list', title: '关联代理', searchList: $.getJSON("/api/basedata/getdlList") },
            // 上下架
            // /*{field: 'number', title:'置顶'},
            // {field: 'number', title:'推荐',formatter: Table.api.formatter.buttons},
            // {field: 'number', title:'状态'},*/
            /*{field: 'is_top', title: '置顶',align: 'center',
                table: table,
                searchList: {"1":__('Yes'),"0":__('No')},
                formatter: Table.api.formatter.toggle},
            {field: 'is_recommend', title: '推荐',align: 'center',
                table: table,
                searchList: {"1":__('Yes'),"0":__('No')},
                formatter: Table.api.formatter.toggle},
             */
            /*{field: 'status', title: '上下架状态',align: 'center',
                table: table,
                searchList: {"1":__('Yes'),"0":__('No')},
                formatter: Table.api.formatter.toggle},*/
            /*{field: 'is_del', title: '删除状态',align: 'center',
                table: table,
                searchList: {"1":__('Yes'),"0":__('No')},
                formatter: Table.api.formatter.toggle},*/
            //{field: 'createtime', title:'创建时间', formatter: Table.api.formatter.datetime},
            {
              field: 'cjname', title: '创建人', visible: function (gid) {
                if (gid == 1) {
                  return true;
                } else {
                  return false;
                }
              }, searchList: $.getJSON("/api/basedata/getdlList")
            },
            { field: 'createtime', title: '关联时间', formatter: Table.api.formatter.datetime, datetimeFormat: 'YYYY-MM-DD HH:mm:ss', operate: 'RANGE', addclass: 'datetimerange', sortable: true },
            //{field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate, formatter: Table.api.formatter.operate}
            {
              field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate,
              buttons: [
                /* {
                     name:'adopt',
                     text:'关联',
                     title:'关联',
                     classname: 'btn btn-xs btn-info btn-view btn-ajax',
                     icon: 'fa fa-check',
                     url: 'resourcesuser/adopt',
                     visible:function(row){
                         /!*if(row['gid']!=1){
                             return true;
                         }else{
                             return false;
                         }*!/
                         return true;
                     },
                     refresh:true
                 },*/
                {
                  name: 'cancel',
                  text: '取消关联',
                  title: '取消关联',
                  classname: 'btn btn-xs btn-warning btn-view btn-ajax',
                  icon: 'fa fa-times',
                  url: 'resourcesuser/cancel',
                  visible: function (row) {
                    /*if(row['gid']!=1){
                        return true;
                    }else{
                        return false;
                    }*/
                    return true;
                  },
                  refresh: true
                },
                {
                  name: 'look',
                  text: '查看',
                  title: '查看',
                  classname: 'btn btn-xs btn-primary btn-dialog', //  btn-dialog
                  extend:' target="_blank"',
                  url: function(row){
                    console.log('点击查看',row)
                    // extend:' target="_blank"',
                    return "/#/pages/detail/index?id="+row.rid+'&pagefrom=backend'
                  },
                }
              ], formatter: Table.api.formatter.operate
            }
          ]
        ]
      };
      //console.log(data);
      // 初始化表格
      table.bootstrapTable(tableOptions);
      // 为表格绑定事件
      Table.api.bindevent(table);
      //绑定TAB事件
      $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
        // var options = table.bootstrapTable(tableOptions);
        var typeStr = $(this).attr("href").replace('#', '');
        var options = table.bootstrapTable('getOptions');
        options.pageNumber = 1;
        options.queryParams = function (params) {
          // params.filter = JSON.stringify({type: typeStr});
          params.type = typeStr;
          return params;
        };
        table.bootstrapTable('refresh', {});
        return false;
      });
      // 搜索切换
      $(".bootstrap-table").on("click", "button[name='commonSearch']", function(res){
        console.log('这里执行了吗')
        var that = $(this)
        $('.commonsearch-table').removeClass('hidden');
        $("select[name='category_name']").parents('.form-group').toggle()
          $("select[name='area_name']").parents('.form-group').toggle()
          $("select[name='pz_id']").parents('.form-group').toggle()
          $("select[name='dl_name_list']").parents('.form-group').toggle()
          $("select[name='is_top']").parents('.form-group').toggle()
          $("select[name='is_recommend']").parents('.form-group').toggle()
          $("select[name='status']").parents('.form-group').toggle()
          $("select[name='is_idle']").parents('.form-group').toggle()
          $("select[name='cjname']").parents('.form-group').toggle()
          $("input[name='createtime']").parents('.form-group').toggle()
        // setTimeout(function(){
        //   $('.commonsearch-table').show();
        // },2000)
        // return false
      })
      $(".bootstrap-table").on("click", ".btn-success", function(res){
        if ($("select[name='category_name']").parents('.form-group').is(":visible")) {
          $("select[name='category_name']").parents('.form-group').show()
          $("select[name='area_name']").parents('.form-group').show()
          $("select[name='pz_id']").parents('.form-group').show()
          $("select[name='dl_name_list']").parents('.form-group').show()
          $("select[name='is_top']").parents('.form-group').show()
          $("select[name='is_recommend']").parents('.form-group').show()
          $("select[name='status']").parents('.form-group').show()
          $("select[name='is_idle']").parents('.form-group').show()
          $("select[name='cjname']").parents('.form-group').show()
          $("input[name='createtime']").parents('.form-group').show()
        } else {
          $("select[name='category_name']").parents('.form-group').hide()
          $("select[name='area_name']").parents('.form-group').hide()
          $("select[name='pz_id']").parents('.form-group').hide()
          $("select[name='dl_name_list']").parents('.form-group').hide()
          $("select[name='is_top']").parents('.form-group').hide()
          $("select[name='is_recommend']").parents('.form-group').hide()
          $("select[name='status']").parents('.form-group').hide()
          $("select[name='is_idle']").parents('.form-group').hide()
          $("select[name='cjname']").parents('.form-group').hide()
          $("input[name='createtime']").parents('.form-group').hide()
        }
      })
      //必须默认触发shown.bs.tab事件
      // $('ul.nav-tabs li.active a[data-toggle="tab"]').trigger("shown.bs.tab");
      //搜索提交
      /*$(document).on("click", ".btn-success", function () {
          var options = table.bootstrapTable('getOptions');
          options.pageNumber = 1;
          options.queryParams = function (params) {
              var number=$('#number').val();
              var title=$('#title').val();
              var status=$('select[name="status"] option:selected').val();
              var category=$('select[name="category"] option:selected').val();
              var area=$('select[name="area"] option:selected').val();
              params.number = number;
              params.title = title;
              params.status = status;
              params.category = category;
              params.area = area;
              return params;
          };
          table.bootstrapTable('refresh', {});
          return false;
      });*/
      //批量-下架
      $(".batchSet1").click(function () {
        layer.confirm("确定所有下架吗？", function () {
          $.ajax({
            url: "resourcesuser/saveStatus",
            type: "POST",
            dataType: 'json',
            data: {
              status: 0 , // 0 下架 1 上架
            },
            success(res) {
              if (res == 1) {
                layer.alert("下架成功！")
              } else {
                layer.alert("下架失败！")
              }
              table.bootstrapTable('refresh', {});
            }
          })
        })
      });
      //批量-上架
      $(".batchSet2").click(function () {
        layer.confirm("确定所有上架吗？", function () {
          $.ajax({
            url: "resourcesuser/saveStatus",
            type: "POST",
            dataType: 'json',
            data: {
              status: 1
            },
            success(res) {
              if (res == 1) {
                //layer.alert("上架成功！")
                layer.msg('上架成功！');
              } else {
                //layer.alert("上架失败！")
                layer.msg('上架失败！');
              }
              table.bootstrapTable('refresh', {});
            }
          })
        })
      });
      //生成链接
      $('#setUrlBtn').on('click', function () {
        var number = $('#number').val();
        var title = $('#title').val();
        var status = $('select[name="status"] option:selected').val();
        var category = $('select[name="category"] option:selected').val();
        var area = $('select[name="area"] option:selected').val();
        //获取当前选中的id
        var ids = Table.api.selectedids(table);
        var rid = '';
        if (ids.length) {
          rid = ids.join();
        }
        console.log(ids.join());
        Fast.api.open($.fn.bootstrapTable.defaults.extend.set_generate_url + '?number=' + number + '&title=' + title + '&status=' + status + '&category=' + category + '&area=' + area + '&rid=' + rid, '生成链接');
      });
      //保存链接
      $(".saveUrlBtn").click(function () {
        layer.confirm("确定所有上架吗？", function () {
          $.ajax({
            url: "resourcesuser/set_generate_url",
            type: "POST",
            dataType: 'json',
            data: {
              status: 1
            },
            success(res) {
              if (res == 1) {
                //layer.alert("上架成功！")
                layer.msg('上架成功');
              } else {
                //layer.alert("上架失败！")
                layer.msg('上架失败');
              }
              table.bootstrapTable('refresh', {});
            }
          })
        })
      });
      table.on('post-body.bs.table', function (e, settings, json, xhr) {
        $('.datetimerange').each(function () {
          $(this).attr('autocomplete', 'off');
        })
      });
    },
    add: function () {
      /*var ue = UE.getEditor('container');
      ue.ready(function(){
          ue.setHeight(300);//设置编辑器高度
      });*/
      Controller.api.bindevent();
      setTimeout(function () {
        $("#c-type").trigger("change");
      }, 100);
    },
    edit: function () {
      /*var ue = UE.getEditor('container');
      ue.ready(function(){
          ue.setHeight(300);//设置编辑器高度
      });*/
      Controller.api.bindevent();
    },
    set_generate_url: function () {
      Controller.api.bindevent();
    },
    api: {
      bindevent: function () {
        $(document).on("change", "#c-type", function () {
          $("#c-pid option[data-type='all']").prop("selected", true);
          $("#c-pid option").removeClass("hide");
          $("#c-pid option[data-type!='" + $(this).val() + "'][data-type!='all']").addClass("hide");
          $("#c-pid").data("selectpicker") && $("#c-pid").selectpicker("refresh");
        });
        Form.api.bindevent($("form[role=form]"));
      }
    }
  };
  return Controller;
});
function batchSetBitField(field, value) {
  var urls = './resourcesuser/saveStatus';
  //console.log(arr);
  const index = layer.load(1, { shade: [0.1, '#fff'] });
  $.ajax({
    url: urls,
    type: "POST",
    data: { 'status': value },
    async: true,
    dataType: 'json',
    success: function (data) {
      //alert(data.url);
      location.reload(true);
      //layer.close("./resourcesuser/index");
      //window.href.url="./resourcesuser/index";
    }
  });
  /* console.log(row);
    const index = layer.load(1, {shade: [0.1, '#fff']});
    layer.close(index);*/
  /* const index = layer.load(1, {shade: [0.1, '#fff']});
   let id = [];
   if (row) {
       id.push(row.id);
       value = row[field] = !row[field];
   } else {
       id = getSelectId(function (row) {
           row[field] = value;
       });
   }
   $scope._request({
       method: 'POST',
       url: '/' + webpageParameter.module + '/Video/batchSetBitField',
       data: {id, field, value: value}
   }).then(function () {
       layer.close(index);
   }).catch(function () {
       layer.close(index);
       layer.alert('操作失败');
   });*/
}
