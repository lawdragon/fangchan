define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {

    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'resourcesconfig/index',
                    add_url: 'resourcesconfig/add',
                    edit_url: 'resourcesconfig/edit',
                    del_url: 'resourcesconfig/del',
                    multi_url: 'resourcesconfig/multi',
                    dragsort_url: '',//ajax/weigh
                    table: 'resourcesconfig',
                },
                //pageList: [5, 10, 'All']
            });

            var table = $("#table");
            var tableOptions = {
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'weigh',
                pagination: true,
                commonSearch: false,
                search: false,
                visible:false,
                showToggle:false,
                showColumns:false,
                showExport:false,
                columns: [
                    [
                        {checkbox: true},
                        {field: 'id', title: __('Id')},
                        {field: 'name', title: '名称'},
                        /*{field: 'sort', title: '排序'},
                        {field: 'status', title: __('Status')},*/
                        {field: 'weigh', title:'排序', operate: false, formatter: function (value, row, index){
                                return "<input type='text' name='weigh-"+row['id']+"' id='weigh-"+row['id']+"' value='"+value+"' onkeyup='editField(this)'>";//这里是重点

                            }},
                        {field: 'status', title: __('Status'),align: 'center',
                            table: table,
                            searchList: {"1":__('Yes'),"0":__('No')},
                            formatter: Table.api.formatter.toggle},
                        {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate, formatter: Table.api.formatter.operate}
                    ]
                ]
            };
            // 初始化表格
            table.bootstrapTable(tableOptions);

            // 为表格绑定事件
            Table.api.bindevent(table);

            //绑定TAB事件
            $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
                // var options = table.bootstrapTable(tableOptions);
                var typeStr = $(this).attr("href").replace('#', '');
                var options = table.bootstrapTable('getOptions');
                options.pageNumber = 1;
                options.queryParams = function (params) {
                    // params.filter = JSON.stringify({type: typeStr});
                    params.type = typeStr;

                    return params;
                };
                table.bootstrapTable('refresh', {});
                return false;

            });

            //必须默认触发shown.bs.tab事件
            // $('ul.nav-tabs li.active a[data-toggle="tab"]').trigger("shown.bs.tab");

        },
        add: function () {
            Controller.api.bindevent();
            setTimeout(function () {
                $("#c-type").trigger("change");
            }, 100);
        },
        edit: function () {
            Controller.api.bindevent();
        },
        api: {
            bindevent: function () {
                $(document).on("change", "#c-type", function () {
                    $("#c-pid option[data-type='all']").prop("selected", true);
                    $("#c-pid option").removeClass("hide");
                    $("#c-pid option[data-type!='" + $(this).val() + "'][data-type!='all']").addClass("hide");
                    $("#c-pid").data("selectpicker") && $("#c-pid").selectpicker("refresh");
                });
                Form.api.bindevent($("form[role=form]"));
            }
        }
    };
    return Controller;
});
function editField(obj){
    //ids: 19
    // params: status=0
    var names=obj.name;
    var arr = names.split('-');
    var values=$("#"+names).val();
    var urls='./area/multi';
    //console.log(arr);
    $.ajax({
        url: urls,
        type:"POST",
        data:{'params':'weigh='+values,'ids':arr[1]},
        async: false,
        dataType:'json',
        success: function (data) {

        }
    });

}
