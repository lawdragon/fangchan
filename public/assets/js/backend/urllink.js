define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {

    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'urllink/index',
                    add_url: 'urllink/add',
                    edit_url: 'urllink/edit',
                    del_url: 'urllink/del',
                    multi_url: 'urllink/multi_save',
                    dragsort_url: '',//ajax/weigh
                    table: 'urllink',
                },
                //pageList: [10, 15, 20, 'All']
            });

            var table = $("#table");
            var tableOptions = {
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'id',
                sortOrder: 'desc',
                searchFormVisible: true,
                showExport: false,
                visible: false,
                showColumns: false,
                search: false,
                showToggle:false,
                showSearch: false,
                columns: [
                    [
                        {checkbox: true},
                        {field: 'id', title: __('Id'), operate: false},
                        {field: 'url', title: 'URL', operate: false},
                        {field: 'token', title: '链接展示', operate: false, formatter: function (value, row, index){
                            return "<a target='_blank' href='"+row['url']+"'>"+row['url']+"</>"
                        }},
                        {field: 'pwd', title: '查看密码', operate: false},
                        {field: 'end_time', title:'到期时间', formatter: Table.api.formatter.datetime,datetimeFormat:'YYYY-MM-DD HH:mm:ss', operate: 'RANGE', addclass: 'datetimerange', sortable: true, operate: false},
                        {field: 'createtime', title:'创建时间', formatter: Table.api.formatter.datetime,datetimeFormat:'YYYY-MM-DD HH:mm:ss', operate: 'RANGE', addclass: 'datetimerange', sortable: true, operate: false},
                        {field: 'nickname', title: '归属代理',visible:function(gid){
                                if(gid==1){
                                    return true;
                                } else {
                                    return false;
                                }
                            },searchList:$.getJSON("access/getdlList")
                        },
                        {field: 'urlStatus', title: '链接过期筛选',visible:false,searchList:$.getJSON("access/getUrlStatusList")
                        },
                        {field: 'status', title: __('Status'),align: 'center',
                            table: table,
                            searchList: {"1":__('Yes'),"0":__('No')},
                            formatter: Table.api.formatter.toggle},
                        {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate,
                            buttons:[
                                {
                                    name: 'question',
                                    text:'查看二维码',
                                    title: __('查看二维码'),
                                    classname: 'btn btn-xs btn-primary btn-view btn-dialog',
                                    icon: 'fa fa-list',
                                    url: 'urllink/look?ids={id}',

                                }
                            ],formatter: Table.api.formatter.operate, operate: false
                        }
                        //{field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate, formatter: Table.api.formatter.operate}
                    ]
                ]
            };
            // 初始化表格
            table.bootstrapTable(tableOptions);

            // 为表格绑定事件
            Table.api.bindevent(table);

            //绑定TAB事件
            $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
                // var options = table.bootstrapTable(tableOptions);
                var typeStr = $(this).attr("href").replace('#', '');
                var options = table.bootstrapTable('getOptions');
                options.pageNumber = 1;
                options.queryParams = function (params) {
                    // params.filter = JSON.stringify({type: typeStr});
                    params.type = typeStr;

                    return params;
                };
                table.bootstrapTable('refresh', {});
                return false;

            });

            //必须默认触发shown.bs.tab事件
            // $('ul.nav-tabs li.active a[data-toggle="tab"]').trigger("shown.bs.tab");

        },
        add: function () {
            Controller.api.bindevent();
            setTimeout(function () {
                $("#c-type").trigger("change");
            }, 100);
        },
        edit: function () {
            Controller.api.bindevent();
        },
        api: {
            bindevent: function () {
                $(document).on("change", "#c-type", function () {
                    $("#c-pid option[data-type='all']").prop("selected", true);
                    $("#c-pid option").removeClass("hide");
                    $("#c-pid option[data-type!='" + $(this).val() + "'][data-type!='all']").addClass("hide");
                    $("#c-pid").data("selectpicker") && $("#c-pid").selectpicker("refresh");
                });
                Form.api.bindevent($("form[role=form]"));
            }
        }
    };
    return Controller;
});
function editField(obj){
    //ids: 19
    // params: status=0
    var names=obj.name;
    var arr = names.split('-');
    var values=$("#"+names).val();
    var urls='./urllink/multi';
    //console.log(arr);
    $.ajax({
        url: urls,
        type:"POST",
        data:{'params':'weigh='+values,'ids':arr[1]},
        async: false,
        dataType:'json',
        success: function (data) {

        }
    });

}
