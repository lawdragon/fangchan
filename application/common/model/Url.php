<?php

namespace app\common\model;

use think\Cache;
use think\Model;
use think\Page;
use think\Db;
/**
 * 域名url数据模型
 */
class Url extends Model
{
    //列表页面
    public function getWeburlList($where,$limit=10,$p=1,$order="a.id desc"){

        $model = Db::name('url');
        $field='a.*,b.username,b.nickname';
        //->Join('admin u',"u.uid=a.cuid","LEFT")
        $list =$model->alias('a')->Join('admin b',"a.dl_uid=b.id","LEFT")->field($field)->where($where)->order($order)->limit($limit)->page($p)->select();
    //     echo Db::getLastSql();
    // exit;
        $count=$model->alias('a')->where($where)->count();

        return array('list'=>$list,'count'=>$count,'p'=>$p);
    }
    //url链接列表
    public function getUrlLinkList($where,$limit=10,$p=1,$order="a.id desc"){

        $model = Db::name('resourced_url');
        $field='a.*,b.username,b.nickname';
        $list =$model->alias('a')->Join('admin b',"a.cuid=b.id","LEFT")->field($field)->where($where)->order($order)->limit($limit)->page($p)->select();
        $count=$model->alias('a')->where($where)->count();

        return array('list'=>$list,'count'=>$count,'p'=>$p);
    }
    //url链接详情
    public function getUrlLinkInfo($where){
        $model = Db::name('resourced_url');
        $field='a.*';
        $list =$model->alias('a')->field($field)->where($where)->find();
        return $list;
    }
}
