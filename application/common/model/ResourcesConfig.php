<?php

namespace app\common\model;

use think\Cache;
use think\Model;
use think\Page;
use think\Db;
/**
 * 资源配置数据模型
 */
class ResourcesConfig extends Model
{

    //列表页面
    public function getResourcesconfigList($where,$limit=10,$p=1,$order="a.weigh desc"){

        $model = Db::name('resources_config');
        $field='a.*';
        //->Join('admin u',"u.uid=a.cuid","LEFT")
        $list =$model->alias('a')->field($field)->where($where)->order($order)->limit($limit)->page($p)->select();
        $count=$model->alias('a')->where($where)->count();

        return array('list'=>$list,'count'=>$count,'p'=>$p);
    }

}
