<?php

namespace app\common\model;

use think\Cache;
use think\Model;
use think\Page;
use think\Db;
/**
 * 访问统计数据模型
 */
class Access extends Model
{
    //列表页面
    public function getAccessList($where,$limit=10,$p=1,$order="a.id desc"){

        $model = Db::name('Access_log');
        $field='a.*,a1.number,a1.title,a1.cover,a1.video,a1.content,a1.address,b.area_name,c.category_name,d.nickname';
        //->Join('resourced_url u',"u.id=a.url_id","LEFT")
        $list =$model->alias('a')->Join('resources a1',"a1.rid=a.rid","LEFT")->Join('area b',"b.id=a1.area_id","LEFT")->Join('category c',"c.id=a1.category_id","LEFT")->Join('admin d',"d.id=a.cuid","LEFT")->Join('resourced_url u',"u.id=a.url_id","LEFT")->field($field)->where($where)->order($order)->limit($limit)->page($p)->select();
        $count=$model->alias('a')->Join('resources a1',"a1.rid=a.rid","LEFT")->Join('area b',"b.id=a1.area_id","LEFT")->Join('category c',"c.id=a1.category_id","LEFT")->Join('admin d',"d.id=a.cuid","LEFT")->Join('resourced_url u',"u.id=a.url_id","LEFT")->where($where)->count();

        return array('list'=>$list,'count'=>$count,'p'=>$p);
    }
    
    
}