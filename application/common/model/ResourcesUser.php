<?php



namespace app\common\model;



use think\Cache;

use think\Model;

use think\Page;

use think\Db;

/**

 * 资源数据模型

 */

class ResourcesUser extends Model

{

    //列表页面

    public function getResourcesList($where, $limit = 10, $p = 1, $order = "a.createtime desc")
    {



        $model = Db::name('Resources_user');

        $field = 'a.info,a.gl_status,a.is_idle,a.is_recommend,a.is_top,a.id,a.rid,a.is_del,a.status,a.remarks,a.createtime,a1.number,a1.title,a1.cover,a1.video,a1.content,a1.address,b.area_name,c.category_name,d.nickname as cjname';

        //->Join('admin u',"u.uid=a.cuid","LEFT")

        $list = $model->alias('a')->Join('resources a1', "a1.rid=a.rid", "left")->Join('area b', "b.id=a1.area_id", "LEFT")->Join('category c', "c.id=a1.category_id", "LEFT")->Join('admin d', "d.id=a1.cuid", "LEFT")->field($field)->where($where)->order($order)->limit($limit)->page($p)->select();

        $count = $model->alias('a')->Join('resources a1', "a1.rid=a.rid", "left")->Join('area b', "b.id=a1.area_id", "LEFT")->Join('category c', "c.id=a1.category_id", "LEFT")->Join('admin d', "d.id=a1.cuid", "LEFT")->where($where)->count();

        // var_dump($count);exit;

        $new_list = array();
        $rid_arr = [];

        // var_dump($list);exit;

        if ($list) {
            foreach ($list as $k => $v) {
                if ($v['rid']) {
                    $rid_arr[] = $v['rid'];
                }
            }
            //var_dump($list,$list_data);exit;

            if ($rid_arr) {
                $where_user['a.rid'] = array('in', join(',', $rid_arr));
                //$where_user['a.gl_status']=1;
                $field = 'a.rid,a.gl_status,a.uid,b.username,b.nickname';
                $list_data = db::name('resources_user')->alias('a')->Join('admin b', "a.uid=b.id", "LEFT")->field($field)->where($where_user)->select();
                foreach ($list as $k1 => $v1) {
                    $v1['dl_name_list'] = '';
                    if ($list_data) {
                        foreach ($list_data as $k2 => $v2) {
                            if ($v1['rid'] == $v2['rid'] && $v2['gl_status'] == 1) {
                                $v1['dl_name_list'] .= $v2['username'] . ',';
                            }
                        }
                    }

                    $new_list[$k1] = $v1;
                }
            }
        }



        return array('list' => $new_list, 'count' => $count, 'p' => $p);
    }

    //代理资源-详情页面查询

    public function getResourcesInfo($where)
    {

        $model = Db::name('Resources_user');

        $field = 'a.is_top,a.is_recommend,a.is_idle,a.id,a.is_del,a.status,a.remarks,a.end_time,a.createtime,a.info,a1.rid,a1.number,a1.title,a1.cover,a1.video,a1.content,a1.address,a1.category_id,a1.area_id,a1.pz_id';



        $info = $model->alias('a')->Join('resources a1', "a1.rid=a.rid", "LEFT")->field($field)->where($where)->find();

        return $info;
    }

    //列表页面

    public function getResourcesLists($where)
    {



        $model = Db::name('Resources_user');

        $field = 'a.id,a.is_del,a1.*,b.area_name,c.category_name';

        $list = $model->alias('a')->Join('resources a1', "a1.rid=a.rid", "LEFT")->Join('area b', "b.id=a1.area_id", "LEFT")->Join('category c', "c.id=a1.category_id", "LEFT")->field($field)->where($where)->select();



        return $list;
    }

    //资源关联表查询

    public function getglInfo($where)
    {

        $model = Db::name('Resources_user');

        $info = $model->where($where)->find();

        return $info;
    }

    /**

     * 批量上下架

     */

    public function saveStatus($where, $savedata)
    {

        $model = Db::name('Resources_user');

        $res = $model->where($where)->setField($savedata);

        return $res;
    }

    /**

     * 域名列表

     */

    public function getUrlLists($uid)
    {

        $where['status'] = 1;

        $model = Db::name('url');

        $field = '*';

        $lists = $model->alias('a')->field($field)->where($where)->select();

        //得到归属url

        if ($lists && GID != 1) {

            foreach ($lists as $k => $v) {

                if (GID != 1 && $v['dl_uid']) {

                    $dl_uid = explode(',', $v['dl_uid']);

                    if (in_array($uid, $dl_uid)) {

                        $list[] = $v;
                    }
                }
            }
        } else {

            $list = $lists;
        }

        return $list;
    }
}
