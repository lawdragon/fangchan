<?php



namespace app\common\model;



use think\Cache;

use think\Model;

use think\Page;

use think\Db;

/**

 * 资源数据模型

 */

class Resources extends Model

{

  //列表页面

  public function getResourcesList($where = "1=1", $limit = 10, $p = 1, $order = "a.createtime desc", $uid = 0)
  {

    // ru.is_top,
    //     ru.is_idle,
    //     ru.is_recommend,
    //     ru.status,
    //     ru.is_del,
    //     ru.gl_status,
    //     ru.rid as ru_rid,

    //     a.rid as id,
    //     a.end_time,
    //     a.number,
    //     a.title,
    //     a.address,
    //     a.video,
    //     a.content,
    //     FROM_UNIXTIME(a.createtime,'%Y-%m-%d %H:%i:%s') as create_time,
    //     a.remarks,
    //     a.cover,
    //     a.pz_id,
    //     b.area_name as region,
    //     c.category_name as classify";

    $model = Db::name('resources_user');

    $field = 'ru.is_top,ru.is_idle,ru.is_recommend,a.*,b.area_name,c.category_name,d.nickname as gx_name,e.gl_is_del,f.nickname as cjname';

    //->Join('admin u',"u.uid=a.cuid","LEFT")
    $list = $model->alias('ru')->join('resources a', "ru.rid = a.rid", "left")->join('area b', "b.id=a.area_id", "LEFT")->Join('category c', "c.id=a.category_id", "LEFT")->Join('admin d', "a.gx_uid=d.id", "LEFT")->Join('resources_del e', "e.rid=a.rid and e.uid={$uid}", "LEFT")->Join('admin f', "f.id=a.cuid", "LEFT")->field($field)->where($where)->order($order)->limit($limit)->page($p)->select();

    $count = $model->alias('ru')->join('resources a', "ru.rid = a.rid", "left")->Join('area b', "b.id=a.area_id", "LEFT")->Join('category c', "c.id=a.category_id", "LEFT")->Join('admin f', "f.id=a.cuid", "LEFT")->where($where)->count();

    // echo '<pre>';
    // print_r($list);
    // exit;

    $new_list = array();

    // if ($list) {

    //   foreach ($list as $k => $v) {

    //     if ($v['rid']) {

    //       $rid_arr[] = $v['rid'];
    //     }
    //   }

    //   //var_dump($list,$list_data);exit;

    //   if ($rid_arr) {

    //     $where_user['a.rid'] = array('in', join(',', $rid_arr));

    //     //$where_user['a.gl_status']=1;

    //     $field = 'a.rid,a.gl_status,a.info,a.uid,b.username,b.nickname';

    //     $list_data = db::name('resources_user')->alias('a')->Join('admin b', "a.uid=b.id", "LEFT")->field($field)->where($where_user)->select();

    //     foreach ($list as $k1 => $v1) {

    //       if ($v1['gx_time']) {

    //         $v1['gx_time'] = date('Y-m-d H:i:s', $v1['gx_time']);
    //       }

    //       if ($v1['status'] == 0) {

    //         $v1['status_name'] = '下架';
    //       } else {

    //         $v1['status_name'] = '上架';
    //       }

    //       $v1['dl_name_list'] = '';

    //       $v1['info'] = '';

    //       if ($list_data) {

    //         foreach ($list_data as $k2 => $v2) {

    //           if ($v1['rid'] == $v2['rid'] && $v2['gl_status'] == 1) {

    //             $v1['dl_name_list'] .= $v2['username'] . ',';
    //           }

    //           if ($v1['rid'] == $v2['rid'] && $v2['uid'] == $uid) {

    //             $v1['info'] = $v2['info'];
    //           }
    //         }
    //       }

    //       $new_list[$k1] = $v1;
    //     }
    //   }
    // }

    //var_dump($list);exit; 

    return array('list' => $list, 'count' => $count, 'p' => $p);
  }


  public function getResourcesListForIndex($where = "1=1", $limit = 10, $p = 1, $order = "a.createtime desc", $uid = 0)
  {
    $model = Db::name('resources');

    $field = 'ru.is_top,ru.is_idle,ru.uid,ru.is_recommend,a.*,b.area_name,c.category_name,d.nickname as gx_name,e.gl_is_del,f.nickname as cjname';

    // $where['ru.uid'] = $uid;

    //->Join('admin u',"u.uid=a.cuid","LEFT")
    $list = $model->alias('a')->join('resources_user ru', "ru.rid = a.rid and ru.uid  = {$uid}", "left")->join('area b', "b.id=a.area_id", "LEFT")->Join('category c', "c.id=a.category_id", "LEFT")->Join('admin d', "a.gx_uid=d.id", "LEFT")->Join('resources_del e', "e.rid=a.rid and e.uid={$uid}", "LEFT")->Join('admin f', "f.id=a.cuid", "LEFT")->field($field)->where($where)->order($order)->limit($limit)->page($p)->select();
    $count = $model->alias('a')->join('resources_user ru', "ru.rid = a.rid and ru.uid  = {$uid}", "left")->Join('area b', "b.id=a.area_id", "LEFT")->Join('category c', "c.id=a.category_id", "LEFT")->Join('admin f', "f.id=a.cuid", "LEFT")->where($where)->count();
    // echo '<pre>';
    // print_r($list);
    // exit;

    $new_list = array();
    $rid_arr = [];
    if ($list) {
      foreach ($list as $k => $v) {
        if ($v['rid']) {
          $rid_arr[] = $v['rid'];
        }
      }

      //var_dump($list,$list_data);exit;

      if (!empty($rid_arr)) {

        $where_user['a.rid'] = array('in', join(',', $rid_arr));

        //$where_user['a.gl_status']=1;

        $field = 'a.rid,a.gl_status,a.info,a.uid,b.username,b.nickname';

        $list_data = db::name('resources_user')->alias('a')->Join('admin b', "a.uid=b.id", "LEFT")->field($field)->where($where_user)->select();

        foreach ($list as $k1 => $v1) {
          if ($v1['gx_time']) {
            $v1['gx_time'] = date('Y-m-d H:i:s', $v1['gx_time']);
          }
          if ($v1['status'] == 0) {
            $v1['status_name'] = '下架';
          } else {
            $v1['status_name'] = '上架';
          }

          $v1['dl_name_list'] = '';
          $v1['info'] = '';
          if ($list_data) {
            foreach ($list_data as $k2 => $v2) {
              if ($v1['rid'] == $v2['rid'] && $v2['gl_status'] == 1) {
                $v1['dl_name_list'] .= $v2['username'] . ',';
              }
              if ($v1['rid'] == $v2['rid'] && $v2['uid'] == $uid) {
                $v1['info'] = $v2['info'];
              }
            }
          }
          $new_list[$k1] = $v1;
        }
      }
    }

    //var_dump($list);exit; 

    return array('list' => $new_list, 'count' => $count, 'p' => $p);
  }
  //关联列表页面

  public function getResourcesglList($where = "1=1", $limit = 10, $p = 1, $order = "a.createtime desc")
  {



    $model = Db::name('resources_user');

    $field = 'a.*,b.area_name,c.category_name';

    //->Join('admin u',"u.uid=a.cuid","LEFT")

    $list = $model->alias('a1')->Join('resources a', "a.rid=a1.rid", "LEFT")->Join('area b', "b.id=a.area_id", "LEFT")->Join('category c', "c.id=a.category_id", "LEFT")->field($field)->where($where)->order($order)->limit($limit)->page($p)->select();

    $count = $model->alias('a1')->Join('resources a', "a.rid=a1.rid", "LEFT")->Join('area b', "b.id=a.area_id", "LEFT")->Join('category c', "c.id=a.category_id", "LEFT")->where($where)->count();



    return array('list' => $list, 'count' => $count, 'p' => $p);
  }

  //更新列表页面

  public function getResourcesgxList($where = "1=1", $limit = 10, $p = 1, $order = "a.createtime desc", $uid = 0)
  {



    $model = Db::name('pb');

    $field = 'a.*,b.area_name,c.category_name,d.nickname as cjname';

    //->Join('admin u',"u.uid=a.cuid","LEFT")

    $list = $model->alias('a1')->Join('Resources a', "a.rid=a1.rid", "LEFT")->Join('area b', "b.id=a.area_id", "LEFT")->Join('category c', "c.id=a.category_id", "LEFT")->Join('admin d', "d.id=a.cuid", "LEFT")->field($field)->where($where)->order($order)->limit($limit)->page($p)->select();

    $count = $model->alias('a1')->Join('Resources a', "a.rid=a1.rid", "LEFT")->Join('area b', "b.id=a.area_id", "LEFT")->Join('category c', "c.id=a.category_id", "LEFT")->Join('admin d', "d.id=a.cuid", "LEFT")->where($where)->count();

    $rid_arr = array();

    $new_list = array();

    //var_dump($list,$where);exit;

    if ($list) {

      foreach ($list as $k => $v) {

        if ($v['rid']) {

          $rid_arr[] = $v['rid'];
        }
      }

      //var_dump($list,$list_data);exit;

      if ($rid_arr) {

        $where_user['a.rid'] = array('in', join(',', $rid_arr));

        //$where_user['a.gl_status']=1;



        $field = 'a.rid,a.gl_status,a.info,a.uid,b.username,b.nickname';

        $list_data = db::name('resources_user')->alias('a')->Join('admin b', "a.uid=b.id", "LEFT")->field($field)->where($where_user)->select();



        $where_pb['a.rid'] = array('in', join(',', $rid_arr));

        $where_pb['a.status'] = 1;

        $field = 'a.rid,a.pb_time,b.username,b.nickname';

        $list_datas = db::name('pb')->alias('a')->Join('admin b', "a.uid=b.id")->field($field)->where($where_pb)->select();



        if ($list_data) {

          foreach ($list as $k1 => $v1) {

            $v1['dl_name_list'] = '';

            $v1['gx_name'] = '';



            $v1['info'] = '';

            if ($list_data) {

              foreach ($list_data as $k2 => $v2) {

                if ($v1['rid'] == $v2['rid'] && $v2['gl_status'] == 1) {

                  $v1['dl_name_list'] .= $v2['username'] . ',';
                }

                if ($v1['rid'] == $v2['rid'] && $v2['uid'] == $uid) {

                  $v1['info'] = $v2['info'];
                }
              }
            }

            if ($list_datas) {

              foreach ($list_datas as $k3 => $v3) {

                if ($v1['rid'] == $v3['rid']) {

                  /*if($v3['pb_time']){

                                        $v1['gx_time']=date('Y-m-d ',$v3['pb_time']);

                                    }*/

                  $v1['gx_name'] = $v3['username'];
                }
              }
            }

            if ($v1['gx_time']) {

              $v1['gx_time'] = date('Y-m-d H:i:s', $v1['gx_time']);
            } else {

              $v1['gx_time'] = '';
            }



            $new_list[$k1] = $v1;
          }

          //var_dump($new_list);exit;

        } else {

          foreach ($list as $k1 => $v1) {

            $v1['dl_name_list'] = '';

            $v1['gx_name'] = '';

            $v1['gx_time'] = '';

            $new_list[$k1] = $v1;
          }
        }
      }
    }



    return array('list' => $new_list, 'count' => $count, 'p' => $p);
  }

  //资源关联表查询

  public function getglInfo($where)
  {

    $model = Db::name('Resources_user');

    $info = $model->where($where)->find();

    return $info;
  }

  /**

   * 批量上下架

   */

  public function saveStatus($where, $savedata)
  {

    $model = Db::name('Resources');
    // $model = Db::name('resources_user');
    $res = $model->where($where)->setField($savedata);
    return $res;
  }

  /**

   * 域名列表

   */

  public function getUrlLists($uid)
  {

    $where['status'] = 1;

    $model = Db::name('url');

    $field = '*';

    $lists = $model->alias('a')->field($field)->where($where)->select();

    //得到归属url

    $list = [];

    if ($lists && GID != 1) {

      foreach ($lists as $k => $v) {

        if (GID != 1 && $v['dl_uid']) {

          $dl_uid = explode(',', $v['dl_uid']);

          if (in_array($uid, $dl_uid)) {

            $list[] = $v;
          }
        }
      }
    } else {

      $list = $lists;
    }

    return $list;
  }
}
