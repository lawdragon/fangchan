<?php

namespace app\admin\controller;

use app\common\controller\Backend;
use app\common\model\Resources as ResourcesModel;
use fast\Tree;
use think\Db;
use think\Log;
use think\Request;

/**
 * 资源管理
 *
 * @icon   fa fa-list
 * @remark
 */
class Resources extends Backend
{
    protected $model = null;
    protected  $ResourcesModel= null;
    public function _initialize()
    {
        parent::_initialize();
        $this->model = model('app\common\model\Resources');
        $this->ResourcesModel=new ResourcesModel();

        $categoryList=$this->getCategoryLists(array('a.status'=>1));
        $getAreaList=$this->getAreaLists(array('a.status'=>1));
        //var_dump($getAreaList);exit;
        $getPzList=$this->getPzLists(array('a.status'=>1));
        if($getPzList){
            $getPzLists = [];
            foreach($getPzList as $k=>$v){
                $getPzLists[$v['id']] = $v['name'];
            }
        }
        $this->view->assign("pzList", $getPzLists);
        $this->view->assign("categoryList", $categoryList);
        $this->view->assign("areaList", $getAreaList);
        $this->view->assign("gid", GID);
        //var_dump(GID);exit;
    }
    /**
     * 查看
     */
    public function index(){
        //设置过滤方法
        $this->request->filter(['strip_tags']);
        $params = $this->request->get();
        if ($this->request->isAjax()) {
            $where['a.is_del']=0;
            if(GID==2){
                //$where['a.status']=1;
            }
            if(isset($params['filter'])){
                $filter=json_decode($params['filter'],true);
                //编号
                if(isset($filter['number'])){
                    $where['a.number']=array('like','%'.$filter['number'].'%');
                }
                if(isset($filter['rid'])){
                    $where['a.rid']=$filter['rid'];
                }
                //标题
                if(isset($filter['title'])){
                    $where['a.title']=array('like','%'.$filter['title'].'%');
                }
                //分类
                if(isset($filter['category_name'])){
                    $where['a.category_id']=$filter['category_name'];
                }
                //地区
                if(isset($filter['area_name'])){
                    $where['a.area_id']=$filter['area_name'];
                }
                //代理
                if(isset($filter['dl_name_list'])){
                    //$where['a.cuid']=$filter['nickname'];
                    $whereuser['gl_status']=1;
                    $whereuser['status']=1;
                    $whereuser['is_del']=0;
                    $whereuser['uid']=$filter['dl_name_list'];
                    $Resources_user_list=db::name('Resources_user')->where($whereuser)->select();
                    $user_rid=array();
                    if($Resources_user_list){
                       foreach($Resources_user_list as $kuser=>$vuser){
                           if($vuser['rid']){
                                $user_rid[]=$vuser['rid'];
                           }
                       }
                       if($user_rid){
                           $where['a.rid']=array('in',join(',',$user_rid));
                       }
                    }
                }
                if(isset($filter['glstatus'])){
                        //$whereuser['a.status']=1;
                        $whereuser['a.is_del']=0;
                        //$whereuser['a1.gl_status']=1;
                        //$whereuser['a1.status']=1;
                        //$whereuser['a1.is_del']=0;//Resources_user
                        $Resources_user_list=db::name('Resources')->alias('a')->Join('Resources_user a1',"a1.rid=a.rid","LEFT")->field('a.rid,a1.uid,a1.id,a1.gl_status,a1.status,a1.is_del,a1.info')->where($whereuser)->select();
                        //var_dump($Resources_user_list);exit;
                        $user_rid1=array();
                        if($Resources_user_list){
                            foreach($Resources_user_list as $kuser=>$vuser){

                                if($vuser['rid'] && $filter['glstatus']==1 && $vuser['uid'] && $vuser['gl_status']==1 && $vuser['status']==1 && $vuser['is_del']==0){
                                    $user_rid1[]=$vuser['rid'];
                                }elseif($vuser['rid'] && $filter['glstatus']==2  && $vuser['gl_status']!=1){
                                    $user_rid1[]=$vuser['rid'];
                                }
                            }
                            if($user_rid1){
                                $where['a.rid']=array('in',join(',',$user_rid1));
                            }else{
                                $where['a.rid']='-1';
                            }
                        }
                }
                //创建人
                if(isset($filter['cjname'])){
                    $where['a.cuid']=$filter['cjname'];
                }
                //配置
                if(isset($filter['pz_id'])){
                    //$where['a.pz_id']=$filter['pz_id'];
                    $where['a.pz_id']=['exp',DB::raw(" REGEXP '".$filter['pz_id']."'")];
                    //var_dump($where);exit;
                }
                //置顶
                if(isset($filter['is_top'])){
                    $where['a.is_top']=$filter['is_top'];
                }
                //推荐
                if(isset($filter['is_recommend'])){
                    $where['a.is_recommend']=$filter['is_recommend'];
                }
                //上下架状态
                if(isset($filter['status'])){
                    $where['a.status']=$filter['status'];
                }
                //创建时间
                if(isset($filter['createtime'])){
                    $createtime=explode(' - ',$filter['createtime']);
                    if($createtime[0] && $createtime[1]){
                        $where['a.createtime']=array(array('egt',strtotime($createtime[0])),array('elt',strtotime($createtime[1])),'and');
                    }
                }

            }

            if(isset($params['limit'])){
                $limit=$params['limit'];
            }else{
                $limit=10;
            }
            if(!isset($params['offset']) || $params['offset']==0){
                $p=1;
            }else{
                $p=($params['offset']/$params['limit'])+1;
            }
            if($params['sort'] && $params['order']){
                $order='a.'.$params['sort'].' '.$params['order'];
            }
            //var_dump($where);exit;
            if(isset($where)){
                $data= $this->ResourcesModel->getResourcesListForIndex($where,$limit,$p,$order,$this->auth->id);
            }else{
                $data= $this->ResourcesModel->getResourcesListForIndex('',$p,$order,$this->auth->id);
            }

            if($data['list']){
                foreach($data['list'] as $k=>$v){
                    $v['gid']=GID;
                    $data['list'][$k]=$v;
                }
            }
            $result = array("total" => $data['count'], "rows" => $data['list'],'gid'=>GID);

            return json($result);
        }
        return $this->view->fetch();
    }
    public function index_sss(){
        //设置过滤方法
        $this->request->filter(['strip_tags']);


        $params = $this->request->get();

        if(!isset($params['number'])){
            $params['number']='';
        }
        if(!isset($params['title'])){
            $params['title']='';
        }
        if(!isset($params['status'])){
            $params['status']='-1';
        }
        if(!isset($params['category'])){
            $params['category']='';
        }
        if(!isset($params['area'])){
            $params['area']='';
        }
        $this->view->assign("params", $params);
        if ($this->request->isAjax()) {
            $where['a.is_del']=0;
            if(GID==2){
                $where['a.status']=1;
            }

            if(isset($params['title']) && $params['title']){
                $where['a.title']=array('like','%'.$params['title'].'%');
            }
            if(isset($params['number']) && $params['number']){
                $where['a.number']=array('like','%'.$params['number'].'%');
            }
            if(isset($params['status']) && $params['status']!='-1' && $params['status']){
                $where['a.status']=$params['status'];
            }
            if(isset($params['category']) && $params['category']){
                $where['a.category_id']=$params['category'];
            }
            if(isset($params['area']) && $params['area']){
                $where['a.area_id']=$params['area'];
            }

            if(isset($params['limit'])){
                $limit=$params['limit'];
            }else{
                $limit=10;
            }
            if(!isset($params['offset']) || $params['offset']==0){
                $p=1;
            }else{
                $p=($params['offset']/$params['limit'])+1;
            }
            if($params['sort'] && $params['order']){
                $order='a.'.$params['sort'].' '.$params['order'];
            }
            //var_dump($where);exit;
            if(isset($where)){
                $data=$this->ResourcesModel->getResourcesList($where,$limit,$p,$order);
            }else{
                $data=$this->ResourcesModel->getResourcesList('',$limit,$p,$order);
            }

            if($data['list']){
                foreach($data['list'] as $k=>$v){
                    $v['gid']=GID;
                    $data['list'][$k]=$v;
                }
            }
            $result = array("total" => $data['count'], "rows" => $data['list'],'gid'=>GID);

            return json($result);
        }
        return $this->view->fetch();
    }
    /**
     * 添加
     */
    public function add(){
        if ($this->request->isPost()) {
            $this->token();
            $params = $this->request->post("row/a");
            //var_dump($params);exit;
            if ($params) {
                $params = $this->preExcludeFields($params);

                /*if(empty($add_data['Resources_name'])){
                    $this->error('区域名称不能为空！');
                }*/
                /*if(empty($add_data['Resources_sort'])){
                    $this->error('排序不能为空！');
                }*/
                /*$info=db::name('Resources')->field('rid')->where(['title'=>$params['title']])->find();
                if($info){
                    $this->error('资源名称已存在！');
                }*/
                $add_data['pz_id']='';
                if($params['pz_id']){
                    $add_data['pz_id']=join(',',$params['pz_id']);
                }
                $add_data['number']=$this->numberNo(1,$params['category_id']);//$params['number'];
                $add_data['title']=trim($params['title']);

                $add_data['cover']=$params['cover'];
                $add_data['video']=$params['video'];
                $add_data['content']=$params['img_list'];
                //$add_data['content']=isset($_POST['editorValue'])?$_POST['editorValue']:'';
                $add_data['address']=isset($params['address'])?$params['address']:'';
                if(GID==1){
                    $add_data['remarks'] = $params['remarks'];
                }
                $add_data['end_time']= isset($params['end_time']) ? strtotime($params['end_time']):'';

                $add_data['category_id']=$params['category_id'];
                $add_data['area_id']=$params['area_id'];
                $add_data['is_top']=$params['is_top'];
                $add_data['is_recommend']=$params['is_recommend'];
                $add_data['status']=$params['status'];
                $add_data['createtime']=time();
                $add_data['cuid']=$this->auth->id;
                $rid=db::name('Resources')->insertGetId($add_data);

                if ($rid !== false) {
                    //增加排班关系
                    if(GID!=1){
                        $add_data_pb['rid']=$rid;
                        $add_data_pb['uid']=$this->auth->id;
                        $add_data_pb['pb_time']=time();
                        $add_data_pb['status']=1;
                        Db::name('pb')->insert($add_data_pb);
                        /*$add_data1['pb_uid']=$this->auth->id;
                        $add_data1['pb_status']=1;
                        $add_data1['pb_time']=time();*/
                    }
                    // 增加归属关系
                    $add_data1['info']=$params['info'];
                    $add_data1['remarks']=$params['remarks'];
                    $add_data1['is_top']=$params['is_top'];
                    $add_data1['is_idle']=$params['is_idle']; // 是否繁忙
                    $add_data1['is_recommend']=$params['is_recommend'];
                    $add_data1['status']=$params['status'];
                    $add_data1['rid']=$rid;
                    $add_data1['gl_status']=1;
                    $add_data1['createtime']=time();
                    $add_data1['sj_time']=time();
                    $add_data1['uid']=$this->auth->id;
                    // echo '<pre>';
                    // print_r($add_data1);
                    // exit;
                    // $result=Db::name('Resources_user')->insert($add_data1);
                  
                    $this->success();
                } else {
                    $this->error('保存失败！');
                }
            }
            $this->error(__('Parameter %s can not be empty', ''));
        }
        return $this->view->fetch();
    }
    /**
     * 资源编号
     * @param $type
     * @return string
     */
    /*public function numberNo($first,$type)
    {

        $count =db::name('Resources')->where(['category_id'=>$type])->count();
        $num = $count ? $count + 1 : 1;
        return  $type.'000'.$num;
    }*/
    /**
     * 编辑
     */
    public function edit($ids = null)
    {
        $row = $this->model->get($ids);
        if (!$row) {
            $this->error(__('No Results were found'));
        }
        if($row['end_time']>1){
            $row['end_time']=date('Y-m-d H:i:s',$row['end_time']);
        }else{
            $row['end_time']='';
        }
        $adminIds = $this->getDataLimitAdminIds();
        if (is_array($adminIds)) {
            if (!in_array($row[$this->dataLimitField], $adminIds)) {
                $this->error(__('You have no permission'));
            }
        }
        if ($this->request->isPost()) {
            $this->token();
            $params = $this->request->post("row/a");
            if ($params) {
                $params = $this->preExcludeFields($params);

                $add_data['pz_id']='';
                if($params['pz_id']){
                    $add_data['pz_id']=join(',',$params['pz_id']);
                }
                if($row['category_id']!=$params['category_id']){
                    $add_data['number']=$this->numberNo(1,$params['category_id']);
                }
                /*$info=db::name('Resources')->field('rid')->where(['title'=>$params['title']])->select();
                if($info){
                    foreach($info as $k=>$v){
                        if($v['rid'] && $v['rid']!=$params['mid']){
                            $this->error('资源名称已存在！');
                            break;
                        }
                    }
                }*/
                $add_data['title']=trim($params['title']);

                $add_data['cover']=$params['cover'];
                $add_data['video']=$params['video'];
                $add_data['content']=$params['img_list'];
                //$add_data['content']=isset($_POST['editorValue'])?$_POST['editorValue']:'';
                $add_data['address']=isset($params['address'])?$params['address']:'';
                $add_data['remarks']=$params['remarks'];
                $add_data['end_time']=isset($params['end_time'])?strtotime($params['end_time']):'';

                $add_data['category_id']=$params['category_id'];
                $add_data['area_id']=$params['area_id'];
                $add_data['is_top']=$params['is_top'];
                $add_data['is_recommend']=$params['is_recommend'];
                $add_data['status']=$params['status'];
                $where['rid']=$params['mid'];
                $result=$this->model->where($where)->setField($add_data);
                if ($result !== false) {
                    $this->success();
                } else {
                    $this->error('保存失败！');
                }
            }
            $this->error(__('Parameter %s can not be empty', ''));
        }
        //当前代理的资源
        if($row['rid']){
            $resources_user = db::name('resources_user')->where(array('uid'=>$this->auth->id,'rid'=>$row['rid']))->find();
            if ($resources_user) {
                $row['remarks'] = $resources_user['remarks'];
                $row['info'] = $resources_user['info'];
                $row['is_top'] = $resources_user['is_top'];
                $row['is_recommend'] = $resources_user['is_recommend'];
                $row['is_idle'] = $resources_user['is_idle'];
                $row['status'] = $resources_user['status'];
            } else {
                // 没有关联的用默认值
                $row['remarks'] = '';
                $row['info'] = '';
                $row['is_top'] = 0;
                $row['is_recommend'] = 0;
                $row['is_idle'] = 0;
            }
            
            // if($resources_user['status']){
            //     $row['status'] = $resources_user['status'];
            // }
        }

        $this->view->assign("row", $row);
        return $this->view->fetch();
    }

    /**
     * 资源更新列表
     */
    public function gxlist(){
        //设置过滤方法
        $this->request->filter(['strip_tags']);
        if ($this->request->isAjax()) {
            $where='1=1';
            $data=$this->ResourcesModel->getResourcesList($where);
            $result = array("total" => $data['count'], "rows" => $data['list']);
            return json($result);
        }
        return $this->view->fetch();
    }
    /**
     * 资源关联操作
     */
    public function adopt($ids = null){
        $model = Db::name('Resources_user');
        //判断是否第一个关联资源 第一个关联资源默认排班成功
        $where_pb['uid']=$this->auth->id;
        $where_pb['rid']=$ids?$ids:0;
        $where_pb2['rid']=$ids?$ids:0;
        $model_pb=Db::name('pb');
        $infos=$model_pb->where($where_pb)->find();
        $infos2=$model_pb->where($where_pb2)->find();
        if(!$infos && GID!=1){
            $add_data_pb['rid']=$ids;
            $add_data_pb['uid']=$this->auth->id;
            $add_data_pb['pb_time']=time();
            $add_data_pb['status']=$infos2?0:1;
            $model_pb->insert($add_data_pb);
            /*$add_data['pb_uid']=$this->auth->id;
            $add_data['pb_status']=1;
            $add_data['pb_time']=time();*/
        }
        //判断关联表是否有数据-有开启关联
        //没有数据添加关联数据
        $where['uid']=$this->auth->id;
        $where['rid']=$ids?$ids:0;
        $info=$this->ResourcesModel->getglInfo($where);
        $add_data = [];
        $add_data['status']=1;
        $add_data['gl_status']=1;
        $add_data['createtime']=time();
        $add_data['sj_time']=time();
        $add_data['is_idle'] = 0;
        $add_data['is_top'] = 0;
        $add_data['is_recommend'] = 0;
        if(empty($info)){
            $add_data['rid']=$ids;
            $add_data['uid'] = $this->auth->id;
            // $add_data['status'] = 0;
            // \think\Log::record('测试日志信息');
            $result = $model->insert($add_data);
        } else {
            //$add_data['status']=$info['status'];
            if ($info['gl_status'] == 1) { // 已经关联提示已经关联
                $this->error('请勿重复关联');
            } else {
                // 更新状态
                // $add_data['gl_status']=1;
                // $add_data['is_idle'] = 0;
                // $add_data['is_top'] = 0;
                // $add_data['is_recommend'] = 0;
                $where['rid'] = $info['rid'];
                $result = $model->where($where)->setField($add_data);
            }
            
        }
        $this->success();
    }
    public function adopt_ss($ids = null){
        $model = Db::name('Resources_user');
        //判断是否第一个关联资源 第一个关联资源默认排班成功
        $where_pb['uid']=$this->auth->id;
        $where_pb['rid']=$ids?$ids:0;
        $model_pb=Db::name('pb');
        $infos=$model_pb->where($where_pb)->find();
        if(!$infos && GID!=1){
            $add_data_pb['rid']=$ids;
            $add_data_pb['uid']=$this->auth->id;
            $add_data_pb['pb_time']=time();
            $add_data_pb['status']=1;
            $model_pb->insert($add_data_pb);
            /*$add_data['pb_uid']=$this->auth->id;
            $add_data['pb_status']=1;
            $add_data['pb_time']=time();*/
        }
        //判断关联表是否有数据-有开启关联
        //没有数据添加关联数据
        $where['uid']=$this->auth->id;
        $where['rid']=$ids?$ids:0;
        $info=$this->ResourcesModel->getglInfo($where);
        if(empty($info)){
            $add_data['rid']=$ids;
            //$add_data['status']=$info['status'];
            $add_data['gl_status']=1;
            $add_data['createtime']=time();
            $add_data['sj_time']=time();
            $add_data['uid']=$this->auth->id;
            $result=$model->insert($add_data);
        }else{
            //$add_data['status']=$info['status'];
            $add_data['gl_status']=1;
            $add_data['createtime']=time();
            $add_data['sj_time']=time();
            $where['rid']=$info['rid'];
            $result=$model->where($where)->setField($add_data);
        }
        $this->success();
    }
    /**
     * 资源取消操作
     */
    public function cancel($ids = null){
        $model = Db::name('Resources_user');
        //判断关联表是否有数据-有开启关联
        //没有数据添加关联数据
        $where['uid']=$this->auth->id;
        $where['rid']=$ids?$ids:0;
        $info=$this->ResourcesModel->getglInfo($where);
        if(empty($info)){
            /*$add_data['rid']=$info['rid'];
            $add_data['gl_status']=2;
            $add_data['uid']=$this->auth->id;
            $result=$model->insert($add_data);*/
        }else{
            $add_data['gl_status']=2;
            $where['rid']=$info['rid'];
            $result=$model->where($where)->setField($add_data);
        }
        $this->success();
    }
    /**
     * 刪除操作
     */
    public function del($ids = null){
        $model = Db::name('Resources');
        $where['rid']=$ids?array('in',$ids):0;
        $add_data['is_del']=1;
        $result=$model->where($where)->setField($add_data);

        $this->success();
    }
    /**
     * 一键下架所有归属代理的资源
     */
    public function saveData(){
        $model = Db::name('Resources_user');
        $where['gl_status']=1;
        $data=$model->getResourcesLists($where);
        if($data){
            $rid_arr=array();
            foreach($data as $k=>$v){
                if($v['rid']){
                    $rid_arr[]=$v['rid'];
                }
            }
            //批量下架操作
            $add_data['status']=0;
            $where['rid']=array('in',join(',',$rid_arr));
            $result=$model->where($where)->setField($add_data);
            //批量下架操作
            $add_data['status']=0;
            $where['rid']=array('in',join(',',$rid_arr));
            $result=$this->ResourcesModel->where($where)->setField($add_data);
        }
        $this->success();
    }
    /**
     * 批量上下架
     */
    public function saveStatus(Request $request){
        $params = $request->post();
        $savedata['status']=$params['status'];
        $where['is_del']=0;
        $info=$this->ResourcesModel->saveStatus($where,$savedata);
        echo 1;
    }
    /**
     * 生成链接页面
     */
    public function set_generate_url(Request $request){
        if ($this->request->isPost()) {
            $params = $this->request->post("row/a");
            if ($params) {
                $params = $this->preExcludeFields($params);
                /*if($params['number']){
                    $querys_data['a1.number']=array('like','%'.$params['number'].'%');
                }
                if($params['title']){
                    $querys_data['a1.title']=array('like','%'.$params['title'].'%');
                }
                if($params['category']){
                    $querys_data['a1.category_id']=$params['category'];
                }
                if($params['area']){
                    $querys_data['a1.area_id']=$params['area'];
                }*/
                if($params['rid']){
                    $querys_data['a.rid']=array('in',$params['rid']);
                }
                $querys_data['a.status']=1;

                if(GID!=1){
                    //$querys_data['a1.uid']=$this->auth->id;
                }
                //批量创建链接
                $add_data['pwd_random']=SALT.time();
                if($params['url']){
                    $model = Db::name('resourced_url');
                    for($a=0;$a<count($params['url']);$a++){
                        $add_data['querys']=json_encode($querys_data);
                        $add_data['url']=$params['url'][$a];
                        $add_data['pwd']=$params['password'];
                        $add_data['token']=md5($params['password'] . $add_data['pwd_random']);
                        $add_data['end_time']=isset($params['end_time'])?strtotime($params['end_time']):'';
                        //$add_data['remarks']=$params['remarks'];

                        $add_data['cuid']=$this->auth->id;
                        $add_data['createtime']=time();

                        $url = 'https://zhan.leiue.com/weibo.html';
                        $arr = parse_url($params['url'][$a]);
                        $domain = $arr['host'];
                        $add_data['link_url']=$domain.'?token='.$add_data['token'];

                        $result=$model->insert($add_data);
                    }
                }



                if ($result !== false) {
                    $this->success();
                } else {
                    $this->error('保存失败！');
                }
            }
            $this->error(__('Parameter %s can not be empty', ''));
        }
        $params = $request->get();
        $urlList=$this->ResourcesModel->getUrlLists($this->auth->id);

        $this->view->assign('urlList',$urlList);
        $this->view->assign('params',$params);
        return $this->view->fetch();
    }
    /**
     * 状态修改
     */
    public function multi_save(Request $request){
        $params = $request->post();
        if($params['ids']){
            if($params['params']=='gl_is_del=1' || $params['params']=='gl_is_del=0'){
                $uid=$this->auth->id;
                $where['rid']=$params['ids'];
                $where['uid']=$uid;
                $model=db::name('resources_del');
                $info=$model->where($where)->find();
                if($info){
                    Db::query("UPDATE `ds_resources_del` SET {$params['params']}  WHERE rid={$params['ids']} and uid={$uid}");
                }else{
                    $add_data['gl_is_del']=$params['params']=='gl_is_del=1'?1:0;
                    $add_data['uid']=$uid;
                    $add_data['rid']=$params['ids'];
                    $result=$model->insert($add_data);
                }
            }else{
                Db::query("UPDATE `ds_resources` SET {$params['params']}  WHERE rid={$params['ids']}");
            }
        }
        $this->success();
    }
    public function findName(Request $request){
        $params = $request->post();
        $code=200;
        if($params['title']){
            $where['title']=$params['title'];
            if(!isset($params['mid'])){
                $info=db::name('Resources')->field('rid')->where($where)->find();
                if($info){
                    $code=400;
                }
            }else{
                $info=db::name('Resources')->field('rid')->where($where)->select();
                if($info){
                    foreach($info as $k=>$v){
                        if($v['rid'] && $v['rid']!=$params['mid']){
                            $code=400;
                            break;
                        }
                    }
                }
            }
        }
        return json(array('code'=>$code));
    }
    public function saveResources(){
        $params = $this->request->post();
        $code=200;
        if ($params) {
            $params = $this->preExcludeFields($params);
            // if($params['mid']){
            //     $row = $this->model->get($params['mid']);
            //     if($row['category_id']!=$params['category_id']){
            //         $add_data['number']=$this->numberNo(1,$params['category_id']);
            //     }
            // }else{
            //     $add_data['number']=$this->numberNo(1,$params['category_id']);
            // }
            // 所有的number都用这个
            $add_data['number']=$this->numberNo(1,$params['category_id']);

            $add_data['pz_id']='';
            if(isset($params['pz_id'])){
                $add_data['pz_id']=join(',',$params['pz_id']);
            }

            $add_data['title']=trim($params['title']);

            $add_data['cover']=$params['cover'];
            $add_data['video']=$params['video'];
            $add_data['content']=$params['img_list'];
            //$add_data['content']=isset($_POST['editorValue'])?$_POST['editorValue']:'';
            $add_data['address']=isset($params['address'])?$params['address']:'';
            //$add_data['remarks']=$params['remarks'];
            $add_data['end_time']=isset($params['end_time'])?strtotime($params['end_time']):'';

            $add_data['category_id']=$params['category_id'];
            $add_data['area_id']=$params['area_id'];
            $add_data['is_top']=$params['is_top'];
            $add_data['is_recommend']=$params['is_recommend'];
            $add_data['status']=$params['status'];
            if($params['mid']){
                $where['rid']=$params['mid'];
                $result=$this->model->where($where)->setField($add_data);
            }else{
                $add_data['createtime']=time();
                $add_data['cuid']=$this->auth->id;
                $rid=db::name('Resources')->insertGetId($add_data);
                $result=$rid;
                if ($rid !== false) {
                    //增加排班关系
                    if(GID!=1){
                        $add_data_pb['rid']=$rid;
                        $add_data_pb['uid']=$this->auth->id;
                        $add_data_pb['pb_time']=time();
                        $add_data_pb['status']=1;
                        Db::name('pb')->insert($add_data_pb);
                    }
                    //增加归属关系
                    $add_data1['info']=$params['info'];
                    $add_data1['remarks']=$params['remarks'];
                    $add_data1['rid']=$rid;
                    $add_data1['gl_status']=1;
                    $add_data1['createtime']=time();
                    $add_data1['sj_time']=time();
                    $add_data1['uid']=$this->auth->id;
                    $add_data1['is_idle']=$params['is_idle']; // 是否繁忙
                    $add_data1['is_top']=$params['is_top'];
                    $add_data1['is_recommend']=$params['is_recommend'];
                    $add_data1['status']=$params['status'];
                    $result=Db::name('Resources_user')->insert($add_data1);
                }
            }

            if ($result !== false) {
                $code=200;
            } else {
                $code=400;
            }
        }
        echo json_encode(array('code'=>$code));
    }
}