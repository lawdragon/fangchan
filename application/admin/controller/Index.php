<?php

namespace app\admin\controller;

use app\admin\model\AdminLog;
use app\common\controller\Backend;
use think\Config;
use think\Db;
use think\Hook;
use think\Request;
use think\Session;
use think\Validate;

/**
 * 后台首页
 * @internal
 */
class Index extends Backend
{

    protected $noNeedLogin = ['login'];
    protected $noNeedRight = ['index', 'logout'];
    protected $layout = '';
    protected $token= '';
    public function _initialize()
    {
        parent::_initialize();
        //移除HTML标签
        $this->request->filter('trim,strip_tags,htmlspecialchars');
    }

    /**
     * 后台首页
     */
    public function index()
    {
        $cookieArr = ['adminskin' => "/^skin\-([a-z\-]+)\$/i", 'multiplenav' => "/^(0|1)\$/", 'multipletab' => "/^(0|1)\$/", 'show_submenu' => "/^(0|1)\$/"];
        foreach ($cookieArr as $key => $regex) {
            $cookieValue = $this->request->cookie($key);
            if (!is_null($cookieValue) && preg_match($regex, $cookieValue)) {
                config('fastadmin.' . $key, $cookieValue);
            }
        }
        //左侧菜单
        list($menulist, $navlist, $fixedmenu, $referermenu) = $this->auth->getSidebar([
            'dashboard' => 'hot',
            'addon'     => ['new', 'red', 'badge'],
            'auth/rule' => __('Menu'),
            'general'   => ['new', 'purple'],
        ], $this->view->site['fixedpage']);
        $action = $this->request->request('action');
        if ($this->request->isPost()) {
            if ($action == 'refreshmenu') {
                $this->success('', null, ['menulist' => $menulist, 'navlist' => $navlist]);
            }
        }
        $this->assignconfig('cookie', ['prefix' => config('cookie.prefix')]);
        $this->view->assign('menulist', $menulist);
        $this->view->assign('navlist', $navlist);
        $this->view->assign('fixedmenu', $fixedmenu);
        $this->view->assign('referermenu', $referermenu);
        $this->view->assign('title', __('Home'));
        return $this->view->fetch();
    }

    /**
     * 管理员登录
     */
    public function login()
    {
        $url = $this->request->get('url', 'index/index');
        if ($this->auth->isLogin()) {
            $this->success(__("You've logged in, do not login again"), $url);
        }
        if ($this->request->isPost()) {
            $username = $this->request->post('username');
            $password = $this->request->post('password');
            $keeplogin = $this->request->post('keeplogin');
            $token = $this->request->post('__token__');
            $rule = [
                'username'  => 'require|length:3,30',
                'password'  => 'require|length:3,30',
                '__token__' => 'require|token',
            ];
            $data = [
                'username'  => $username,
                'password'  => $password,
                '__token__' => $token,
            ];
            if (Config::get('fastadmin.login_captcha')) {
                $rule['captcha'] = 'require|captcha';
                $data['captcha'] = $this->request->post('captcha');
            }
            $validate = new Validate($rule, [], ['username' => __('Username'), 'password' => __('Password'), 'captcha' => __('Captcha')]);
            $result = $validate->check($data);
            if (!$result) {
                $this->error($validate->getError(), $url, ['token' => $this->request->token()]);
            }
            AdminLog::setTitle(__('Login'));
            $result = $this->auth->login($username, $password, $keeplogin ? 86400 : 0);
            if ($result === true) {
                Hook::listen("admin_login_after", $this->request);
                $this->success(__('Login successful'), $url, ['url' => $url, 'id' => $this->auth->id, 'username' => $username, 'avatar' => $this->auth->avatar]);
            } else {
                $msg = $this->auth->getError();
                $msg = $msg ? $msg : __('Username or password is incorrect');
                $this->error($msg, $url, ['token' => $this->request->token()]);
            }
        }

        // 根据客户端的cookie,判断是否可以自动登录
        if ($this->auth->autologin()) {
            Session::delete("referer");
            $this->redirect($url);
        }
        $background = Config::get('fastadmin.login_background');
        $background = $background ? (stripos($background, 'http') === 0 ? $background : config('site.cdnurl') . $background) : '';
        $this->view->assign('background', $background);
        $this->view->assign('title', __('Login'));
        Hook::listen("admin_login_init", $this->request);
        return $this->view->fetch();
    }

    /**
     * 退出登录
     */
    public function logout()
    {
        if ($this->request->isPost()) {
            $this->auth->logout();
            Hook::listen("admin_logout_after", $this->request);
            $this->success(__('Logout successful'), 'index/login');
        }
        $html = "<form id='logout_submit' name='logout_submit' action='' method='post'>" . token() . "<input type='submit' value='ok' style='display:none;'></form>";
        $html .= "<script>document.forms['logout_submit'].submit();</script>";

        return $html;
    }
    //提交密码
    public function verifyPassword2(Request $request){
        if ($this->request->isPost()) {
            $password = $this->request->post('password');
            $token = $this->request->get('token');
            $accessPassword=md5($password . SALT);
            if(empty($token) || empty($accessPassword) || $token!=$accessPassword){
                $arr=array('data'=>'','message'=>'error','statusCode'=>500,'timestamp'=>time());
                return json($arr);
            }else{
                cookie('access_password', $accessPassword, [
                    'expire' => 12*3600,
                ]);
            }
        }
        return 'success';
    }
    public function index2(Request $request)
    {
        $categoryList=$this->getCategoryLists(array('a.status'=>1));
        $getAreaList=$this->getAreaLists(array('a.status'=>1));
        $classify=array();
        if($categoryList){
            foreach($categoryList as $k=>$v){
                //$classify[$v['id']]=$v['category_name'];
                $classify[]=array('id'=>$v['id'],'name'=>$v['category_name']);
            }
        }
        $region=array();
        if($getAreaList){
            foreach($getAreaList as $k1=>$v1){
                //$region[$v1['id']]=$v1['area_name'];
                $region[]=array('id'=>$v1['id'],'name'=>$v1['area_name']);
            }
        }
        $this->token = $this->request->post('token');
        return json([
            'token' => $this->token,
            'notice' => '内部联盟资源，请勿外传！',
            'classify' => $classify,
            'region' => $region
        ]);
    }
    /*
         address: "松茂御龙湾"
classify: "一室"
cover: "https://s1011.aihuifu.cn/data2/f1/10011/101.zip"
end_time: ""
id: 4
is_recommend: 0
region: "宝安"
remarks: ""
title: "御龙湾57平"*/
    public function list2(Request $request)
    {
        $this->token = $this->request->post('token');
        $new = $this->request->post('new');//0 上新 1 今日上新 2.3日上新
        $classify = $this->request->post('classify');//分类
        $region = $this->request->post('region');//地区
        $page = $this->request->post('page');//分页
        $recommend = $this->request->post('recommend');//0 推荐 1 好评推荐
        $value = $this->request->post('value');//搜索值
        $where['status']=1;
        if (in_array($new = $this->request->post('new/d', 0), [1, 2])) {
            $todayTime = strtotime(date('Ymd')) - 1;
            $where[] = ['create_time', 'between', [$new === 1 ? $todayTime + 1 : $todayTime + 1 - 86400 * 3, $todayTime + 86400]];
        }
        $res['data']=[
            'currentPage' => 1,//分页
            'data' => array(),//列表数据
            'total' => 25,//总条数
            'totalPages' => 3,//总分页数
            'message' => 'success',//状态
            'statusCode' => 200,//状态码
            'timestamp' => time(),//时间戳
        ];
        return json($res);

    }
    /*
     address: "松茂御龙湾"
classify: "一室"
content: "<p><img src="https://s1011.aihuifu.cn/data2/f1/10011/101.zip"/></p><p><img src="https://s1011.aihuifu.cn/data2/f1/10011/102.zip"/></p><p><img src="https://s1011.aihuifu.cn/data2/f1/10011/103.zip"/></p><p><img src="https://s1011.aihuifu.cn/data2/f1/10011/104.zip"/></p><p><img src="https://s1011.aihuifu.cn/data2/f1/10011/105.zip"/></p>"
cover: "https://s1011.aihuifu.cn/data2/f1/10011/101.zip"
create_time: 1679464767
end_time: ""
id: 4
is_recommend: 0
is_top: 0
number: "10011"
region: "宝安"
remarks: ""
status: 1
title: "御龙湾57平"
video: "https://s1011.aihuifu.cn/data2/f1/10011/201.7z"
message: "success"
statusCode: 200
timestamp: 1685864716
     * */
    //实时验证token的方法和进入详情时的 访问统计
    public function details2(Request $request){
        $this->token = $this->request->post('token');
        $id = $this->request->post('id');//资源id
        $res['data']=[
            'address' => '松茂御龙湾',
            'classify' => "一室",
            'content' => '<p><img src="https://s1011.aihuifu.cn/data2/f1/10011/101.zip"/></p><p><img src="https://s1011.aihuifu.cn/data2/f1/10011/102.zip"/></p><p><img src="https://s1011.aihuifu.cn/data2/f1/10011/103.zip"/></p><p><img src="https://s1011.aihuifu.cn/data2/f1/10011/104.zip"/></p><p><img src="https://s1011.aihuifu.cn/data2/f1/10011/105.zip"/></p>',
            'cover' => "https://s1011.aihuifu.cn/data2/f1/10011/101.zip",
            'create_time' => 1679464767,
            'end_time' => "",
            'id' => 5,
            'is_recommend' => 0,
            'is_top' => 0,
            'number' => "10011",
            'region' => "宝安",
            'remarks' =>  "",
            'status' =>  1,
            'title' =>  "御龙湾57平",
            'video' =>  "https://s1011.aihuifu.cn/data2/f1/10011/201.7z",
            'message' => 'success',//状态
            'statusCode' => 200,//状态码
            'timestamp' => time(),//时间戳
        ];
        //判断今日IP是否重复进线
        $time=strtotime(date('Y-m-d',time()));
        $ip=$this->getip();
        $where_log['ip']=$ip;
        $where_log['rid']=$res['data']['id'];
        $where_log['url_id']=1;
        $where_log['cuid']=1;
        $where_log['createtime']=array('GT',$time);
        $model = Db::name('access_log');
        $field='*';
        $info =$model->alias('a')->field($field)->where($where_log)->find();
        if(!$info){
            $add_log['ip']=$ip;
            $add_log['rid']=$res['data']['id'];
            $add_log['url_id']=1;
            $add_log['cuid']=1;
            $add_log['createtime']=time();
            $result=$model->insert($add_log);
        }
        return json($res);
    }
    public function getip() {

        static $ip = '';

        $ip = $_SERVER['REMOTE_ADDR'];

        if(isset($_SERVER['HTTP_CDN_SRC_IP'])) {

            $ip = $_SERVER['HTTP_CDN_SRC_IP'];

        } elseif (isset($_SERVER['HTTP_CLIENT_IP']) && preg_match('/^([0-9]{1,3}\.){3}[0-9]{1,3}$/', $_SERVER['HTTP_CLIENT_IP'])) {

            $ip = $_SERVER['HTTP_CLIENT_IP'];

        } elseif(isset($_SERVER['HTTP_X_FORWARDED_FOR']) AND preg_match_all('#\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}#s', $_SERVER['HTTP_X_FORWARDED_FOR'], $matches)) {

            foreach ($matches[0] AS $xip) {

                if (!preg_match('#^(10|172\.16|192\.168)\.#', $xip)) {

                    $ip = $xip;

                    break;

                }

            }

        }

        return $ip;

    }
}
