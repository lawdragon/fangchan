<?php

namespace app\admin\controller;

use app\common\controller\Backend;
use app\common\model\Url as WeburlModel;
use fast\Tree;
use think\Db;

/**
 * 地域管理
 *
 * @icon   fa fa-list
 * @remark
 */
class Weburl extends Backend
{
    protected $model = null;
    protected $dlLists = null;
    public function _initialize()
    {
        parent::_initialize();
        $this->model = model('app\common\model\Url');
        //代理列表
        $this->dlLists=$this->getdlLists(array('b.group_id'=>2));
        $this->view->assign("dlLists", $this->dlLists);
    }
    /**
     * 查看
     */
    public function index(){
        //设置过滤方法
        $this->request->filter(['strip_tags']);
        if ($this->request->isAjax()) {
            $where='a.status in(0,1)';
            $params = $this->request->get();
            if($params['limit']){
                $limit=$params['limit'];
            }
            if($params['offset']==0){
                $p=1;
            }else{
                $p=($params['offset']/$params['limit'])+1;
            }
            if($params['sort'] && $params['order']){
                $order='a.'.$params['sort'].' '.$params['order'];
            }
            $data=$this->model->getWeburlList($where,$limit,$p,$order);
            $dlLists=$this->dlLists;
            if($data['list']){
                foreach($data['list'] as $k=>$v){
                    $v['dl_name']='';
                    if($dlLists && $v['dl_uid']){
                        foreach($dlLists as $k1=>$v1){
                            if(in_array($v1['id'],explode(',',$v['dl_uid']))){
                               $v['dl_name'].=$v1['username'].'/'.$v1['nickname'].',';
                            }
                        }
                    }
                    /*if($v['dl_uid']){
                        $dl_uid_arr=json_decode($v['dl_uid'],true);
                        $dl_uid_arrs=join(',',$dl_uid_arr);
                        $v['dl_uid']=$dl_uid_arrs;
                    }else{
                        $v['dl_uid']='';
                    }*/
                    $data['list'][$k]=$v;
                }
            }
            //var_dump($data['list']);exit;
            $result = array("total" => $data['count'], "rows" => $data['list']);
            return json($result);
        }
        return $this->view->fetch();
    }
    /**
     * 添加
     */
    public function add(){
        if ($this->request->isPost()) {
            $this->token();
            $params = $this->request->post("row/a");
            if ($params) {
                $params = $this->preExcludeFields($params);
                $add_data['url']=$params['url'];
                if(empty($add_data['url'])){
                    $this->error('域名url不能为空！');
                }
                $add_data['dl_uid']='';
                if($params['dl_uid']){
                    $add_data['dl_uid']=join(',',$params['dl_uid']);
                }
                // $add_data['weigh']=$params['weigh'];
                $add_data['status']=$params['status'];
                $add_data['createtime']=time();
                $add_data['cuid']=$this->auth->id;

                $result=$this->model->insert($add_data);
                if ($result !== false) {
                    $this->success();
                } else {
                    $this->error('保存失败！');
                }
            }
            $this->error(__('Parameter %s can not be empty', ''));
        }
        return $this->view->fetch();
    }
    /**
     * 编辑
     */
    public function edit($ids = null)
    {
        $row = $this->model->get($ids);
        if (!$row) {
            $this->error(__('No Results were found'));
        }
        $adminIds = $this->getDataLimitAdminIds();
        if (is_array($adminIds)) {
            if (!in_array($row[$this->dataLimitField], $adminIds)) {
                $this->error(__('You have no permission'));
            }
        }
        if ($this->request->isPost()) {
            $this->token();
            $params = $this->request->post("row/a");
            if ($params) {
                $params = $this->preExcludeFields($params);

                $add_data['url']=$params['url'];
                if(empty($add_data['url'])){
                    $this->error('域名url不能为空！');
                }
                $add_data['dl_uid']='';
                if($params['dl_uid']){
                    $add_data['dl_uid']=join(',',$params['dl_uid']);
                }
                // $add_data['weigh']=$params['weigh'];
                $add_data['status']=$params['status'];
                $where['id']=$params['mid'];
                $result=$this->model->where($where)->setField($add_data);
                if ($result !== false) {
                    $this->success();
                } else {
                    $this->error('保存失败！');
                }
            }
            $this->error(__('Parameter %s can not be empty', ''));
        }
        $this->view->assign("row", $row);
        return $this->view->fetch();
    }
    /**
     * 删除
     */
    public function del($ids = "")
    {
        $model = Db::name('url');
        if (!$this->request->isPost()) {
            $this->error(__("Invalid parameters"));
        }
        $ids = $ids ? $ids : $this->request->post("ids");
        if ($ids) {
            $where['id']=array('in',$ids);
            $add_data['status']=3;
            $result=$model->where($where)->setField($add_data);
            if($result){
                $this->success();
            }
        }
        $this->error();
    }
}