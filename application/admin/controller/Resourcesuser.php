<?php

namespace app\admin\controller;

use app\common\controller\Backend;
use app\common\model\ResourcesUser as ResourcesModel;
use fast\Tree;
use think\Db;
use think\Request;
use think\Loader;

/**
 * 资源管理
 *
 * @icon   fa fa-list
 * @remark
 */
class Resourcesuser extends Backend
{
    protected $model = null;
    protected  $ResourcesModel= null;
    public function _initialize()
    {
        parent::_initialize();
        //$this->model = model('app\common\model\Resources');
        $this->ResourcesModel=new ResourcesModel();

        $categoryList=$this->getCategoryLists(array('a.status'=>1));
        $getAreaList=$this->getAreaLists(array('a.status'=>1));
        $getPzList=$this->getPzLists(array('a.status'=>1));
        if($getPzList){
            $getPzLists = [];
            foreach($getPzList as $k=>$v){
                $getPzLists[$v['id']] = $v['name'];
            }
        }
        $this->view->assign("pzList", $getPzLists);
        $this->view->assign("categoryList", $categoryList);
        $this->view->assign("areaList", $getAreaList);
        $this->view->assign("gid", GID);

    }
    /**
     * 查看
     */
    public function index(){
        //设置过滤方法
        $this->request->filter(['strip_tags']);
        $params = $this->request->get();
        if ($this->request->isAjax()) {
            $where['a.is_del']=0;
            // $where['a1.is_del'] = 0; // 如果要管理资源市场的是否删除，则开启这个条件
            $where['a.uid']=$this->auth->id;
            // $where['a.status']=1;
            if(isset($params['filter'])){
                $filter=json_decode($params['filter'],true);
                //编号
                if(isset($filter['number'])){
                    $where['a1.number']=array('like','%'.$filter['number'].'%');
                }
                // 房东信息
                if(isset($filter['info'])){
                    $where['a.info']=array('like','%'.$filter['info'].'%');
                }
                //删除状态
                if(isset($filter['rid'])){
                    $where['a1.rid'] = $filter['rid'];
                }
                //标题
                if(isset($filter['title'])){
                    $where['a1.title']=array('like','%'.$filter['title'].'%');
                }
                //分类
                if(isset($filter['category_name'])){
                    $where['a1.category_id']=$filter['category_name'];
                }
                //地区
                if(isset($filter['area_name'])){
                    $where['a1.area_id']=$filter['area_name'];
                }
                //代理
                if(isset($filter['dl_name_list'])){
                    $where['a.uid'] = $filter['dl_name_list'];
                }
                //创建人
                if(isset($filter['cjname'])){
                    $where['a1.cuid']=$filter['cjname'];
                }
                //配置
                if(isset($filter['pz_id'])){
                    //$where['a1.pz_id']=$filter['pz_id'];
                    $where['a1.pz_id']=['exp',DB::raw(" REGEXP '".$filter['pz_id']."'")];
                }
                //置顶
                if(isset($filter['is_top'])){
                    $where['a.is_top']=$filter['is_top'];
                }
                //推荐
                if(isset($filter['is_recommend'])){
                    $where['a.is_recommend']=$filter['is_recommend'];
                }
                // 忙碌
                if(isset($filter['is_idle'])){
                    $where['a.is_idle']=$filter['is_idle'];
                }
                //上下架状态
                if(isset($filter['status'])){
                    $where['a.status']=$filter['status'];
                }
                //删除状态
                if(isset($filter['is_del'])){
                    $where['a.status']=$filter['is_del'];
                }
                //创建时间
                if(isset($filter['createtime'])){
                    $createtime=explode(' - ',$filter['createtime']);
                    if($createtime[0] && $createtime[1]){
                        $where['a.createtime']=array(array('egt',strtotime($createtime[0])),array('elt',strtotime($createtime[1])),'and');
                    }
                }

            }

            // halt($where);
           /* if(isset($params['title']) && $params['title']){
                $where['a1.title']=array('like','%'.$params['title'].'%');
            }
            if(isset($params['number']) && $params['number']){
                $where['a1.number']=array('like','%'.$params['number'].'%');
            }
            if(isset($params['status']) && $params['status']!='-1'){
                $where['a1.status']=$params['status'];
            }
            if(isset($params['category']) && $params['category']){
                $where['a1.category_id']=$params['category'];
            }
            if(isset($params['area']) && $params['area']){
                $where['a1.area_id']=$params['area'];
            }*/
            if(isset($params['limit'])){
                $limit=$params['limit'];
            }else{
                $limit=10;
            }
            if(!isset($params['offset']) || $params['offset']==0){
                $p=1;
            }else{
                $p=($params['offset']/$params['limit'])+1;
            }
            if($params['sort'] && $params['order']){
                $order='a.'.$params['sort'].' '.$params['order'];
            }
            $where['a.gl_status']=1;
            $data=$this->ResourcesModel->getResourcesList($where,$limit,$p,$order);
            if($data['list']){
                foreach($data['list'] as $k=>$v){
                    $v['gid']=GID;
                    $data['list'][$k]=$v;
                }
            }
            $result = array("total" => $data['count'], "rows" => $data['list'],'gid'=>GID);

            return json($result);
        }
        return $this->view->fetch();
    }
    /**
     * 添加
     */
    public function add(){
        if ($this->request->isPost()) {
            $this->token();
            $params = $this->request->post("row/a");
            //var_dump($params);exit;
            if ($params) {
                $params = $this->preExcludeFields($params);

                /*if(empty($add_data['Resources_name'])){
                    $this->error('区域名称不能为空！');
                }*/
                /*if(empty($add_data['Resources_sort'])){
                    $this->error('排序不能为空！');
                }*/
                $add_data['pz_id']='';
                if($params['pz_id']){
                    $add_data['pz_id']=join(',',$params['pz_id']);
                }
                $add_data['number']=$params['number'];
                $add_data['title']=$params['title'];

                $add_data['cover']=$params['cover'];
                $add_data['video']=$params['video'];
                $add_data['content']=$params['img_list'];
                //$add_data['content']=isset($_POST['editorValue'])?$_POST['editorValue']:'';
                $add_data['address']=isset($params['address'])?$params['address']:'';
                $add_data['remarks']=$params['remarks'];
                $add_data['end_time']=isset($params['end_time'])?strtotime($params['end_time']):'';

                $add_data['category_id']=$params['category_id'];
                $add_data['area_id']=$params['area_id'];
                $add_data['is_top']=$params['is_top'];
                $add_data['is_recommend']=$params['is_recommend'];
                $add_data['status']=$params['status'];
                $add_data['createtime']=time();
                $add_data['cuid']=$this->auth->id;
                $result=$this->model->insert($add_data);

                if ($result !== false) {
                    //增加排班关系
                    if(GID!=1){
                        $add_data_pb['rid']=1;
                        $add_data_pb['uid']=$this->auth->id;
                        $add_data_pb['pb_time']=time();
                        $add_data_pb['status']=1;
                        Db::name('pb')->insert($add_data_pb);
                        /*$add_data1['pb_uid']=$this->auth->id;
                        $add_data1['pb_status']=1;
                        $add_data1['pb_time']=time();*/
                    }

                    //增加归属关系
                    $add_data1['info'] = $params['info'];
                    $add_data1['rid']=$this->model->getLastInsID();
                    $add_data1['gl_status']=1;
                    $add_data1['createtime']=time();
                    $add_data1['sj_time']=time();
                    $add_data1['uid']=$this->auth->id;
                    $result=Db::name('Resources_user')->insert($add_data1);
                    $this->success();
                } else {
                    $this->error('保存失败！');
                }
            }
            $this->error(__('Parameter %s can not be empty', ''));
        }
        return $this->view->fetch();
    }
    public function add_sss(){
        if ($this->request->isPost()) {
            $this->token();
            $params = $this->request->post("row/a");
            //var_dump($params);exit;
            if ($params) {
                $params = $this->preExcludeFields($params);
                
                /*if(empty($add_data['Resources_name'])){
                    $this->error('区域名称不能为空！');
                }*/
                /*if(empty($add_data['Resources_sort'])){
                    $this->error('排序不能为空！');
                }*/
                $add_data['pz_id']='';
                if($params['pz_id']){
                    $add_data['pz_id']=join(',',$params['pz_id']);
                }
                $add_data['number']=$params['number'];
                $add_data['title']=$params['title'];

                $add_data['cover']=$params['cover'];
                $add_data['video']=$params['video'];
                $add_data['content']=$params['img_list'];
                //$add_data['content']=isset($_POST['editorValue'])?$_POST['editorValue']:'';
                $add_data['address']=isset($params['address'])?$params['address']:'';
                $add_data['remarks']=$params['remarks'];
                $add_data['end_time']=isset($params['end_time'])?strtotime($params['end_time']):'';

                $add_data['category_id']=$params['category_id'];
                $add_data['area_id']=$params['area_id'];
                /*$add_data['is_top']=$params['is_top'];
                $add_data['is_recommend']=$params['is_recommend'];*/
                $add_data['status']=$params['status'];
                $add_data['createtime']=time();
                $add_data['cuid']=$this->auth->id;
                $model1 = Db::name('Resources');
                $rid=$model1->insertGetId($add_data);

                $add_data2['rid']=$rid;
                $add_data2['remarks']=$add_data['remarks'];
                $add_data2['createtime']=time();
                $add_data2['uid']=$this->auth->id;
                $add_data2['end_time']=$add_data['end_time'];
                $add_data2['status']=$add_data['status'];

                $model2 = Db::name('Resources_user');
                $result=$model2->insert($add_data2);
                if ($result !== false) {
                    $this->success();
                } else {
                    $this->error('保存失败！');
                }
            }
            $this->error(__('Parameter %s can not be empty', ''));
        }
        return $this->view->fetch();
    }
    /**
     * 编辑
     */
    public function edit($ids = null)
    {

        $row = $this->ResourcesModel->getResourcesInfo(array('a.id'=>$ids));
        //var_dump($row);exit;
        if (!$row) {
            $this->error(__('No Results were found'));
        }
        if($row['end_time']>1){
            $row['end_time']=date('Y-m-d H:i:s',$row['end_time']);
        }else{
            $row['end_time']='';
        }
        $adminIds = $this->getDataLimitAdminIds();
        if (is_array($adminIds)) {
            if (!in_array($row[$this->dataLimitField], $adminIds)) {
                $this->error(__('You have no permission'));
            }
        }
        $model2 = Db::name('Resources_user');
        if(GID!=1){
            $info=$model2->where(array('id'=>$ids,'uid'=>$this->auth->id))->find();
            if($info['remarks']){
                $row['remarks']= $info['remarks'];
                /*$row['remarks']= $info['remarks'];
                $row['remarks']= $info['remarks'];*/
            }
            if($info['info']){
                $row['info'] = $info['info'];
            }
        }

        if ($this->request->isPost()) {
            $this->token();
            $params = $this->request->post("row/a");
            if ($params) {
                $params = $this->preExcludeFields($params);

                $add_data['pz_id']='';
                if(isset($params['pz_id'])){
                    $add_data['pz_id']=join(',',$params['pz_id']);
                }
                if($row['category_id']!=$params['category_id']){
                    $add_data['number']=$this->numberNo(1,$params['category_id']);
                }
                /*$info=db::name('Resources')->field('rid')->where(['title'=>$params['title']])->select();
                if($info){
                    foreach($info as $k=>$v){
                        if($v['rid'] && $v['rid']!=$params['mid']){
                            $this->error('资源名称已存在！');
                            break;
                        }
                    }
                }*/
                $add_data['title']=trim($params['title']);

                $add_data['cover']=$params['cover'];
                $add_data['video']=$params['video'];
                $add_data['content']=$params['img_list'];
                //$add_data['content']=isset($_POST['editorValue'])?$_POST['editorValue']:'';
                $add_data['address']=isset($params['address'])?$params['address']:'';
                //$add_data['remarks']=$params['remarks'];
                $add_data['end_time']=isset($params['end_time'])?strtotime($params['end_time']):'';

                $add_data['category_id']=$params['category_id'];
                $add_data['area_id']=$params['area_id'];
                /*$add_data['is_top']=$params['is_top'];
                $add_data['is_recommend']=$params['is_recommend'];*/
                $add_data['status']=$params['status'];
                $add_data['updatetime']=time();
                $where['rid']=$params['mid'];
                $model1 = Db::name('Resources');
                $result=$model1->where($where)->setField($add_data);

                $add_data2['info']=$params['info'];
                $add_data2['remarks']=$params['remarks'];
                $add_data2['updatetime']=time();
                $add_data2['end_time']=$add_data['end_time'];
                $add_data2['status']=$params['status'];
                $where2['id']=$params['mid2'];

                $result=$model2->where($where2)->setField($add_data2);
                if ($result !== false) {
                    $this->success();
                } else {
                    $this->error('保存失败！');
                }
            }
            $this->error(__('Parameter %s can not be empty', ''));
        }
        $this->view->assign("row", $row);
        return $this->view->fetch();
    }


    public function saveResources(){
            $params = $this->request->post();
            $model2 = Db::name('Resources_user');
            if ($params) {
                $params = $this->preExcludeFields($params);

                $add_data['pz_id']='';
                if($params['pz_id']){
                    $add_data['pz_id']=join(',',$params['pz_id']);
                }
                if($params['mid']){
                    $row = db::name('Resources')->where(['rid'=>$params['mid']])->find();
                    if($row['category_id']!=$params['category_id']){
                        $add_data['number']=$this->numberNo(1,$params['category_id']);
                    }
                }
                /*$info=db::name('Resources')->field('rid')->where(['title'=>$params['title']])->select();
                if($info){
                    foreach($info as $k=>$v){
                        if($v['rid'] && $v['rid']!=$params['mid']){
                            $this->error('资源名称已存在！');
                            break;
                        }
                    }
                }*/
                $add_data['title']=$params['title'];

                $add_data['cover']=$params['cover'];
                $add_data['video']=$params['video'];
                $add_data['content']=$params['img_list'];
                //$add_data['content']=isset($_POST['editorValue'])?$_POST['editorValue']:'';
                $add_data['address']=isset($params['address'])?$params['address']:'';
                //$add_data['remarks']=$params['remarks'];
                $add_data['end_time'] = isset($params['end_time'])?strtotime($params['end_time']):'';

                $add_data['category_id']=$params['category_id'];
                $add_data['area_id']=$params['area_id'];
                /*$add_data['is_top']=$params['is_top'];
                $add_data['is_recommend']=$params['is_recommend'];*/
                //$add_data['status']=$params['status'];
                $add_data['updatetime']=time();
                $where['rid']=$params['mid'];
                $model1 = Db::name('Resources');
                $result=$model1->where($where)->setField($add_data);

                $add_data2['remarks']=$params['remarks'];
                $add_data2['updatetime']=time();
                $add_data2['end_time']=$add_data['end_time'];
                //$add_data2['status']=$params['status'];
                $where2['id']=$params['mid2'];

                $result=$model2->where($where2)->setField($add_data2);
                if ($result !== false) {
                    $code=200;
                } else {
                    $code=400;
                }
            }
        echo json_encode(array('code'=>$code));
    }
    /**
     * 资源编号
     * @param $type
     * @return string
     */
    /*public function numberNo($first,$type)
    {

        $count =db::name('Resources')->where(['category_id'=>$type])->count();
        $num = $count ? $count + 1 : 1;
        return  $type.'000'.$num;
    }*/
    /**
     * 资源更新列表
     */
    public function gxlist(){
        //设置过滤方法
        $this->request->filter(['strip_tags']);
        if ($this->request->isAjax()) {
            $where='1=1';
            $data=$this->ResourcesModel->getResourcesList($where);
            $result = array("total" => $data['count'], "rows" => $data['list']);
            return json($result);
        }
        return $this->view->fetch();
    }
    /**
     * 资源关联操作
     */
    public function adopt($ids = null){
        $model = Db::name('Resources_user');
        //判断是否第一个关联资源 第一个关联资源默认排班成功
        $where_pb['uid']=$this->auth->id;
        $where_pb['rid']=$ids?$ids:0;
        $model_pb=Db::name('pb');
        $infos=$model_pb->where($where_pb)->find();
        if(!$infos && GID!=1){
            // $add_data_pb['rid']=$ids;
            // $add_data_pb['uid']=$this->auth->id;
            // $add_data_pb['pb_time']=time();
            // $add_data_pb['status']=1;
            // $model_pb->insert($add_data_pb);
            /*$add_data['pb_uid']=$this->auth->id;
            $add_data['pb_status']=1;
            $add_data['pb_time']=time();*/
        }
        //判断关联表是否有数据-有开启关联
        //没有数据添加关联数据
        $where['uid'] = $this->auth->id;
        $where['rid'] = $ids?$ids:0;
        $info=$this->ResourcesModel->getglInfo($where);
        
        if(empty($info)){
            $add_data['rid']=$info['rid'];
            $add_data['status']=$info['status'];
            $add_data['gl_status']=1;
            $add_data['createtime']=time();
            $add_data['sj_time']=time();
            $add_data['uid']=$this->auth->id;
            $result=$model->insert($add_data);
        }else{
            $add_data['status']=$info['status'];
            $add_data['gl_status']=1;
            $add_data['createtime']=time();
            $add_data['sj_time']=time();
            $where['rid']=$info['rid'];
            $result=$model->where($where)->setField($add_data);
        }
        $this->success();
    }
    /**
     * 资源取消操作
     */
    public function cancel($ids = null){
        $model = Db::name('Resources_user');
        //判断关联表是否有数据-有开启关联
        //没有数据添加关联数据
        $where['uid']=$this->auth->id;
        $where['id']=$ids?$ids:0;
        $info=$this->ResourcesModel->getglInfo($where);
        if(empty($info)){
            /*$add_data['rid']=$info['rid'];
            $add_data['gl_status']=2;
            $add_data['uid']=$this->auth->id;
            $result=$model->insert($add_data);*/
        }else{
            $add_data['gl_status']=2;
            $result=$model->where($where)->setField($add_data);
        }
        $this->success();
    }
    /**
     * 刪除操作
     */
    public function del_status($ids = null){
        $model = Db::name('Resources_user');
        $where['uid']=$this->auth->id;
        $where['id']=$ids?array('in',$ids):0;
        $add_data['is_del']=1;
        $result=$model->where($where)->setField($add_data);

        $this->success();
    }
    /**
     * 批量上下架
     */
    public function saveStatus(Request $request){
        $params = $request->post();
        $savedata['status']=$params['status'];
        $where['gl_status']=1;
        $where['uid'] = $this->auth->id;
        // $info=$this->ResourcesModel->saveStatus($where,$savedata);
        $info=$this->ResourcesModel->saveStatus($where,$savedata);
        echo 1;
    }
    /**
     * 生成链接页面
     */
    public function set_generate_url(Request $request){
        if ($this->request->isPost()) {
            $params = $this->request->post("row/a");
            if ($params) {
                $params = $this->preExcludeFields($params);
                /*if($params['number']){
                    $querys_data['a1.number']=array('like','%'.$params['number'].'%');
                }
                if($params['title']){
                    $querys_data['a1.title']=array('like','%'.$params['title'].'%');
                }
                if($params['category']){
                    $querys_data['a1.category_id']=$params['category'];
                }
                if($params['area']){
                    $querys_data['a1.area_id']=$params['area'];
                }*/
                if($params['rid']){
                    $querys_data['a1.id']=array('in',$params['rid']);
                }
                $querys_data['a.status']=1;
                $querys_data['a.is_del']=0;
                if(GID!=1){
                    $querys_data['a1.uid']=$this->auth->id;
                    $querys_data['a1.status']=1;
                    $querys_data['a1.is_del']=0;
                }
                //批量创建链接
                $add_data['pwd_random']=SALT.time();
                if($params['url']){
                    $model = Db::name('resourced_url');
                    for($a=0;$a<count($params['url']);$a++){
                        $add_data['querys']=json_encode($querys_data);
                        $add_data['url']=$params['url'][$a];
                        $add_data['pwd']=$params['password'];
                        $add_data['token']=md5($params['password'] . $add_data['pwd_random']);
                        $add_data['end_time']=isset($params['end_time'])?strtotime($params['end_time']):'';
                        //$add_data['remarks']=$params['remarks'];

                        $add_data['cuid']=$this->auth->id;
                        $add_data['createtime']=time();

                        $url = 'https://zhan.leiue.com/weibo.html';
                        $arr = parse_url($params['url'][$a]);
                        $domain = $arr['host'];
                        $add_data['link_url']=$domain.'?token='.$add_data['token'];

                        $result=$model->insert($add_data);
                    }
                }



                if ($result !== false) {
                    $this->success();
                } else {
                    $this->error('保存失败！');
                }
            }
            $this->error(__('Parameter %s can not be empty', ''));
        }
        $params = $request->get();
        $urlList=$this->ResourcesModel->getUrlLists($this->auth->id);

        $this->view->assign('urlList',$urlList);
        $this->view->assign('params',$params);
        return $this->view->fetch();
    }
    public function set_generate_url_v1(Request $request){
            if ($this->request->isPost()) {
                $params = $this->request->post("row/a");
                if ($params) {
                    $params = $this->preExcludeFields($params);
                    /*if($params['number']){
                        $querys_data['a1.number']=array('like','%'.$params['number'].'%');
                    }
                    if($params['title']){
                        $querys_data['a1.title']=array('like','%'.$params['title'].'%');
                    }
                    if($params['category']){
                        $querys_data['a1.category_id']=$params['category'];
                    }
                    if($params['area']){
                        $querys_data['a1.area_id']=$params['area'];
                    }*/
                    if($params['rid']){
                        $querys_data['a1.rid']=array('in',$params['rid']);
                    }
                    $querys_data['a.status']=1;
                    $querys_data['a.is_del']=0;
                    if(GID!=1){
                        $querys_data['a1.uid']=$this->auth->id;
                        $querys_data['a1.status']=1;
                        $querys_data['a1.is_del']=0;
                    }
                    $add_data['pwd_random']=SALT.time();//
                    $add_data['querys']=json_encode($querys_data);
                    $add_data['url']=$params['url'];
                    $add_data['pwd']=$params['password'];
                    $add_data['token']=md5($params['password'] . $add_data['pwd_random']);
                    $add_data['end_time']=isset($params['end_time'])?strtotime($params['end_time']):'';
                    //$add_data['remarks']=$params['remarks'];

                    $add_data['cuid']=$this->auth->id;
                    $add_data['createtime']=time();
                    $arr = parse_url($params['url']);
                    $domain = $arr['host'];
                    $add_data['link_url']=$domain.'?token='.$add_data['token'];

                    $model = Db::name('ds_resourced_url');
                    $result=$model->insert($add_data);
                    if ($result !== false) {
                        $this->success();
                    } else {
                        $this->error('保存失败！');
                    }
                }
                $this->error(__('Parameter %s can not be empty', ''));
            }
            $params = $request->get();
            $urlList=$this->ResourcesModel->getUrlLists($this->auth->id);

            $this->view->assign('urlList',$urlList);
            $this->view->assign('params',$params);
            return $this->view->fetch();
    }
    /**
     * 保存-生成链接
     */
    public function save_generate_url(Request $request){
        /*$params = $request->post();
        $savedata['status']=$params['status'];
        $where['gl_status']=1;
        $info=$this->ResourcesModel->saveStatus($where,$savedata);*/
        echo 1;
    }
    public function getQrcode($url){
        // 引入 extend/qrcode.php;
        Loader::import('phpqrcode.phpqrcode', EXTEND_PATH);
        $qrcode = new \QRcode();//声明qrcode类
        //$url='https://www.baidu.com/';//要转成二维码的url地址
        $errorLevel = "L";//容错率
        $size = "4";//生成图片大小
        // ob_clean();//若二维码图片未正常输出，需先清除缓存
        $img=$qrcode->png($url, false, $errorLevel, $size);//调用png()方法生成二维码
        return $img;
    }
    /**
     * 状态修改
     */
    public function multi_save(Request $request){
        $params = $request->post();
        if($params['ids']){
            //'UPDATE `ds_kh_yuyue` SET hz_tx_wzd_time='.time().'  WHERE  gh_id='.$gh_id
            // echo "UPDATE `ds_resources_user` SET {$params['params']}  WHERE id={$params['ids']}";exit;
            Db::query("UPDATE `ds_resources_user` SET {$params['params']}  WHERE id={$params['ids']}");
            if ($params['params'] == 'is_idle=1') {
                $time = time();
                Db::query("UPDATE `ds_resources_user` SET is_idle_updatetime={$time}  WHERE id={$params['ids']}");
            }
        }
        $this->success();
    }
}