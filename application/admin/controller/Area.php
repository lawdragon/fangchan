<?php

namespace app\admin\controller;

use app\common\controller\Backend;
use app\common\model\Area as AreaModel;
use fast\Tree;
use think\Db;

/**
 * 地域管理
 *
 * @icon   fa fa-list
 * @remark
 */
class Area extends Backend
{
    protected $model = null;
    protected $AreaModel = null;
    public function _initialize()
    {
        parent::_initialize();
        $this->model = model('app\common\model\Area');
        $this->AreaModel=new AreaModel();
    }
    /**
     * 查看
     */
    public function index(){
        //设置过滤方法
        $this->request->filter(['strip_tags']);
        if ($this->request->isAjax()) {
            $where='a.status in(0,1)';
            $params = $this->request->get();
            if($params['limit']){
                $limit=$params['limit'];
            }
            if($params['offset']==0){
                $p=1;
            }else{
                $p=($params['offset']/$params['limit'])+1;
            }
            if($params['sort'] && $params['order']){
                $order='a.'.$params['sort'].' '.$params['order'];
            }
            $data=$this->AreaModel->getAreaList($where,$limit,$p,$order);
            $result = array("total" => $data['count'], "rows" => $data['list']);
            return json($result);
        }
        return $this->view->fetch();
    }
    /**
     * 添加
     */
    public function add(){
        if ($this->request->isPost()) {
            $this->token();
            $params = $this->request->post("row/a");
            if ($params) {
                $params = $this->preExcludeFields($params);
                $add_data['area_name']=$params['area_name'];
                if(empty($add_data['area_name'])){
                    $this->error('区域名称不能为空！');
                }
                /*if(empty($add_data['weigh'])){
                    $this->error('排序不能为空！');
                }*/
                $add_data['weigh']=$params['weigh'];
                $add_data['status']=$params['status'];
                $add_data['createtime']=time();
                $add_data['cuid']=$this->auth->id;
                $result=$this->model->insert($add_data);
                if ($result !== false) {
                    $this->success();
                } else {
                    $this->error('保存失败！');
                }
            }
            $this->error(__('Parameter %s can not be empty', ''));
        }
        return $this->view->fetch();
    }
    /**
     * 编辑
     */
    public function edit($ids = null)
    {
        $row = $this->model->get($ids);
        if (!$row) {
            $this->error(__('No Results were found'));
        }
        $adminIds = $this->getDataLimitAdminIds();
        if (is_array($adminIds)) {
            if (!in_array($row[$this->dataLimitField], $adminIds)) {
                $this->error(__('You have no permission'));
            }
        }
        if ($this->request->isPost()) {
            $this->token();
            $params = $this->request->post("row/a");
            if ($params) {
                $params = $this->preExcludeFields($params);

                $add_data['area_name']=$params['area_name'];
                if(empty($add_data['area_name'])){
                    $this->error('区域名称不能为空！');
                }
                /*if(empty($add_data['weigh'])){
                    $this->error('排序不能为空！');
                }*/
                $add_data['weigh']=$params['weigh'];
                $add_data['status']=$params['status'];
                $where['id']=$params['mid'];
                $result=$this->model->where($where)->setField($add_data);
                if ($result !== false) {
                    $this->success();
                } else {
                    $this->error('保存失败！');
                }
            }
            $this->error(__('Parameter %s can not be empty', ''));
        }
        $this->view->assign("row", $row);
        return $this->view->fetch();
    }
    /**
     * 删除
     */
    public function del($ids = "")
    {
        $model = Db::name('area');
        if (!$this->request->isPost()) {
            $this->error(__("Invalid parameters"));
        }
        $ids = $ids ? $ids : $this->request->post("ids");
        if ($ids) {
            $where['id']=array('in',$ids);
            $add_data['status']=3;
            $result=$model->where($where)->setField($add_data);
            if($result){
                $this->success();
            }
        }
        $this->error();
    }
}