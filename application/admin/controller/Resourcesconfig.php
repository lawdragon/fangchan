<?php

namespace app\admin\controller;

use app\common\controller\Backend;
use app\common\model\ResourcesConfig as ResourcesConfigModel;
use fast\Tree;

/**
 * 资源配置项管理
 *
 * @icon   fa fa-list
 * @remark
 */
class Resourcesconfig extends Backend
{
    protected $model = null;
    protected $ResourcesConfigModel = null;
    public function _initialize()
    {
        parent::_initialize();
        $this->model = model('app\common\model\ResourcesConfig');
        $this->ResourcesConfigModel=new ResourcesConfigModel();
    }
    /**
     * 查看
     */
    public function index(){
        //设置过滤方法
        $this->request->filter(['strip_tags']);
        if ($this->request->isAjax()) {
            $params = $this->request->get();
            $where='a.status=1 or a.status=0';
            if(isset($params['limit'])){
                $limit=$params['limit'];
            }else{
                $limit=10;
            }
            if(!isset($params['offset']) || $params['offset']==0){
                $p=1;
            }else{
                $p=($params['offset']/$params['limit'])+1;
            }
            $data=$this->ResourcesConfigModel->getResourcesconfigList($where,$limit,$p);
            $result = array("total" => $data['count'], "rows" => $data['list']);
            return json($result);
        }
        return $this->view->fetch();
    }
    /**
     * 添加
     */
    public function add(){
        if ($this->request->isPost()) {
            $this->token();
            $params = $this->request->post("row/a");
            if ($params) {
                $params = $this->preExcludeFields($params);
                $add_data['name']=$params['name'];
                if(empty($add_data['name'])){
                    $this->error('名称不能为空！');
                }
                $add_data['weigh']=$params['weigh'];
                $add_data['status']=$params['status'];
                $add_data['createtime']=time();
                $add_data['cuid']=$this->auth->id;
                $result=$this->model->insert($add_data);
                if ($result !== false) {
                    $this->success();
                } else {
                    $this->error('保存失败！');
                }
            }
            $this->error(__('Parameter %s can not be empty', ''));
        }
        return $this->view->fetch();
    }
    /**
     * 编辑
     */
    public function edit($ids = null)
    {
        $row = $this->model->get($ids);
        if (!$row) {
            $this->error(__('No Results were found'));
        }
        $adminIds = $this->getDataLimitAdminIds();
        if (is_array($adminIds)) {
            if (!in_array($row[$this->dataLimitField], $adminIds)) {
                $this->error(__('You have no permission'));
            }
        }
        if ($this->request->isPost()) {
            $this->token();
            $params = $this->request->post("row/a");
            if ($params) {
                $params = $this->preExcludeFields($params);

                $add_data['name']=$params['name'];
                if(empty($add_data['name'])){
                    $this->error('名称不能为空！');
                }
                $add_data['weigh']=$params['weigh'];
                $add_data['status']=$params['status'];
                $where['id']=$params['mid'];
                $result=$this->model->where($where)->setField($add_data);
                if ($result !== false) {
                    $this->success();
                } else {
                    $this->error('保存失败！');
                }
            }
            $this->error(__('Parameter %s can not be empty', ''));
        }
        $this->view->assign("row", $row);
        return $this->view->fetch();
    }
}