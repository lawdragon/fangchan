<?php



namespace app\admin\controller;



use app\common\controller\Backend;

use app\common\model\Url as urlModel;

use app\common\model\Resources as ResourcesModel;

use fast\Tree;

use think\Db;

use think\Loader;

use think\Request;



/**

 * 链接url管理

 *

 * @icon   fa fa-list

 * @remark

 */

class Urllink extends Backend

{

    protected $model = null;

    protected $urlModel = null;

    protected  $ResourcesModel= null;

    public function _initialize()

    {

        parent::_initialize();

        $this->urlModel=new urlModel();

        $this->ResourcesModel=new ResourcesModel();

        $this->view->assign("gid", GID);

    }

    /**

     * 查看

     */

    public function index(){

        //设置过滤方法

        $this->request->filter(['strip_tags']);

        if ($this->request->isAjax()) {

            $where['a.status']=array('in','0,1');

            $params = $this->request->get();

            if($params['limit']){

                $limit=$params['limit'];

            }

            if($params['offset']==0){

                $p=1;

            }else{

                $p=($params['offset']/$params['limit'])+1;

            }

            if($params['sort'] && $params['order']){

                $order='a.'.$params['sort'].' '.$params['order'];

            }

            if(GID!=1){

                $where['a.cuid']=$this->auth->id;

            }

            $list=array();

            if(isset($params['filter'])){

                $filter=json_decode($params['filter'],true);

                //代理

                if(isset($filter['nickname'])){

                    $where['a.cuid']=$filter['nickname'];

                }

                //创建时间

                if(isset($filter['urlStatus'])){

                    if($filter['urlStatus']==1){

                        $where['a.end_time']=array('elt',time());

                    }else{

                        $where['a.end_time']=array('egt',time());

                    }

                }

            }



            $data=$this->urlModel->getUrlLinkList($where,$limit,$p,$order);



           /* if($data['list'] && GID!=1){

                $dl_uid_arr=array();

                foreach($data['list'] as $k=>$v){

                   if(!empty($v['dl_uid'])){

                       $dl_uid_arr=explode(',',$v['dl_uid']);

                       if(in_array($this->auth->id,$dl_uid_arr)){

                           $list[$k]=$v;

                       }

                   }

                }

                var_dump($this->auth->id,$data['list']);exit;

            }else{

                $list=$data['list'];

            }*/

            $result = array("total" => $data['count'], "rows" => $data['list']);

            return json($result);

        }

        return $this->view->fetch();

    }

    public function add(){

                if ($this->request->isPost()) {

            $params = $this->request->post("row/a");

            if ($params) {

                $params = $this->preExcludeFields($params);

                /*if($params['number']){

                    $querys_data['a1.number']=array('like','%'.$params['number'].'%');

                }

                if($params['title']){

                    $querys_data['a1.title']=array('like','%'.$params['title'].'%');

                }

                if($params['category']){

                    $querys_data['a1.category_id']=$params['category'];

                }

                if($params['area']){

                    $querys_data['a1.area_id']=$params['area'];

                }*/

                /*if($params['rid']){

                    $querys_data['a1.rid']=array('in',$params['rid']);

                }*/

                $querys_data['a.status']=1;



                if(GID!=1){

                    //$querys_data['a1.uid']=$this->auth->id;

                }

                //批量创建链接

                $add_data['pwd_random']=SALT.time();

                if(isset($params['url'])){

                    $model = Db::name('resourced_url');

                    for($a=0;$a<count($params['url']);$a++){

                        $add_data['querys']=json_encode($querys_data);

                        $add_data['url']=$params['url'][$a];

                        $add_data['pwd']=$params['password'];

                        $add_data['token']=md5($add_data['pwd_random']);//$params['password'] .

                        $add_data['end_time']=isset($params['end_time'])?strtotime($params['end_time']):'';

                        //$add_data['remarks']=$params['remarks'];



                        $add_data['cuid']=$this->auth->id;

                        $add_data['createtime']=time();



                        $url = 'https://zhan.leiue.com/weibo.html';

                        $arr = parse_url($params['url'][$a]);
                        // echo '<pre>';
                        // print_r($arr);exit;

                        $domain = isset($arr['host']) ? $arr['host'] : $arr['path'];

                        // $add_data['link_url']=$domain.'?token='.$add_data['token'];
                        $add_data['link_url']=$domain;



                        $result=$model->insert($add_data);

                    }

                }







                if ($result !== false) {

                    $this->success();

                } else {

                    $this->error('保存失败！');

                }

            }

            $this->error(__('Parameter %s can not be empty', ''));

        }

        

        $urlList=$this->ResourcesModel->getUrlLists($this->auth->id);



        $this->view->assign('urlList',$urlList);

        return $this->view->fetch();

    }

    /**

     * 编辑

     */

    public function edit($ids = null)

    {

        $row = $this->urlModel->getUrlLinkInfo(array('id'=>$ids));



        if (!$row) {

            $this->error(__('No Results were found'));

        }

        if($row['end_time']){

            $row['end_time']=date('Y-m-d H:i:s',$row['end_time']);

        }

        $adminIds = $this->getDataLimitAdminIds();

        if (is_array($adminIds)) {

            if (!in_array($row[$this->dataLimitField], $adminIds)) {

                $this->error(__('You have no permission'));

            }

        }

        if ($this->request->isPost()) {

            $this->token();

            $params = $this->request->post("row/a");

            if ($params) {

                $params = $this->preExcludeFields($params);



                if($row['pwd']!=$params['password']){

                    $add_data['pwd']=$params['password'];

                    //$add_data['pwd_random']=SALT.time();

                    //$add_data['token']=md5($params['password'] . $add_data['pwd_random']);



                    $arr = parse_url($params['url']);

                    $domain = $arr['host'];

                    //$add_data['link_url']=$domain.'?token='.$add_data['token'];

                }

                



                $add_data['end_time']=isset($params['end_time'])?strtotime($params['end_time']):'';

                //$add_data['remarks']=$params['remarks'];



                $add_data['update_cuid']=$this->auth->id;

                $add_data['updatetime']=time();



                



                $where['id']=$params['mid'];

                $model = Db::name('resourced_url');

                $result=$model->where($where)->setField($add_data);

                if ($result !== false) {

                    $this->success('保存成功！');

                } else {

                    $this->error('保存失败！');

                }

            }

            $this->error(__('Parameter %s can not be empty', ''));

        }

        $this->view->assign("row", $row);

        return $this->view->fetch();

    }

    /**

     * 删除

     */

    public function del($ids = "")

    {

        $model = Db::name('resourced_url');

        if (!$this->request->isPost()) {

            $this->error(__("Invalid parameters"));

        }

        $ids = $ids ? $ids : $this->request->post("ids");

        if ($ids) {

            $where['id']=array('in',$ids);

            $add_data['status']=3;

            $result=$model->where($where)->setField($add_data);

            if($result){

                $this->success();

            }

        }

        $this->error();

    }

    /**

     * 查看二维码

     */
    public function look(Request $request){

        //设置过滤方法

        $this->request->filter(['strip_tags']);

        $ids = $request->get('ids');

        $row = $this->urlModel->getUrlLinkInfo(array('id'=>$ids));

        if($row['url']){
            $url=$this->getQrcode($row['url']);
        }

        echo $url;exit;

        //$this->view->assign("url", $url);

        return $this->view->fetch();

    }

    public function getQrcode($url){

        // 引入 extend/qrcode.php;

        Loader::import('phpqrcode.phpqrcode', EXTEND_PATH);

        $qrcode = new \QRcode();//声明qrcode类

        //$url='https://www.baidu.com/';//要转成二维码的url地址

        $errorLevel = "L";//容错率

        $size = "4";//生成图片大小

        // ob_clean();//若二维码图片未正常输出，需先清除缓存

        $img=$qrcode->png($url, false, $errorLevel, $size);//调用png()方法生成二维码

        return $img;

    }

    /**

     * 状态修改

     */

    public function multi_save(Request $request){

        

        $params = $request->post();

        if($params['ids']){



            Db::query("UPDATE `ds_resourced_url` SET {$params['params']}  WHERE id={$params['ids']}");

        }

        $this->success();

    }

}