<?php

namespace app\admin\controller;

use app\common\controller\Backend;
use app\common\model\Access as AccessModel;
use think\Db;
/**
 * 地域管理
 *
 * @icon   fa fa-list
 * @remark
 */
class Access extends Backend
{
    protected $AccessModel = null;

    public function _initialize()
    {
        parent::_initialize();
        $this->AccessModel = new AccessModel();
        $categoryList=$this->getCategoryLists(array('a.status'=>1));
        $getAreaList=$this->getAreaLists(array('a.status'=>1));
        $getdlLists=$this->getdlLists();

        $this->view->assign("categoryList", $categoryList);
        $this->view->assign("areaList", $getAreaList);
        $this->view->assign("dlList", $getdlLists);
        $this->view->assign("gid", GID);
    }

    /**
     * 查看
     */
    public function index()
    {
        //设置过滤方法
        $this->request->filter(['strip_tags']);
        $params = $this->request->get();

        if ($this->request->isAjax()) {
            if(isset($params['filter'])){
                $filter=json_decode($params['filter'],true);
                if(isset($filter['category_name'])){
                    $where['a1.category_id']=$filter['category_name'];
                }
                if(isset($filter['area_name'])){
                    $where['a1.area_id']=$filter['area_name'];
                }
                if(isset($filter['nickname'])){
                    $where['u.cuid']=$filter['nickname'];
                }
                if(isset($filter['createtime'])){
                    $createtime=explode(' - ',$filter['createtime']);
                    if($createtime[0] && $createtime[1]){
                        $where['a.createtime']=array(array('egt',strtotime($createtime[0])),array('elt',strtotime($createtime[1])),'and');
                    }
                }
                //var_dump($where);exit;
            }

            if(GID!=1){
                $where['u.cuid']=$this->auth->id;
            }
            $limit =10;
            if ($params['limit']) {
                $limit = $params['limit'];
            }
            if ($params['offset'] == 0) {
                $p = 1;
            } else {
                $p = ($params['offset'] / $params['limit']) + 1;
            }
            if ($params['sort'] && $params['order']) {
                $order = 'a.' . $params['sort'] . ' ' . $params['order'];
            }
            if(isset($where)){
                $data = $this->AccessModel->getAccessList($where, $limit, $p, $order,GID);
            }else{
                $data = $this->AccessModel->getAccessList('', $limit, $p, $order);
            }

            $result = array("total" => $data['count'], "rows" => $data['list']);
            return json($result);
        }
        return $this->view->fetch();
    }
    public function index_sss()
    {
        //设置过滤方法
        $this->request->filter(['strip_tags']);
        $params = $this->request->get();

        if(!isset($params['number'])){
            $params['number']='';
        }
        if(!isset($params['title'])){
            $params['title']='';
        }
        if(!isset($params['uid'])){
            $params['uid']='';
        }
        if(!isset($params['category'])){
            $params['category']='';
        }
        if(!isset($params['area'])){
            $params['area']='';
        }
        $this->view->assign("params", $params);
        if ($this->request->isAjax()) {
            if(isset($params['category']) && $params['category']){
                $where['a1.category_id']=$params['category'];
            }
            if(isset($params['area']) && $params['area']){
                $where['a1.area_id']=$params['area'];
            }
            if(isset($params['uid']) && $params['uid']){
                $where['u.cuid']=$params['uid'];
            }
            if(GID!=1){
                $where['u.cuid']=$this->auth->id;
            }
            $limit =10;
            if ($params['limit']) {
                $limit = $params['limit'];
            }
            if ($params['offset'] == 0) {
                $p = 1;
            } else {
                $p = ($params['offset'] / $params['limit']) + 1;
            }
            if ($params['sort'] && $params['order']) {
                $order = 'a.' . $params['sort'] . ' ' . $params['order'];
            }
            if(isset($where)){
                $data = $this->AccessModel->getAccessList($where, $limit, $p, $order,GID);
            }else{
                $data = $this->AccessModel->getAccessList('', $limit, $p, $order);
            }

            $result = array("total" => $data['count'], "rows" => $data['list']);
            return json($result);
        }
        return $this->view->fetch();
    }

    public function getAreaList(){
        $getAreaList=$this->getAreaLists(array('a.status'=>1));
        $list=array();
        if($getAreaList){
            foreach($getAreaList as $k=>$v){
                $list[$v['id']]=$v['area_name'];
            }
        }
        return json($list);
    }

    public function categoryList(){
        $categoryList=$this->getCategoryLists(array('a.status'=>1));
        // echo '<pre>';
        // print_r($categoryList);
        // exit;
        $list=array();
        if($categoryList){
            foreach($categoryList as $k=>$v){
                $list[$v['id']]=$v['category_name'];
            }
        }
        return json($list);
    }


    //$list =  array('1' => '别墅','2' => '洋房','3' => '车位');
    //$json = json($list);
    public function getdlList(){
        $getdlLists=$this->getdlLists();
        $list=array();
        if(GID!=1){
            foreach($getdlLists as $k=>$v){
                if($v['id']==$this->auth->id){
                    $list[$v['id']]=$v['nickname'];
                }
            }
        }elseif($getdlLists){
            foreach($getdlLists as $k=>$v){
                $list[$v['id']]=$v['nickname'];
            }
        }
        return json($list);
    }

    public function getPzList(){
        $getPzLists=$this->getPzLists();
        $list=array();
        if(GID!=1){
            foreach($getPzLists as $k=>$v){
                $list[$v['id']]=$v['name'];
            }
        }elseif($getPzLists){
            foreach($getPzLists as $k=>$v){
                $list[$v['id']]=$v['name'];
            }
        }
        return json($list);
    }
    public function getUrlStatusList(){
        $list[1]='已过期';
        $list[2]='未过期';
        return json($list);
    }
}