<?php

return array (
  'name' => '房产系统',
  'beian' => '',
  'cdnurl' => '',
  'version' => '1.0.3',
  'timezone' => 'Asia/Shanghai',
  'forbiddenip' => '',
  'languages' => 
  array (
    'backend' => 'zh-cn',
    'frontend' => 'zh-cn',
  ),
  'fixedpage' => 'dashboard',
  'categorytype' => 
  array (
    'default' => 'Default',
    'page' => 'Page',
    'article' => 'Article',
    'test' => 'Test',
  ),
  'configgroup' => 
  array (
    'basic' => 'Basic',
    'email' => 'Email',
    'dictionary' => 'Dictionary',
    'user' => 'User',
    'example' => 'Example',
  ),
  'mail_type' => '1',
  'mail_smtp_host' => 'smtp.qq.com',
  'mail_smtp_port' => '465',
  'mail_smtp_user' => '10000',
  'mail_smtp_pass' => 'password',
  'mail_verify_type' => '2',
  'mail_from' => '10000@qq.com',
  'attachmentcategory' => 
  array (
    'category1' => 'Category1',
    'category2' => 'Category2',
    'custom' => 'Custom',
  ),
  'announcement' => '<p style="text-wrap: wrap;">请每天按时更新资源！！</p><p style="text-wrap: wrap;"><br/></p><p style="text-wrap: wrap;"><strong>功能特色说明</strong>：</p><p style="text-wrap: wrap;"><br/></p><p style="text-wrap: wrap;">一、</p><p style="text-wrap: wrap;"><br/></p><p style="text-wrap: wrap;">二、</p><p style="text-wrap: wrap;"><br/></p><p style="text-wrap: wrap;">三、</p>',
  'qtgg' => '请每天按时更新资源！！66666',
  'sitedomain' => 'fangchang.lwb88.cn',
);
