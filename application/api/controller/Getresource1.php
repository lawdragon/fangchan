<?php
namespace app\api\controller;

use app\common\controller\Api;
use think\Controller;
use think\Page;
use think\Verify;
use think\Image;
use think\Db;
use think\Request;
use think\Session;
use think\Cookie;

class Getresource extends Controller
{
    public $info;
    public $token;
    public $cook_name;
    public function __construct()
    {
        header("Access-Control-Allow-Origin: *");
        header("Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept");
        //获取当前域名
        $domain = $_SERVER['HTTP_HOST'];
        if(!isset($_GET['token']) && !isset($_POST['token'])){
            $arr=array('data'=>'','message'=>'error','msg'=>'没有token参数','statusCode'=>500,'timestamp'=>time());
            echo json_encode($arr);exit;
        }else{
            $token = $_GET['token']?$_GET['token']:$_POST['token'];
        }
        $this->token=$token;
        /*
        //URL中域名提取示例
            $url = 'https://zhan.leiue.com/weibo.html';
            $arr = parse_url($url);
            $domain = $arr['host'];
            echo $domain;
        */
        //通过域名url和token得到链接id
        $link_url=$domain.'?token='.$token;
        $model = Db::name('resourced_url');
        $field='*';
        //localhost?token=729ac8dda044ddaa7747627d4598d1a2
        if(strpos($link_url,'localhost') !== false || $token=="729ac8dda044ddaa7747627d4598d1a2"){
            $link_url='localhost?token='.$token;
            $where['id']=28;
        }else{
            $where['link_url']=$link_url?$link_url:0;
            $where['status']=1;
        }
        //$where['id']=28;
        //$where['end_time']=array('gt',time());
        //状态=1 end_time>当前时间
        $info =$model->alias('a')->field($field)->where($where)->find();
        //$this->info=$info;
        if(($info && $info['end_time']<time()) || empty($info)){
            if(strpos($_SERVER['REQUEST_URI'],'index2') !== false || strpos($_SERVER['REQUEST_URI'],'list2') !== false){
                $msg='信息验证中';
            }else{
                $msg='链接token已失效';
            }
            $res=array('data'=>'','msg'=>$msg,'tokens'=>$where['link_url'],'SERVER_NAME'=>$_SERVER['REQUEST_URI'],'message'=>'error','statusCode'=>500,'timestamp'=>time());
            $res['statusCode']=2004;
            echo json_encode($res);exit;
        }elseif(empty($info)){
            if(strpos($_SERVER['REQUEST_URI'],'index2') !== false || strpos($_SERVER['REQUEST_URI'],'list2') !== false){
                $msg='信息验证中';
            }else{
                $msg='链接token已失效';
            }
            $res=array('data'=>'','msg'=>$msg,'tokens'=>$where['link_url'],'SERVER_NAME'=>$_SERVER['REQUEST_URI'],'message'=>'error','statusCode'=>500,'timestamp'=>time());
            $res['statusCode']=2004;
            echo json_encode($res);exit;
        }else{
            $this->info=$info;
        }
        /*if(empty($info)){
            if(strpos($_SERVER['REQUEST_URI'],'index2') !== false || strpos($_SERVER['REQUEST_URI'],'list2') !== false){
                $msg='信息验证中';
            }else{
                $msg='链接token已失效';
            }
            $res=array('data'=>'','msg'=>$msg,'tokens'=>$link_url,'SERVER_NAME'=>$_SERVER['REQUEST_URI'],'message'=>'error','statusCode'=>500,'timestamp'=>time());
            $res['statusCode']=2004;
            echo json_encode($res);exit;
        }else{
            $this->info=$info;
        }*/
        $ip=str_ireplace('.','_',$this->getip());
        $this->cook_name='name_auth_'.$ip;//'name_auth-'.$ip;
        //通过密码比对链接设置的密码返回成功或者失败
        parent::__construct();

    }
    //提交密码
    public function verifyPassword2(Request $request){
        if ($this->request->isPost()) {
            $password = $this->request->post('password')?$this->request->post('password'):7778;
            $token = $this->token;//$this->request->get('token');
            $accessPassword=md5($password . $this->info['pwd_random']);
            //验证密码是否为空
            if(!$password){
                $res=array('data'=>array('auth'=>''),'msg'=>'密码不能为空','message'=>'success','statusCode'=>400,'timestamp'=>time());
                $res['statusCode']=5001;
                echo json_encode($res);exit;
            }
            if(empty($token) || empty($accessPassword) || $token!=$accessPassword){
                $res=array('data'=>'','message'=>'error','msg'=>'密码错误！','statusCode'=>500,'timestamp'=>time());
                $res['statusCode']=5001;
                echo json_encode($res);exit;
            }
            //验证密码是否过期
            $ip=$this->getip();
            $auth_arr['ip']=$ip;
            $auth_arr['url_id']=$this->info['id'];
            $auth_arr['token']=$token;
            $auth_arr['cuid']=$this->info['cuid'];
            //$auth=MD5($ip.'-'.$this->info['id'].'-'.$token);
            $key = 'sdojiofjreoi*&%75&%^'; // 密钥
            $auth = $this->encrypt(json_encode($auth_arr), $key);
            // 设置Cookie 有效期为 3600秒 链接id+ip+time()

            Cookie::set($this->cook_name,$auth,['prefix' => 'think_', 'expire' => 3600*2]);

            $res=array('data'=>array('auth'=>$auth),'message'=>'success','statusCode'=>200,'timestamp'=>time());

            echo json_encode($res);exit;
        }
        $res=array('data'=>array('auth'=>'','post'=>$this->request->isPost()),'message'=>'success','statusCode'=>500,'timestamp'=>time());
        $res['statusCode']=2004;
        echo json_encode($res);exit;
    }
    public function encrypt($data, $key) {
        $key = md5($key);
        $x = 0;
        $len = strlen($data);
        $l = strlen($key);
        $char = '';
        $str = '';
        for ($i = 0; $i < $len; $i++) {
            if ($x == $l) {
                $x = 0;
            }
            $char.= $key{$x};
            $x++;
        }
        for ($i = 0; $i < $len; $i++) {
            $str.= chr(ord($data{$i}) + (ord($char{$i})) % 256);
        }
        return base64_encode($str);
    }
    public function decrypt($data, $key) {
        $key = md5($key);
        $x = 0;
        $data = base64_decode($data);
        $len = strlen($data);
        $l = strlen($key);
        $char = '';
        $str = '';
        for ($i = 0; $i < $len; $i++) {
            if ($x == $l) {
                $x = 0;
            }
            $char.= substr($key, $x, 1);
            $x++;
        }
        for ($i = 0; $i < $len; $i++) {
            if (ord(substr($data, $i, 1)) < ord(substr($char, $i, 1))) {
                $str.= chr((ord(substr($data, $i, 1)) + 256) - ord(substr($char, $i, 1)));
            } else {
                $str.= chr(ord(substr($data, $i, 1)) - ord(substr($char, $i, 1)));
            }
        }
        return $str;
    }
    public function index2(Request $request)
    {
        $categoryList=$this->getCategoryLists(array('a.status'=>1));
        $getAreaList=$this->getAreaLists(array('a.status'=>1));
        $classify=array();
        if($categoryList){
            foreach($categoryList as $k=>$v){
                //$classify[$v['id']]=$v['category_name'];
                $classify[]=array('id'=>$v['id'],'name'=>$v['category_name']);
            }
        }
        $region=array();
        if($getAreaList){
            foreach($getAreaList as $k1=>$v1){
                //$region[$v1['id']]=$v1['area_name'];
                $region[]=array('id'=>$v1['id'],'name'=>$v1['area_name']);
            }
        }
        $getPzList=$this->getPzLists(array('a.status'=>1));
        /*$getPzList=array();
        if($getPzList){
            foreach($getPzList as $k2=>$v2){
                //$region[$v1['id']]=$v1['area_name'];
                $pzList[]=array('id'=>$v2['id'],'name'=>$v2['area_name']);
            }
        }*/
        $getPzLists[]=array('id'=>168,'name'=>'好评推荐');
        //$this->token = $this->request->post('token');
        return json([
            'token' => $this->token,
            'notice' => '内部联盟资源，请勿外传！',
            'classify' => $classify,
            'region' => $region,
            'pzList'=>array_merge($getPzLists,$getPzList)
        ]);
    }
    /*
         address: "松茂御龙湾"
classify: "一室"
cover: "https://s1011.aihuifu.cn/data2/f1/10011/101.zip"
end_time: ""
id: 4
is_recommend: 0
region: "宝安"
remarks: ""
title: "御龙湾57平"*/
    public function list2(Request $request)
    {
        //$this->token = $this->request->post('token');
        $new = $this->request->post('new');//0 上新 1 今日上新 2.3日上新
        $classify = $this->request->post('classify');//分类
        $region = $this->request->post('region');//地区
        $page = $this->request->post('page')?$this->request->post('page'):1;//分页
        $recommend = $this->request->post('recommend');//0 推荐 1 好评推荐
        $value = $this->request->post('value');//搜索值
        $auth = $this->request->post('auth');//auth
        if($this->info['querys']){
            $where=json_decode($this->info['querys'],true);
        }
        if (in_array($new = $this->request->post('new/d', 0), [1, 2])) {
            $todayTime = strtotime(date('Ymd')) - 1;
            //$where= ['a.createtime', 'between', [$new === 1 ? $todayTime + 1 : $todayTime + 1 - 86400 * 3, $todayTime + 86400]];
            if($new === 1){
                $where['a.createtime']=array(array('egt',$todayTime + 1),array('elt',$todayTime + 86400,'and'));
            }else{
                $where['a.createtime']=array(array('egt',$todayTime + 1 - 86400 * 3),array('elt',$todayTime + 86400,'and'));
            }
        }
        //var_dump($where);exit;
        if($classify){//&& !$where['a.category_id']
            $where['a.category_id']=$classify;
        }
        if($region){// && !$where['a.area_id']
            $where['a.area_id']=$region;
        }
        if($recommend==168){
            $where['a.is_recommend']=1;
        }elseif($recommend){
            //$where['a.pz_id']=$recommend;
            //WHERE categories REGEXP "(^|,)4(,|$)"
            //->where('a.pz_id','exp',"REGEXP '".$recommend."' ")
        }
        if($recommend==1){
            //$where['a.is_recommend']=1;
        }
        if($value){
            $where['a.title']=array('like','%'.$value.'%');
        }
        //判断auth是否过期
        $ip=$this->getip();
        $yz_auth=Cookie::get($this->cook_name,'think_');
        $statusCode=200;
        if(empty($auth) || empty($yz_auth) || $yz_auth!=$auth){
            $res['msg']='信息验证中';//$yz_auth.'=>'.$auth
            $statusCode=2004;
        }
        $limit=10;
        $order='a.createtime desc';
        //条件搜索
        $list=array();
        //得到当前代理删除的资源id
        $where_del['uid']=$this->info['cuid'];
        $where_del['gl_is_del']=1;
        $list_del=db::name('resources_del')->field('rid')->where($where_del)->select();
        if($list_del){
            $arr_rid=array();
            foreach($list_del as $k2=>$v2){
                if($v2['rid']){
                    $arr_rid[]=$v2['rid'];
                }
            }
            if($arr_rid && @!$where['a.rid']){
                $where['a.rid']=array('not in',join(',',$arr_rid));
            }
        }
        $where['a.status']=1;
        $where['a.is_del']=0;
        //var_dump($where);exit;
        if($statusCode==200){
            $model = Db::name('Resources');//classify ,FROM_UNIXTIME(a.createtime,'%Y-%m-%d %H:%i:%s') as create_time
            $field="a.rid as id,a.number,a.title,a.address,a.video,a.content,FROM_UNIXTIME(a.createtime,'%Y-%m-%d %H:%i:%s') as create_time,a.end_time,a.is_recommend,a.is_top,a.remarks,a.status,a.cover,b.area_name as region,c.category_name as classify";
            if($recommend==168 || empty($recommend)){
                $list =$model->alias('a')->Join('area b',"b.id=a.area_id","LEFT")->Join('category c',"c.id=a.category_id","LEFT")->field($field)->where($where)->order($order)->limit($limit)->page($page)->select();
                $count=$model->alias('a')->Join('area b',"b.id=a.area_id","LEFT")->Join('category c',"c.id=a.category_id","LEFT")->where($where)->count();
            }elseif($recommend){
                //$where['a.pz_id']=$recommend;
                //WHERE categories REGEXP "(^|,)4(,|$)"
                //->where('a.pz_id','exp',"REGEXP '".$recommend."' ")
                $list =$model->alias('a')->Join('area b',"b.id=a.area_id","LEFT")->Join('category c',"c.id=a.category_id","LEFT")->field($field)->where($where)->where('a.pz_id','exp',"REGEXP '".$recommend."' ")->order($order)->limit($limit)->page($page)->select();
                $count=$model->alias('a')->Join('area b',"b.id=a.area_id","LEFT")->Join('category c',"c.id=a.category_id","LEFT")->where($where)->where('a.pz_id','exp',"REGEXP '".$recommend."' ")->count();
            }

        }
        $total=0;
        $totalPages=0;
        if($list){
            $total=$count;
            $totalPages=ceil($total/10);
            $resources_user=db::name('resources_user');
            foreach($list as $k=>&$v){
                $info=$resources_user->where(array('rid'=>$v['id'],'uid'=>$this->info['cuid'],'gl_status'=>1,'is_del'=>0))->field('remarks')->find();
                if($info['remarks']){
                    $v['remarks']=$info['remarks'];
                }
                if($v['end_time']>time()){
                    $v['end_time']=$this->getEndTime($v['end_time']);
                }else{
                    $v['end_time']='';
                }
             }
        }
        $res['data']=[
            'currentPage' => $page,//分页
            'data' => $list,//列表数据
            'total' => $total,//总条数
            'totalPages' => $totalPages,//总分页数
            'message' => 'success',//状态
            'statusCode' => $statusCode,//状态码
            'timestamp' => time(),//时间戳
        ];
        $res['statusCode']=$statusCode;
        return json($res);

    }
    /*
     address: "松茂御龙湾"
classify: "一室"
content: "<p><img src="https://s1011.aihuifu.cn/data2/f1/10011/101.zip"/></p><p><img src="https://s1011.aihuifu.cn/data2/f1/10011/102.zip"/></p><p><img src="https://s1011.aihuifu.cn/data2/f1/10011/103.zip"/></p><p><img src="https://s1011.aihuifu.cn/data2/f1/10011/104.zip"/></p><p><img src="https://s1011.aihuifu.cn/data2/f1/10011/105.zip"/></p>"
cover: "https://s1011.aihuifu.cn/data2/f1/10011/101.zip"
create_time: 1679464767
end_time: ""
id: 4
is_recommend: 0
is_top: 0
number: "10011"
region: "宝安"
remarks: ""
status: 1
title: "御龙湾57平"
video: "https://s1011.aihuifu.cn/data2/f1/10011/201.7z"
message: "success"
statusCode: 200
timestamp: 1685864716
     * */
    //实时验证token的方法和进入详情时的 访问统计
    public function details2(Request $request){
        //$this->token = $this->request->post('token');
        $id = $this->request->get('id');//资源id
        $auth = $this->request->get('auth');//auth
        //判断auth是否过期
        $ip=$this->getip();
        $yz_auth=Cookie::get($this->cook_name,'think_');
        $statusCode=200;
        if(empty($auth) || empty($yz_auth) || $yz_auth!=$auth){
            $statusCode=2004;
        }
        $where['a.rid']=$id;
        //条件搜索
        $model = Db::name('Resources');//classify
        $field="a.rid as id,a.number,a.title,a.address,a.video,a.content,FROM_UNIXTIME(a.createtime,'%Y-%m-%d %H:%i:%s') as create_time,a.end_time,a.is_recommend,a.is_top,a.remarks,a.status,a.cover,b.area_name as region,c.category_name as classify";//,a1.remarks as remarks2
        if($statusCode==200){
            //->Join('resources_user a1',"a1.rid=a.rid","LEFT")
            $info =$model->alias('a')->Join('area b',"b.id=a.area_id","LEFT")->Join('category c',"c.id=a.category_id","LEFT")->field($field)->where($where)->find();

            $resources_user_info=db::name('resources_user')->where(array('rid'=>$info['id'],'uid'=>$this->info['cuid'],'gl_status'=>1,'is_del'=>0))->field('remarks')->find();
            if($info['remarks']){
                $v['remarks']=$info['remarks'];
            }
            if($info['end_time']>time()){
                $info['end_time']=$this->getEndTime($info['end_time']);
            }else{
                $info['end_time']='';
            }

        }
         $res['data']=[
            'address' => $info['address'],
            'classify' => $info['classify'],
            'content' => $info['content'],
            'cover' => $info['cover'],
            'create_time' => $info['create_time'],
            'end_time' => $info['end_time'],
            'id' => $info['id'],
            'is_recommend' => $info['is_recommend'],
            'is_top' => $info['is_top'],
            'number' => $info['number'],
            'region' => $info['region'],
            'remarks' =>  $resources_user_info['remarks']?$resources_user_info['remarks']:$info['remarks'],
            'status' =>  $info['status'],
            'title' =>  $info['title'],
            'video' =>  $info['video'],
            'message' => 'success',//状态
            'statusCode' => $statusCode,//状态码
            'timestamp' => time(),//时间戳
        ];
        //判断今日IP是否重复进线
        $time=strtotime(date('Y-m-d',time()));
        //$ip=$this->getip();
        $key = 'sdojiofjreoi*&%75&%^'; // 密钥
        $auth_arr = $this->decrypt($auth, $key);
        $auth_arrs=json_decode($auth_arr,true);
        $where_log['ip']=$ip;
        //$where_log['rid']=$res['data']['id'];
        //$where_log['url_id']=$auth_arrs['url_id'];
        $where_log['cuid']=$auth_arrs['cuid'];
        $where_log['createtime']=array('GT',$time);
        $model = Db::name('access_log');
        $field='*';
        $info =$model->alias('a')->field($field)->where($where_log)->find();
        if(!$info){
            $add_log['ip']=$ip;
            $add_log['rid']=$res['data']['id'];
            $add_log['url_id']=$auth_arrs['url_id'];
            $add_log['cuid']=$auth_arrs['cuid'];
            $add_log['createtime']=time();
            $result=$model->insert($add_log);
        }
        $res['statusCode']=$statusCode;
        return json($res);
    }
    private function getEndTime(int $endTime): string
    {
        $return = '';
        if ($endTime > time()) {
            $time = $endTime - time();
            $day = $time > 86400 ? floor($time / 86400) : 0;
            $time -= $day * 86400;
            $hour = $time > 3600 ? floor($time / 3600) : 0;
            $time -= $hour * 3600;
            $minute = $time > 60 ? floor($time / 60) : 0;
            $return = ($day ? $day . '天' : '') . ($hour ? $hour . '时' : '') . ($minute ? $minute . '分' : '');
        }
        return $return ? $return . '后' : '';
    }
    public function getip() {

        static $ip = '';

        $ip = $_SERVER['REMOTE_ADDR'];

        if(isset($_SERVER['HTTP_CDN_SRC_IP'])) {

            $ip = $_SERVER['HTTP_CDN_SRC_IP'];

        } elseif (isset($_SERVER['HTTP_CLIENT_IP']) && preg_match('/^([0-9]{1,3}\.){3}[0-9]{1,3}$/', $_SERVER['HTTP_CLIENT_IP'])) {

            $ip = $_SERVER['HTTP_CLIENT_IP'];

        } elseif(isset($_SERVER['HTTP_X_FORWARDED_FOR']) AND preg_match_all('#\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}#s', $_SERVER['HTTP_X_FORWARDED_FOR'], $matches)) {

            foreach ($matches[0] AS $xip) {

                if (!preg_match('#^(10|172\.16|192\.168)\.#', $xip)) {

                    $ip = $xip;

                    break;

                }

            }

        }

        return $ip;

    }
    //分类列表
    public function getCategoryLists($where="a.status in(0,1)",$order="a.createtime desc"){

        $model = Db::name('category');
        $field='a.*';
        $list =$model->alias('a')->field($field)->where($where)->order($order)->select();

        return $list?$list:array();
    }
    //区域列表
    public function getAreaLists($where="a.status in(0,1)",$order="a.createtime desc"){

        $model = Db::name('area');
        $field='a.*';
        $list =$model->alias('a')->field($field)->where($where)->order($order)->select();

        return $list?$list:array();
    }
    //资源配置项列表
    public function getPzLists($where="a.status in(0,1)",$order="a.createtime desc"){

        $model = Db::name('resources_config');
        $field='a.id,a.name';
        $list =$model->alias('a')->field($field)->where($where)->order($order)->select();

        return $list?$list:array();
    }
}
