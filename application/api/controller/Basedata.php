<?php

namespace app\api\controller;

use app\common\controller\Api;
use think\Db;
/**
 * 首页接口
 */
class Basedata extends Api
{
    protected $noNeedLogin = ['*'];
    protected $noNeedRight = ['*'];

    /**
     * 分类
     */
    public function categoryList()
    {
      $status = input('status', 1);
      $where= ['status' => $status];
      $order= input('order', 'weigh desc');
      $model = Db::name('category');
      $field='a.*';
      $categoryList =$model->alias('a')->field($field)->where($where)->order($order)->select();
      $list=array();
      if($categoryList){
          foreach($categoryList as $k=>$v){
              $list[$v['id']]=$v['category_name'];
          }
      }
      return json($list);
    }

    public function getAreaList()
    {
      $status = input('status', 'in(0,1)'); // $where="a.status in(0,1)",$order="a.weigh desc"
      $where= ['status' => $status];
      $order= input('order', 'weigh desc');
      $model = Db::name('area');
        $field='a.*';
        $getAreaList =$model->alias('a')->field($field)->where($where)->order($order)->select();
        $list=array();
        if($getAreaList){
            foreach($getAreaList as $k=>$v){
                $list[$v['id']]=$v['area_name'];
            }
        }
      return json($list);
    }

    public function getdllist(){
        $order= input('order', 'createtime desc');
        $order = 'a.'.$order;
        $model = Db::name('admin');
        $field='a.id,a.username,a.nickname';
        $getdlLists =$model->alias('a')->Join('auth_group_access b',"b.uid=a.id")->field($field)->where(1)->order($order)->select();
        $list=array();
        if($getdlLists){
            foreach($getdlLists as $k=>$v){
                $list[$v['id']]=$v['nickname'];
            }
        }
        return json($list);
    }

}
