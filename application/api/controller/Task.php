<?php

namespace app\api\controller;

use app\common\controller\Api;
use think\Controller;
use think\Page;
use think\Verify;
use think\Image;
use think\Db;
use think\Request;
use think\Session;
use think\Cookie;

class Task extends Controller
{
    public function update_idle_status()
    {
        $where = [
            'status' => 1,
            'is_del' => 0,
            'is_idle' => 1,
        ];
        // update(['is_idle' => 1])
        // echo time()-3600*2;  // ->whereTime('updatetime','<', time()-3600*2)
        $list = db::name('resources_user')->where($where)->select();
        // echo '<pre>';
        // print_r($list);
        // exit;
        if (!empty($list)) {
            $ids = [];
            foreach ($list as $v) {
                // 1分钟  60， 2小时 3600 * 2
                if ($v['is_idle_updatetime'] < time() - 3600 * 2) {
                    $ids[] = $v['id'];
                }
            }
            // Db::query("UPDATE `ds_resources_user` SET is_idle=0 WHERE id={$v['id']}");
            $where = [];
            $where['id'] = array('in', join(',', $ids));
            // echo '<pre>';
            // print_r($where);
            // exit;
            db::name('resources_user')->where($where)->update(['is_idle' => 0]);
        }
        // echo '<pre>';
        // print_r($list);
    }


    public function index()
    {
        //定时脚本:跑出当天排班
        //清空前一天的所有关联资源排班

        //1.查询所有 未删除的关联资源
        //2.参加排班表
        //资源id 代理id 排班时间 状态
        db::name('pb_log')->insert(array('addtime' => time()));
        $model = Db::name('resources_user');
        $model_pb = Db::name('pb');

        $where_user['a.is_del'] = 0;
        $where_user['a.gl_status'] = 1;
        $where_user['a1.is_del'] = 0;
        $list = $model->alias('a')->field('a.rid,a.uid,c.id')->Join('resources a1', "a1.rid=a.rid")->Join('pb c', "c.rid=a.rid and c.status=1", 'LEFT')->where($where_user)->order('a.rid asc')->select();
        //->group('a.rid') 	
        $rid_arr = array();
        $list_arr = array();

        if ($list) {
            //查询所有资源关联代理排班
            foreach ($list as $k => $v) {
                if ($v['rid']) {
                    $rid_arr[$v['rid']] = $v['rid'];
                    if (!isset($list_arr[$v['rid']])) {
                        $list_arr[$v['rid']] = $v['id'];
                    }
                }
            }

            if ($rid_arr) {
                $where_pb['a.rid'] = array('in', join(',', array_values($rid_arr)));
                $where_pd['b.expire_time'] = array('gt', time());
                $where_pd['b.status'] = 'normal';
                $pd_list = $model_pb->alias('a')->field('a.id,a.rid,a.uid,a.status')->Join('admin b', "b.id=a.uid")->where($where_pb)->order('a.id asc')->select();
                $pd_lists = array();
                if ($pd_list) {
                    foreach ($pd_list as $k3 => $v3) {
                        if ($v3['rid']) {
                            $pd_lists[$v3['rid']][] = $v3;
                        }
                    }
                }
            }
            //var_dump($pd_lists);exit;
            $save_arr = array();
            $model_pb->where('id>1')->update(array('status' => 0, 'pb_time' => ''));
            if ($pd_list) {
                foreach ($list_arr as $k1 => $v1) {
                    if (@$pd_lists[$k1]) {
                        $counts = COUNT($pd_lists[$k1]);
                        for ($a = 0; $a < $counts; $a++) {
                            if ($pd_lists[$k1][$a]['id'] == $v1) {
                                if (($a + 1) == $counts) {
                                    $save_arr[$k1] = array('id' => $pd_lists[$k1][0]['id'], 'rid' => $k1, 'uid' => $pd_lists[$k1][0]['uid']);
                                } else {
                                    $save_arr[$k1] = array('id' => $pd_lists[$k1][($a + 1)]['id'], 'rid' => $k1, 'uid' => $pd_lists[$k1][($a + 1)]['uid']);
                                }
                            }
                            if ($pd_lists[$k1][$a]['status'] != 1 && !isset($save_arr[$k1])) {
                                $save_arr[$k1] = array('id' => $pd_lists[$k1][$a]['id'], 'rid' => $k1, 'uid' => $pd_lists[$k1][$a]['uid']);
                            }
                        }
                        if (!isset($save_arr[$k1])) {
                            $save_arr[$k1] = array('id' => $pd_lists[$k1][0]['id'], 'rid' => $k1, 'uid' => $pd_lists[$k1][0]['uid']);
                        }
                    }
                }
            }

            if (@$save_arr) {
                foreach ($save_arr as $k4 => $v4) {
                    $model_pb->where(array('id' => $v4['id']))->update(array('status' => 1, 'pb_time' => time()));
                }
            }
        } else {
            $model_pb->where('id>1')->update(array('status' => 0, 'pb_time' => ''));
        }
        echo '排班成功！！';
    }
    public function index_sss()
    {
        //定时脚本:跑出当天排班
        //清空前一天的所有关联资源排班

        //1.查询所有 未删除的关联资源
        //2.参加排班表
        //资源id 代理id 排班时间 状态

        $model = Db::name('resources_user');
        $model_pb = Db::name('pb');

        $where_user['a.is_del'] = 0;
        $where_user['a.gl_status'] = 1;
        $where_user['a1.is_del'] = 0;
        $list = $model->alias('a')->field('a.rid,a.uid,c.id')->Join('resources a1', "a1.rid=a.rid")->Join('pb c', "c.rid=a.rid and c.status=1", 'LEFT')->where($where_user)->group('a.rid,a.uid')->order('a.id asc')->select();
        $rid_arr = array();
        if ($list) {
            //查询所有资源关联代理排班
            foreach ($list as $k => $v) {
                if ($v['rid']) {
                    $rid_arr[] = $v['rid'];
                }
            }
            if ($rid_arr) {
                $where_pb['a.rid'] = array('in', join(',', $rid_arr));
                $where_pd['b.expire_time'] = array('gt', time());
                $where_pd['b.status'] = 'normal';
                $pd_list = $model_pb->alias('a')->field('a.id,a.rid,a.uid')->Join('resources a1', "a1.rid=a.rid", 'LEFT')->Join('admin b', "b.id=a.uid")->where($where_pb)->order('a.rid asc,a.id asc')->select();

                $pd_lists = array();
                if ($pd_list) {
                    foreach ($pd_list as $k3 => $v3) {
                        if ($v3['rid']) {
                            $pd_lists[$v3['rid']][] = $v3;
                        }
                    }
                }
            }
            var_dump($list);
            exit;
            $save_arr = array();
            if ($pd_list) {
                foreach ($list as $k1 => $v1) {
                    if ($pd_lists[$v1]) {
                        $counts = COUNT($pd_lists[$v1]);
                        for ($a = 0; $a < $counts; $a++) {
                            $save_arr[$v1] = array('id' => $pd_lists[$v1][0]['id'], 'rid' => $v1, 'uid' => $pd_lists[$v1][0]['uid']);
                            if ($pd_lists[$v1][$a]['id'] == $v1['id']) {
                                $model_pb->where(array('id' => $v1['id']))->update(array('status' => 0, 'pb_time' => ''));
                                if (($a + 1) == $counts) {
                                    $save_arr[$v1] = array('id' => $pd_lists[$v1][0]['id'], 'rid' => $v1, 'uid' => $pd_lists[$v1][0]['uid']);
                                } else {
                                    $save_arr[$v1] = array('id' => $pd_lists[$v1][($a + 1)]['id'], 'rid' => $v1, 'uid' => $pd_lists[$v1][($a + 1)]['uid']);
                                }
                            }
                            $model_pb->where(array('id' => $save_arr[$v1]['id']))->update(array('status' => 1, 'pb_time' => time()));
                        }
                    }
                }
            }
        } else {
            $model_pb->update(array('status' => 0, 'pb_time' => ''));
        }
        echo '排班成功！！';
        //var_dump($save_arr,$pd_list);
    }
}
