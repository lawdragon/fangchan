<?php

namespace app\api\controller;

use app\common\controller\Api;
use think\Controller;
use think\Page;
use think\Verify;
use think\Image;
use think\Db;
use think\Request;
use think\Session;
use think\Cookie;

class Getresource extends Controller
{
    public $info;
    public $token;
    public $cook_name;
    public $sessen_name;
    public $admin;

    public function __construct()
    {
        header("Access-Control-Allow-Origin: *");
        header("Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept");
        //通过密码比对链接设置的密码返回成功或者失败
        // if (!empty($auth)) {
        //     Cookie::set($this->cook_name, $auth, ['prefix' => 'think_', 'expire' => 3600 * 3]);
        // }
        $this->admin = Session::get('admin');
        // halt($this->admin);
        if ($this->admin) {
            $this->info['cuid'] = $this->admin['id'];
        }
        parent::__construct();
    }

    //提交密码
    public function verifyPassword2(Request $request)
    {
        if ($this->request->isPost()) {
            $domain = $_SERVER['HTTP_HOST'];
            $link_url = $domain;
            // echo $domain;exit;
            $model = Db::name('resourced_url');
            $field = '*';
            $where['link_url'] = $link_url ? $link_url : 0;
            $where['status'] = 1;
            $info = $model->alias('a')->field($field)->where($where)->find();
            $password = $this->request->post('password') ? $this->request->post('password') : '';
            // $token = $this->token; //$this->request->get('token');
            $accessPassword = md5($password . $this->info['pwd_random'] . time());
            $this->sessen_name = $accessPassword;
            //验证密码是否为空
            if (!$password) {
                $res = array('data' => array('auth' => ''), 'msg' => '密码不能为空', 'message' => 'success', 'statusCode' => 5001, 'timestamp' => time());
                echo json_encode($res);
                exit;
            }
            /*if(empty($token) || empty($accessPassword) || $token!=$accessPassword){
                      $res=array('data'=>'','message'=>'error','msg'=>'密码错误！','statusCode'=>500,'timestamp'=>time());
                      $res['statusCode']=5001;
                      echo json_encode($res);exit;
                  }*/
            // halt($info);
            if ($password != $info['pwd']) {
                $res = array('data' => '', 'message' => 'error', 'msg' => '密码错误！', 'statusCode' => 5001, 'timestamp' => time());
                // $res['statusCode'] = 5001;
                echo json_encode($res);
                exit;
            }
            // 验证密码是否过期
            $ip = $this->getip();
            $auth_arr['ip'] = $ip;
            $auth_arr['url_id'] = $info['id'];
            $auth_arr['token'] = $accessPassword;
            $auth_arr['cuid'] = $info['cuid'];
            $auth_arr['pwd'] = $password;
            //$auth=MD5($ip.'-'.$this->info['id'].'-'.$token);
            // $key = 'sdojiofjreoi*&%75&%^'; // 密钥
            // $auth = $this->encrypt(json_encode($auth_arr), $key);
            // 设置Cookie 有效期为 3600秒 链接id+ip+time()
            // Cookie::set($this->cook_name, $auth, ['prefix' => 'think_', 'expire' => 3600 * 3]);
            $this->token = $accessPassword;
            Session::set($this->sessen_name, $auth_arr);
            $res = array('data' => array('auth' => $accessPassword), 'message' => 'success', 'statusCode' => 200, 'timestamp' => time());
            echo json_encode($res);
            exit;
        }
        $res = array('data' => array('auth' => '', 'post' => $this->request->isPost()), 'message' => 'success', 'statusCode' => 500, 'timestamp' => time());
        $res['statusCode'] = 2004;
        echo json_encode($res);
        exit;
    }

    public function encrypt($data, $key)
    {
        $key = md5($key);
        $x = 0;
        $len = strlen($data);
        $l = strlen($key);
        $char = '';
        $str = '';
        for ($i = 0; $i < $len; $i++) {
            if ($x == $l) {
                $x = 0;
            }
            $char .= $key{
            $x};
            $x++;
        }
        for ($i = 0; $i < $len; $i++) {
            $str .= chr(ord($data{
                $i}) + (ord($char{
                $i})) % 256);
        }
        return base64_encode($str);
    }

    public function decrypt($data, $key)
    {
        $key = md5($key);
        $x = 0;
        $data = base64_decode($data);
        $len = strlen($data);
        $l = strlen($key);
        $char = '';
        $str = '';
        for ($i = 0; $i < $len; $i++) {
            if ($x == $l) {
                $x = 0;
            }
            $char .= substr($key, $x, 1);
            $x++;
        }
        for ($i = 0; $i < $len; $i++) {
            if (ord(substr($data, $i, 1)) < ord(substr($char, $i, 1))) {
                $str .= chr((ord(substr($data, $i, 1)) + 256) - ord(substr($char, $i, 1)));
            } else {
                $str .= chr(ord(substr($data, $i, 1)) - ord(substr($char, $i, 1)));
            }
        }
        return $str;
    }

    public function checkLogin()
    {
        $admin = Session::get('admin');
        if ($admin) {
            // 如果后台已登录，前端跳转不校验
            return false;
        }
        $domain = $_SERVER['HTTP_HOST'];
        // 第一步先校验域名是否过期
        //通过域名url和token得到链接id
        // halt(cookie('__uni__uid'));
        //获取当前域名
        $token = input('token');
        if ($token) {
            $link_url = $domain;
            $where['link_url'] = $link_url;
            $this->sessen_name = $token; //'name_auth-'.$ip;
            $auth = Session::get($this->sessen_name);
            // echo $this->sessen_name;exit;
            // 第二部判断是否有cookie，没有cookie跳登录页
            if (!$auth && $token) {
                // 登录过期了
                $msg = '登录已过期';
                $res = array('data' => '', 'msg' => $msg, 'message' => 'error', 'statusCode' => 2004, 'timestamp' => time());
                echo json_encode($res);
                exit;
            }
            $model = Db::name('resourced_url');
            $field = '*';
            $where['link_url'] = $link_url ? $link_url : 0;
            $where['status'] = 1;
            $info = $model->alias('a')->field($field)->where($where)->find();
            // halt($info);
            if (($info && $info['end_time'] < time())) {
                $msg = '链接已失效';
                $res = array('data' => '', 'msg' => $msg, 'message' => 'error', 'statusCode' => 500, 'timestamp' => time());
                $res['statusCode'] = 2004;
                echo json_encode($res);
                exit;
            } elseif (empty($info)) {
                $msg = '无效链接';
                $res = array('data' => '', 'msg' => $msg, 'message' => 'error', 'statusCode' => 500, 'timestamp' => time());
                $res['statusCode'] = 2004;
                echo json_encode($res);
                exit;
            } else {
                $this->info = $info;
            }
            // if ($token && $auth) {
            //     // Cookie::set($this->cook_name, $auth, ['prefix' => 'think_', 'expire' => 3600 * 3]);
            //     Session::set($this->sessen_name, $token);
            //     $this->token = $token;
            // }
        } else {
            $msg = '请先登录';
            $res = array('data' => '', 'msg' => $msg, 'message' => 'error', 'statusCode' => 500, 'timestamp' => time());
            $res['statusCode'] = 2004;
            echo json_encode($res);
            exit;
        }
    }

    public function index2(Request $request)
    {
        $this->checkLogin();
        $categoryList = $this->getCategoryLists(array('a.status' => 1));
        $getAreaList = $this->getAreaLists(array('a.status' => 1));
        $classify = array();
        if ($categoryList) {
            foreach ($categoryList as $k => $v) {
                //$classify[$v['id']]=$v['category_name'];
                $classify[] = array('id' => $v['id'], 'name' => $v['category_name']);
            }
        }
        $region = array();
        if ($getAreaList) {
            foreach ($getAreaList as $k1 => $v1) {
                //$region[$v1['id']]=$v1['area_name'];
                $region[] = array('id' => $v1['id'], 'name' => $v1['area_name']);
            }
        }
        $getPzList = $this->getPzLists(array('a.status' => 1));
        /*$getPzList=array();
            if($getPzList){
                foreach($getPzList as $k2=>$v2){
                    //$region[$v1['id']]=$v1['area_name'];
                    $pzList[]=array('id'=>$v2['id'],'name'=>$v2['area_name']);
                }
            }*/
        $getPzLists[] = array('id' => 168, 'name' => '好评推荐');
        $config = db::name('config')->where(array('id' => 20))->find();
        //$this->token = $this->request->post('token');
        return json([
            'token' => $this->token,
            'notice' => $config['value'], //'内部联盟资源，请勿外传！',
            'classify' => $classify,
            'region' => $region,
            'pzList' => array_merge($getPzLists, $getPzList)
        ]);
    }

    /*
           address: "松茂御龙湾"
  classify: "一室"
  cover: "https://s1011.aihuifu.cn/data2/f1/10011/101.zip"
  end_time: ""
  id: 4
  is_recommend: 0
  region: "宝安"
  remarks: ""
  title: "御龙湾57平"*/
    public function list2(Request $request)
    {
        //$this->token = $this->request->post('token');
        $this->checkLogin();
        $new = $this->request->post('new'); //0 上新 1 今日上新 2.3日上新
        $classify = $this->request->post('classify'); //分类
        $region = $this->request->post('region'); //地区
        $page = $this->request->post('page') ? $this->request->post('page') : 1; //分页
        $recommend = $this->request->post('recommend'); //0 推荐 1 好评推荐
        $value = $this->request->post('value'); //搜索值
        $auth = $this->request->post('auth'); //auth
        if (isset($this->info['querys']) && $this->info['querys']) {
            $querys = json_decode($this->info['querys'], true);
            $where = [];
            if (is_array($querys)) {
                foreach ($querys as $k => $v) {
                    if ($k != 'a.status') {
                        $where[$k] = $v;
                    }
                }
            }
        }
        // echo '<pre>';
        // print_r($where);
        // exit;
        if (in_array($new = $this->request->post('new/d', 0), [1, 2])) {
            $todayTime = strtotime(date('Ymd')) - 1;
            //$where= ['a.createtime', 'between', [$new === 1 ? $todayTime + 1 : $todayTime + 1 - 86400 * 3, $todayTime + 86400]];
            if ($new === 1) {
                $where['a.createtime'] = array(array('egt', $todayTime + 1), array('elt', $todayTime + 86400, 'and'));
            } else {
                $where['a.createtime'] = array(array('egt', $todayTime + 1 - 86400 * 3), array('elt', $todayTime + 86400, 'and'));
            }
        }
        //var_dump($where);exit;
        if ($classify) { //&& !$where['a.category_id']
            $where['a.category_id'] = $classify;
        }
        if ($region) { // && !$where['a.area_id']
            $where['a.area_id'] = $region;
        }
        if ($recommend == 168) {
            $where['ru.is_recommend'] = 1;
        } elseif ($recommend) {
            //$where['a.pz_id']=$recommend;
            //WHERE categories REGEXP "(^|,)4(,|$)"
            //->where('a.pz_id','exp',"REGEXP '".$recommend."' ")
        }
        if ($recommend == 1) {
            //$where['a.is_recommend']=1;
        }
        if ($value) {
            $where['a.title|a.rid|ru.remarks'] = array('like', '%' . $value . '%');
        }
        //判断auth是否过期
        // $ip = $this->getip();
        // $yz_auth = Cookie::get($this->cook_name, 'think_');
        // $statusCode = 200;
        // if (empty($auth) || empty($yz_auth) || $yz_auth != $auth) {
        //     $res['msg'] = '信息验证中'; //$yz_auth.'=>'.$auth
        //     $statusCode = 2004;
        // } else {
        //     $key = 'sdojiofjreoi*&%75&%^'; // 密钥
        //     $auth_arr = $this->decrypt($auth, $key);
        //     $auth_arrs = json_decode($auth_arr, true);
        //     if ($this->info['pwd'] != @$auth_arrs['pwd']) {
        //         $res['msg'] = '信息验证中';
        //         $statusCode = 2004;
        //     }
        // }
        $limit = 10;
        $order = 'a.createtime desc';
        $indexSort = \think\Config::get('site.indexSort');
        // $indexSort = 4;
        if ($indexSort == 3) {
            $order = 'ru.is_idle asc';
        }
        //条件搜索
        $list = array();
        //得到当前代理删除的资源id
        $where_del['uid'] = $this->info['cuid'];
        $where_del['gl_is_del'] = 1;
        // $list_del = db::name('resources_del')->field('rid')->where($where_del)->select();
        //得到当前代理下架的资源id
        $where_user1['uid'] = $this->info['cuid'];
        $where_user1['status'] = 0;
        // $list_user = db::name('resources_user')->field('rid')->where($where_user1)->select();
        $arr_rid = array();
        // if ($list_del) {
        //     foreach ($list_del as $k2 => $v2) {
        //         if ($v2['rid']) {
        //             $arr_rid[] = $v2['rid'];
        //         }
        //     }
        // }
        // if ($list_user) {
        //     foreach ($list_user as $k21 => $v21) {
        //         if ($v21['rid']) {
        //             $arr_rid[] = $v21['rid'];
        //         }
        //     }
        // }
        $isSearchTop = false;
        $istop_list = [];
        $top_ids = []; // 置顶ID
        if (!$new && !$classify && !$region && !$recommend && !$value) {
            $isSearchTop = true;
            // 置顶的，置顶只在首页展示，选择了排序筛选就不管。
            $where_istop = [
                'is_top' => 1, 'is_del' => 0, 'gl_status' => 1, 'status' => 1, 'uid' => $this->info['cuid']
            ];
            $isTopSourceList = db::name('resources_user')->where($where_istop)->select();
            if ($isTopSourceList) {
                foreach ($isTopSourceList as $k21 => $v21) {
                    if ($v21['rid']) {
                        $arr_rid[] = $v21['rid'];
                        $top_ids[] = $v21['rid'];
                    }
                }
            }
            if (count($isTopSourceList)) {
                $limit = $limit - count($isTopSourceList);
            }
            // echo '<pre>';
            // print_r($where_istop);
            // exit;
        }
        if ($top_ids && @!$where['ru.rid']) {
            $where['ru.rid'] = array('not in', join(',', $top_ids));
        }
        //var_dump($where['a.rid'],$list_user);exit;
        $where['ru.status'] = 1; // 1.上架 2.下架 3.删除
        $where['ru.is_del'] = 0; // 0正常 1刪除
        $where['ru.gl_status'] = 1; // 0取消关联，1关联
        $where['uid'] = $this->info['cuid']; // 当前代理ID
        //var_dump($where);exit;
        $model = Db::name('resources_user'); //classify ,FROM_UNIXTIME(a.createtime,'%Y-%m-%d %H:%i:%s') as create_time
        // 查询是否有设置置顶 a.is_recommend,a.rid,au.is_top,a.rid as id, a.rid,
        $field = "
        ru.is_top,
        ru.is_idle,
        ru.is_recommend,
        ru.status,
        ru.is_del,
        ru.gl_status,
        ru.rid as ru_rid,
        a.rid as id,
        a.number,
        a.title,
        a.address,
        a.video,
        a.content,FROM_UNIXTIME(a.createtime,'%Y-%m-%d %H:%i:%s') as create_time,
        a.end_time,
        a.remarks,
        a.cover,
        a.pz_id,
        b.area_name as region,
        c.category_name as classify";
        // $resourceUserModel = Db::name('resources_user');
        // $isTopSourceList = $resourceUserModel->where(['is_top' => 1, 'is_del' => 0, 'gl_status' => 1, 'status' => 1])->select();
        // if (count($isTopSourceList)) {
        //     $limit = $limit - count($isTopSourceList);
        // }
        //
        // echo '<pre>';
        // print_r($where);
        // exit;
        $count = 0;
        if ($recommend == 168 || empty($recommend)) {
            // $list = $model->alias('a')->Join('area b', "b.id=a.area_id", "LEFT")->Join('category c', "c.id=a.category_id", "LEFT")->field($field)->where($where)->order($order)->limit($limit)->page($page)->select();
            if ($indexSort == 2) {
                $list = $model->alias('ru')->join('resources a', "ru.rid = a.rid", "left")->Join('area b', "b.id=a.area_id", "LEFT")->Join('category c', "c.id=a.category_id", "LEFT")->field($field)->where($where)->orderRaw('rand()')->limit($limit)->page($page)->select();
            } else if ($indexSort == 4) {
                $list = $model->alias('ru')->join('resources a', "ru.rid = a.rid", "left")->Join('area b', "b.id=a.area_id", "LEFT")->Join('category c', "c.id=a.category_id", "LEFT")->field($field)->where($where)->order('ru.is_idle asc')->orderRaw('rand()')->limit($limit)->page($page)->select();
            } else {
                $list = $model->alias('ru')->join('resources a', "ru.rid = a.rid", "left")->Join('area b', "b.id=a.area_id", "LEFT")->Join('category c', "c.id=a.category_id", "LEFT")->field($field)->where($where)->order($order)->limit($limit)->page($page)->select();
            }
            $count = $model->alias('ru')->join('resources a', "ru.rid = a.rid", "left")->Join('area b', "b.id=a.area_id", "LEFT")->Join('category c', "c.id=a.category_id", "LEFT")->where($where)->count();
        } elseif ($recommend) {
            //$where['a.pz_id']=$recommend;
            //WHERE categories REGEXP "(^|,)4(,|$)"
            //->where('a.pz_id','exp',"REGEXP '".$recommend."' ")
            // $list = $model->alias('a')->Join('area b', "b.id=a.area_id", "LEFT")->Join('category c', "c.id=a.category_id", "LEFT")->field($field)->where($where)->where('a.pz_id', 'exp', "REGEXP '" . $recommend . "' ")->order($order)->limit($limit)->page($page)->select();
            if ($indexSort == 2) {
                $list = $model->alias('ru')->Join('resources a', "a.rid=ru.rid", "LEFT")->Join('area b', "b.id=a.area_id", "LEFT")->Join('category c', "c.id=a.category_id", "LEFT")->field($field)->where($where)
//                    ->where('a.pz_id', 'exp', "REGEXP '" . $recommend . "' ")
                    ->where('FIND_IN_SET('.$recommend.', a.pz_id)')
                    ->orderRaw('rand()')->limit($limit)->page($page)->select();
            } else if ($indexSort == 4) {
                $list = $model->alias('ru')->Join('resources a', "a.rid=ru.rid", "LEFT")->Join('area b', "b.id=a.area_id", "LEFT")->Join('category c', "c.id=a.category_id", "LEFT")->field($field)->where($where)
//                    ->where('a.pz_id', 'exp', "REGEXP '" . $recommend . "' ")
                    ->where('FIND_IN_SET('.$recommend.', a.pz_id)')
                    ->order('ru.is_idle asc')->orderRaw('rand()')->limit($limit)->page($page)->select();
            } else {
                $list = $model->alias('ru')->Join('resources a', "a.rid=ru.rid", "LEFT")->Join('area b', "b.id=a.area_id", "LEFT")->Join('category c', "c.id=a.category_id", "LEFT")->field($field)->where($where)
//                    ->where('a.pz_id', 'exp', "REGEXP '" . $recommend . "' ")
                    ->where('FIND_IN_SET('.$recommend.', a.pz_id)')
                    ->order($order)->limit($limit)->page($page)->select();
            }
            $count = $model->alias('ru')->Join('resources a', "a.rid=ru.rid", "LEFT")->Join('area b', "b.id=a.area_id", "LEFT")->Join('category c', "c.id=a.category_id", "LEFT")->where($where)
//                ->where('a.pz_id', 'exp', "REGEXP '" . $recommend . "' ")
                ->where('FIND_IN_SET('.$recommend.', a.pz_id)')
                ->count();
        }
        if ($isSearchTop) { // 置顶数据信息
            $where_istop = [];
            $where_istop['a.rid'] = array('in', join(',', $top_ids));
            if ($indexSort == 2) {
                $istop_list = $model->alias('ru')->join('resources a', "ru.rid = a.rid", "left")->Join('area b', "b.id=a.area_id", "LEFT")->Join('category c', "c.id=a.category_id", "LEFT")->field($field)->where($where_istop)->orderRaw('rand()')->select();
            } else if ($indexSort == 4) {
                $istop_list = $model->alias('ru')->join('resources a', "ru.rid = a.rid", "left")->Join('area b', "b.id=a.area_id", "LEFT")->Join('category c', "c.id=a.category_id", "LEFT")->field($field)->where($where_istop)->order('ru.is_idle asc')->orderRaw('rand()')->select();
            } else {
                $istop_list = $model->alias('ru')->join('resources a', "ru.rid = a.rid", "left")->Join('area b', "b.id=a.area_id", "LEFT")->Join('category c', "c.id=a.category_id", "LEFT")->field($field)->where($where_istop)->order($order)->select();
            }
        }
//        echo $model->getLastSql();die;
        //查询所有的资源配置信息
        $getPzList = $this->getPzLists(array('a.status' => 1));
        $getPzLists = [];
        if ($getPzList) {
            $getPzLists = array_column($getPzList, null, 'id');
        }
        $total = 0;
        $totalPages = 0;
        // 获取置顶数据
        if ($isSearchTop && count($istop_list)) {
            $list = array_merge($istop_list, $list);
        }
        if ($list) {
            $total = $count;
            $totalPages = ceil($total / 10);
            $resources_user = db::name('resources_user');
            foreach ($list as $k => &$v) {
                $info = $resources_user->where(array('rid' => $v['id'], 'uid' => $this->info['cuid'], 'gl_status' => 1, 'is_del' => 0))->field('remarks')->find();
                if (isset($info['remarks'])) {
                    $v['remarks'] = $info['remarks'];
                }
                if ($v['end_time'] > time()) {
                    $v['end_time_original'] = $v['end_time'];
                    $v['end_time'] = $this->getEndTime($v['end_time']);
                } else {
                    $v['end_time'] = '';
                }
                $v['pz_list'] = '';
                $v['pz_sum'] = 0;
                if ($v['pz_id'] && $getPzLists) {
                    $pzlist = explode(',', $v['pz_id']);
                    // echo '<pre>';print_r($pzlist);exit;
                    for ($a = 0; $a < count($pzlist); $a++) {
                        if ($v['pz_sum'] < 2) {
                            $name = isset($getPzLists[$pzlist[$a]]['name']) ? $getPzLists[$pzlist[$a]]['name'] : '';
                            $v['pz_list'] .= $name . ' ';
                            $v['pz_sum'] += 1;
                        }
                    }
                }
            }
        }
        $res['data'] = [
            'currentPage' => $page, //分页
            'data' => $list, //列表数据
            'total' => $total, //总条数
            'totalPages' => $totalPages, //总分页数
            'message' => 'success', //状态
            'statusCode' => 200, //状态码
            'timestamp' => time(), //时间戳
        ];
        return json($res);
    }
    /*
       address: "松茂御龙湾"
  classify: "一室"
  content: "<p><img src="https://s1011.aihuifu.cn/data2/f1/10011/101.zip"/></p><p><img src="https://s1011.aihuifu.cn/data2/f1/10011/102.zip"/></p><p><img src="https://s1011.aihuifu.cn/data2/f1/10011/103.zip"/></p><p><img src="https://s1011.aihuifu.cn/data2/f1/10011/104.zip"/></p><p><img src="https://s1011.aihuifu.cn/data2/f1/10011/105.zip"/></p>"
  cover: "https://s1011.aihuifu.cn/data2/f1/10011/101.zip"
  create_time: 1679464767
  end_time: ""
  id: 4
  is_recommend: 0
  is_top: 0
  number: "10011"
  region: "宝安"
  remarks: ""
  status: 1
  title: "御龙湾57平"
  video: "https://s1011.aihuifu.cn/data2/f1/10011/201.7z"
  message: "success"
  statusCode: 200
  timestamp: 1685864716
       * */
    //实时验证token的方法和进入详情时的 访问统计
    public function details2(Request $request)
    {
        //$this->token = $this->request->post('token');
        $this->checkLogin();
        $id = $this->request->get('id'); //资源id
        $pagefrom = $this->request->get('pagefrom', '');
        // $auth = $this->request->get('auth'); //auth
        //判断auth是否过期
        $ip = $this->getip();
        // $yz_auth = Cookie::get($this->cook_name, 'think_');
        // $statusCode = 200;
        // if (empty($auth) || empty($yz_auth) || $yz_auth != $auth) {
        //     $statusCode = 2004;
        // }
        $where['a.rid'] = $id;
        $resources = '';
        // $where['ru.uid'] = $this->info['cuid'];
        if ($pagefrom == 'backend') {
            // 后台查看，uid 为创建人
            $resources = db::name('resources')->where(['rid' => $id])->field('cuid')->find();
            $where['ru.uid'] = $resources['cuid']; // 当前代理ID
        } else {
            $where['ru.uid'] = $this->info['cuid']; // 当前代理ID
        }
        //条件搜索
        $model = Db::name('resources_user'); //classify
        $field = "
        ru.is_top,
        ru.is_idle,
        ru.is_recommend,
        ru.status,
        ru.is_del,
        ru.gl_status,
        ru.rid as ru_rid,
        ru.uid,
        a.rid as id,
        a.end_time,
        a.number,
        a.title,
        a.address,
        a.video,
        a.content,
        FROM_UNIXTIME(a.createtime,'%Y-%m-%d %H:%i:%s') as create_time,
        a.remarks,
        a.cover,
        a.pz_id,
        b.area_name as region,
        c.category_name as classify"; //,a1.remarks as remarks2
        $info = [];
        $info = $model->alias('ru')->join('resources a', "ru.rid = a.rid", "left")->Join('area b', "b.id=a.area_id", "LEFT")->Join('category c', "c.id=a.category_id", "LEFT")->field($field)->where($where)->find();
        // echo Db::getLastSql();exit;
        $where2 = array('rid' => $info['id'], 'gl_status' => 1, 'is_del' => 0);
        if ($pagefrom == 'backend') {
            $where2['uid'] = $resources['cuid'];
        } else {
            $where2['uid'] = $this->info['cuid'];
        }
        $resources_user_info = db::name('resources_user')->where($where2)->field('remarks')->find();
        if ($info['remarks']) {
            $v['remarks'] = $info['remarks'];
        }
        if ($info['end_time'] > time()) {
            $info['end_time'] = $this->getEndTime($info['end_time']);
        } else {
            $info['end_time'] = '';
        }
        //$html_content=isset($info['cover'])?"<p><img src='{$info['cover']}'></p>":'';
        $html_content = '';
        if (isset($info['content'])) {
            $contentList = explode(',', $info['content']);
            for ($a = 0; $a < count($contentList); $a++) {
                $html_content .= "<p><img src='{$contentList[$a]}'></p>";
            }
            //var_dump($html_content);
        }
        //查询所有的资源配置信息
        $getPzList = $this->getPzLists(array('a.status' => 1));
        $getPzLists = [];
        if ($getPzList) {
            $getPzLists = array_column($getPzList, null, 'id');
        }
        $pz_list = '';
        $pz_sum = 0;
        if (isset($info['pz_id']) && $info['pz_id'] && $getPzLists) {
            $pzlist = explode(',', $info['pz_id']);
            for ($a = 0; $a < count($pzlist); $a++) {
                //if($pz_sum<3){
                $name = isset($getPzLists[$pzlist[$a]]['name']) ? $getPzLists[$pzlist[$a]]['name'] : '';
                $pz_list .= $name . ' ';
                $pz_sum += 1;
                //}
            }
        }
        // $key = 'sdojiofjreoi*&%75&%^'; // 密钥
        // $auth_arr = $this->decrypt($auth, $key);
        // $auth_arrs = json_decode($auth_arr, true);
        // if ($this->info['pwd'] != @$auth_arrs['pwd']) {
        //     $statusCode = 2004;
        // }
        $res['data'] = [
            'pz_list' => $pz_list,
            'address' => isset($info['address']) ? $info['address'] : '',
            'classify' => isset($info['classify']) ? $info['classify'] : '',
            'content' => $html_content, //$info['content'],
            'cover' => isset($info['cover']) ? $info['cover'] : '',
            'create_time' => isset($info['create_time']) ? $info['create_time'] : '',
            'end_time' => isset($info['end_time']) ? $info['end_time'] : '',
            'id' => isset($info['id']) ? $info['id'] : '',
            'is_recommend' => isset($info['is_recommend']) ? $info['is_recommend'] : '',
            'is_top' => isset($info['is_top']) ? $info['is_top'] : '',
            'is_idle' => isset($info['is_idle']) ? $info['is_idle'] : '',
            'number' => isset($info['number']) ? $info['number'] : '',
            'region' => isset($info['region']) ? $info['region'] : '',
            'remarks' => isset($resources_user_info['remarks']) ? $resources_user_info['remarks'] : isset($info['remarks']) ? $info['remarks'] : '',
            'status' => isset($info['status']) ? $info['status'] : '',
            'title' => isset($info['title']) ? $info['title'] : '',
            'video' => isset($info['video']) ? $info['video'] : '',
        ];
        // 判断今日IP是否重复进线
        $time = strtotime(date('Y-m-d', time()));
        //$ip=$this->getip();
        if (!$this->admin) {
            // 统计访问量
            $auth_arrs = Session::get($this->sessen_name);
            // echo '<pre>';
            // Session::delete($this->sessen_name);
            // print_r($auth_arrs);
            // halt($auth_arrs);
            // exit;
            $where_log['ip'] = $ip;
            //$where_log['rid']=$res['data']['id'];
            //$where_log['url_id']=$auth_arrs['url_id'];
            $where_log['cuid'] = $auth_arrs['cuid'];
            $where_log['createtime'] = array('GT', $time);
            $model = Db::name('access_log');
            $field = '*';
            $info = $model->alias('a')->field($field)->where($where_log)->find();
            if (!$info) {
                $add_log['ip'] = $ip;
                $add_log['rid'] = $res['data']['id'];
                $add_log['url_id'] = $auth_arrs['url_id'];
                $add_log['cuid'] = $auth_arrs['cuid'];
                $add_log['createtime'] = time();
                $result = $model->insert($add_log);
            }
        }
        return json($res);
    }

    private function getEndTime(int $endTime): string
    {
        $return = '';
        if ($endTime > time()) {
            $time = $endTime - time();
            $day = $time > 86400 ? floor($time / 86400) : 0;
            $time -= $day * 86400;
            $hour = $time > 3600 ? floor($time / 3600) : 0;
            $time -= $hour * 3600;
            $minute = $time > 60 ? floor($time / 60) : 0;
            $return = ($day ? $day . '天' : '') . ($hour ? $hour . '时' : '') . ($minute ? $minute . '分' : '');
        }
        return $return ? $return . '后' : '';
    }

    public function getip()
    {
        static $ip = '';
        $ip = $_SERVER['REMOTE_ADDR'];
        if (isset($_SERVER['HTTP_CDN_SRC_IP'])) {
            $ip = $_SERVER['HTTP_CDN_SRC_IP'];
        } elseif (isset($_SERVER['HTTP_CLIENT_IP']) && preg_match('/^([0-9]{1,3}\.){3}[0-9]{1,3}$/', $_SERVER['HTTP_CLIENT_IP'])) {
            $ip = $_SERVER['HTTP_CLIENT_IP'];
        } elseif (isset($_SERVER['HTTP_X_FORWARDED_FOR']) and preg_match_all('#\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}#s', $_SERVER['HTTP_X_FORWARDED_FOR'], $matches)) {
            foreach ($matches[0] as $xip) {
                if (!preg_match('#^(10|172\.16|192\.168)\.#', $xip)) {
                    $ip = $xip;
                    break;
                }
            }
        }
        return $ip;
    }

    //分类列表
    public function getCategoryLists($where = "a.status in(0,1)", $order = "a.weigh desc")
    {
        $model = Db::name('category');
        $field = 'a.*';
        $list = $model->alias('a')->field($field)->where($where)->order($order)->select();
        return $list ? $list : array();
    }

    //区域列表
    public function getAreaLists($where = "a.status in(0,1)", $order = "a.weigh desc")
    {
        $model = Db::name('area');
        $field = 'a.*';
        $list = $model->alias('a')->field($field)->where($where)->order($order)->select();
        return $list ? $list : array();
    }

    //资源配置项列表
    public function getPzLists($where = "a.status in(0,1)", $order = "a.weigh desc")
    {
        $model = Db::name('resources_config');
        $field = 'a.id,a.name';
        $list = $model->alias('a')->field($field)->where($where)->order($order)->select();
        return $list ? $list : array();
    }

    public function getConfig()
    {
        // $config.变量名
        // 取所有的二维码链接
        $erweima_links = "";
        $auth_arrs = Session::get($this->sessen_name);
        foreach ($auth_arrs as $key => $item) {
            $erweima_links = db::name('resourced_url')->field('url')->where('status', 1)->where('cuid', $item['cuid'])->select();
            break;
        }
        $data = [
            'sitename' => \think\Config::get('site.name'),
            'sitecolor' => \think\Config::get('site.sitecolor'),
            'isAllowWechat' => \think\Config::get('site.isAllowWechat'),
            'links' => $erweima_links,
        ];
        $res = [
            'data' => $data, //列表数据
            'message' => 'success', //状态
            'statusCode' => 200, //状态码
            'timestamp' => time(), //时间戳
        ];
        return json($res);
    }
}
